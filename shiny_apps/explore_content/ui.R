###################################
# CONNECTION EXPLORATION APP ######
###################################

library(shiny)
library(plotly)
library(roller)
library(shinyjs)
source("~/projects/techno_sample/shiny_apps/common_functions.R")

# need a better way to get this infomration than loading it here
# global gex metadata 
global.gex.meta <- read.table(return_files('','','global_gex_meta'), sep='\t', header=T, quote='')
# limit to aig
global.gex.meta <- global.gex.meta[!is.na(global.gex.meta$pr_is_lm),]


shinyUI(
  tagList(
    useShinyjs(),
    navbarPage(title="Explore connections between TCGA and CMap", id='bigNav', inverse=T, 
               
               # TCGA summary tab ----------
               tabPanel('TCGA Summary',
                        tabsetPanel(id='tcga_summary',
                                    # global summary --------
                                    tabPanel('Global summary',
                                             fluidPage(
                                               # data table of cohorts and t-SNE plot
                                               fluidRow(
                                                 tags$h2('Summary of TCGA gene expression data'),
                                                 tags$h5('10255 samples were analyzed across 30 different cancer types.'),
                                                 hr()),
                                               column(6, align='center',
                                                      tags$h4('t-SNE clustering on expression of 10147 Best INferred Genes (BING)'),
                                                      tags$h5('TCGA samples primarily cluster by tissue of origin'), 
                                                      # plotlyOutput('global_mrna_tsne',height = 'auto'),
                                                      tags$hr(),
                                                      tags$br(),
                                                      tags$h4('High quality example connections'),
                                                      tags$h5('click row to view connection plot'),
                                                      dataTableOutput('global_interesting_connections')),
                                               column(6, align='center',  
                                                      tags$h4('Number of samples in each cohort'),
                                                      tags$h5('Different analyses were performed on subsets of the entire list of samples'),
                                                      dataTableOutput('global_cohort_info_table')
                                               )
                                               # List of links to high quality connections
                                             )
                                    ),
                                    
                                    # cohort-level summary --------
                                    tabPanel('Cohort-level summary', value='cohort_summary',
                                             fluidPage(
                                               fluidRow(
                                                 tags$h3('Summary for an individual TCGA cohort'), 
                                                 column(3, selectInput('cohort_summary_choice', 'Cohort', selected='BRCA',
                                                             choices=tcga.cohorts,selectize = T)),
                                                 column(3, uiOutput('cohort_summary_stats'))),
                                               hr(),
                                               column(4, align='center',
                                                      tags$h4('t-SNE clustering on expression of 10147 Best INferred Genes (BING)'),
                                                      tags$h5('colored by transcriptional subtype from NMF clustering'),
                                                      plotlyOutput('cohort_mrna_tsne',height = 'auto'),
                                                      tags$hr(),
                                                      tags$h4('NMF clustering of gene expression data'),
                                                      tags$h5('Image coming soon.....')),
                                               column(8, align='center',
                                                      fluidRow(tags$h4('Number of sample sets'),
                                                               dataTableOutput('cohort_sample_set_summary_table')),
                                                      tags$hr(),
                                                      tags$h4('Frequently altered genes in this cohort'),
                                                      tags$h5('Click a row to view best connections'),
                                                      fluidRow(
                                                        column(5, align='center', 
                                                               tags$h4('Mutations'),
                                                               dataTableOutput('cohort_frequently_mutated_table')),
                                                        column(7, align='center',
                                                               tags$h4('Copy number alterations'),
                                                               dataTableOutput('cohort_frequently_cn_table')))
                                               )
                                             )
                                             
                                    ),
                                    
                                    #gene expression information --------
                                    tabPanel('Gene expression information',
                                             fluidPage(
                                               fluidRow(
                                                 tags$h4('View gene expression or Z-scores across cohorts'),
                                                 # column(4, uiOutput('choose_gene_expression'), actionButton('update_expression_plot', 'Update Plot')),
                                                 column(4, selectizeInput('choose_gene_expression', 'Select a gene', choices=global.gex.meta$pr_gene_symbol, selected='TP53'),
                                                        actionButton('update_expression_plot', 'Update Plot')),
                                                 column(2, radioButtons('gex_plot_exzs', 'Plot', choices=c('expression', 'Z-score'), selected='expression')),
                                                 column(4, selectInput('expression_cohort_limit', 'Limit to cohort', selected='None',
                                                                       choices=c('None (boxplot)', 'Pancan (histogram)', tcga.cohorts),selectize = T))
                                                 
                                               ),
                                               hr(),
                                               uiOutput('expression_loading_text'),
                                               plotlyOutput('expression_plot'),
                                               dataTableOutput('expression_statistics')
                                             )
                                             
                                    )
                                    
                        )
               ), 
               
               # sample set summary tab ---------
               tabPanel('Sample Set Summary', value='sample_set_summary',
                        fluidPage(
                          fluidRow(
                            column(2,
                                   selectInput('choose_cohort_summary','TCGA cohort', choices=c('brca', 'luad', 'coad','prad','skcm_met'),selected = 'brca')
                            ),
                            column(2, uiOutput('choose_alteration_summary')
                            ),
                            uiOutput('num_samples_summary')
                          ),
                          fluidRow(
                            tabsetPanel(id = 'sample_set_summary_tabset',
                                        tabPanel('Set overview',
                                                 fluidRow(
                                                   tags$h4('Sample sets in this class'),
                                                   tags$h5('Click on a row to see set members'),
                                                   dataTableOutput('sample_set_list')
                                                 ),
                                                 fluidRow(
                                                   column(8, br(), plotlyOutput('ss_size_hist')),
                                                   column(4, tableOutput('ss_size_stats'))
                                                 )
                                        ),
                                        tabPanel('Pairwise overlap',
                                                 column(8,
                                                        tags$p(),
                                                        tags$p('Different sample sets might contain many of the 
                                                               same samples. This will lead to inherent correlation of results. 
                                                               Here, we are showing the fraction of overlap between all possible 
                                                               pairs of sets. A value of 1 means the smaller set was completely 
                                                               contained within the larger set. Its easier for larger sets to have 
                                                               high pairwise overlap values, so its recommended to use the Jaccard similarity
                                                               as well to evaluate this problem.'),
                                                        tags$p('Pairwise overlap (a,b) = |a n b| / min(|a|,|b|)'),
                                                        uiOutput('overlap_text'),
                                                        plotOutput('ss_overlap_hist'),
                                                        plotOutput('ss_overlap_hist2')
                                                 ),
                                                 column(4, 
                                                        uiOutput('ss_overlap_warnings'),
                                                        uiOutput('ss_overlap_75'),
                                                        uiOutput('ss_overlap_1'),
                                                        tags$p('TODO: click on a row and see sets that overlap?'))
                                        ),
                                        tabPanel('Pairwise Jaccard',
                                                 column(8,
                                                        tags$p(),
                                                        tags$p('Different sample sets might contain many of the 
                                      same samples. This will lead to inherent correlation of results. 
                                      Here, we are showing the Jaccard similarity of all possible 
                                      pairs of sets.'),
                                                        tags$p('Jaccard index J(a,b) = |a n b|/|a u b|'),
                                                        uiOutput('jaccard_text'),
                                                        plotOutput('ss_jaccard_hist'),
                                                        plotOutput('ss_jaccard_hist2')
                                                 ),
                                                 column(4, 
                                                        uiOutput('ss_jaccard_warnings'),
                                                        uiOutput('ss_jaccard_75'),
                                                        uiOutput('ss_jaccard_1'),
                                                        tags$p('TODO: click on a row and see sets that overlap?'))
                                        ),
                                        tabPanel('Set member clustering',
                                                 column(4, 
                                                        tags$p('Some text about the options here'),
                                                        radioButtons('cluster_metric','Clustering metric', 
                                                                     choices=c('pairwise overlap', 'Jaccard index'),
                                                                     selected = 'pairwise overlap'),
                                                        radioButtons('cluster_method', '2D projection method', c('MDS', 'PCA', 't-SNE'), selected='MDS'),
                                                        sliderInput('cluster_thresh', 'Similarity threshold', min = 0.75, max=1.0, round = -2,value = 0.75),
                                                        actionButton('cluster_go', 'Do clustering'),
                                                        uiOutput('cluster_error'),
                                                        textInput('cluster_nclus', 'Number of k-means clusters',value = 3),
                                                        radioButtons('cluster_kmdata', 'Perform k-means on what data', 
                                                                     choices=c('2D projection','distance matrix')),
                                                        actionButton('update_kmeans', 'Update kmeans coloring')
                                                 ),
                                                 column(8,
                                                        plotlyOutput('cluster_scatter'),
                                                        downloadButton('cluster_download', 'Download cluster members in csv format')
                                                 )
                                        ),
                                        tabPanel('Signature strength',
                                                 fluidRow(
                                                   column(8,
                                                          plotlyOutput('sig_strength_plot')
                                                   ),
                                                   column(4,
                                                          tags$h4('Signature strength of sample sets'),
                                                          tags$p('A sample set defines a case-control comparison between two classes
                                                     of samples. The strength metric captures how different the cases and controls
                                                     are in gene expression. It is the difference between the mean S2N values from the 
                                                     top and bottom 100 differentially expressed genes. A higher strength means the sample
                                                     set is capturing a feature that is visible in gene expression space.'),
                                                          tags$p('Usually smaller sets have better strength scores. This plot can be used to find 
                                                     the strongest sample sets when compared to random sets of comparable size. The orange
                                                     line represents a loess smooth fit of random sets. ')
                                                   )
                                                 ), 
                                                 fluidRow(
                                                   column(8,
                                                          plotlyOutput('strength_pvalue_scatter')
                                                   ),
                                                   column(4,
                                                          tags$h4('Signature strength and p-values are related'),
                                                          tags$p('A sample set with a stronger strength is more likely to capture a biological signal of 
                                                     interest and connect well to CMap perturbagens. This relationship is shown in the plot')
                                                   )
                                                 )
                                        ),
                                        tabPanel('Sample set enrichment clustering', value='enrichment_clustering',
                                                 fluidPage(
                                                   radioButtons('cor_type','Correlation data to show', 
                                                                choices=c('enrichment','pvalue','signed pvalue')),
                                                   imageOutput('cor_image', width='800px', height='600px'),
                                                   fluidRow(
                                                     br(),
                                                     column(3, textInput('cor_nclus', 'Number of clusters to cut dendrogram',value = 3)),
                                                     column(3, br(), downloadButton('cor_download', 'Download cluster members in text format'))
                                                   ),
                                                   plotOutput('cor_dend'),
                                                   hr(),
                                                   h3('Clustering information'),
                                                   tags$p('Spearman correlation between sample sets. 
                                                                   All pertrubagens were used to do the correlation calculation. 
                                                                   Hierarchical clustering is done on 1 minus the correlation matrix with the ward.D2 method.')
                                                 )
                                        ),
                                        tabPanel('Specific set information',
                                                 fluidPage(
                                                   fluidRow(
                                                    column(4,uiOutput('specific_info_pick_set')),
                                                    column(4,uiOutput('specific_info_num_samples')),
                                                    column(4,br(),downloadButton('specific_info_download_members','Download data table'))
                                                   ),
                                                   dataTableOutput('specific_info_set_members'),
                                                   plotOutput('specific_info_nmf_members')
                                                 )
                                        )
                            )
                          )
                          
                        )
               ),
               # Explore connections ---------------
               tabPanel('Explore Connections', value='explore_connections',
                        fluidPage(
                          sidebarLayout(
                            sidebarPanel(
                              selectInput('choose_cohort','TCGA cohort', choices=c('brca', 'luad', 'coad','prad','skcm_met'),selected = 'brca'),
                              selectInput('choose_alteration', 'Pick a set of alterations', choices = c('cna', 'mutations', 'outliers', 'receptor_status')),
                              # uiOutput('choose_alteration'),
                              
                              radioButtons('filter_connections', 'filter connections',
                                           choices={button.choices <- list('expected', 'best_set', 'best_pert', 'best_overall', 'all_set')
                                           names(button.choices) <- c('expected connections', 'best to sample set', 'best to perturbagen',
                                                                      'best overall', 'all to a sample set')
                                           button.choices},
                                           selected = 'expected'),
                              uiOutput('choose_ss'),
                              uiOutput('choose_type'),
                              uiOutput('choose_iname'),
                              uiOutput('choose_cell'),
                              actionButton('update_table', 'Update table'),
                              hr(), 
                              uiOutput('rows_out')
                            ),
                            mainPanel(
                              plotlyOutput('connectivity_bar'),
                              # h1('explore connections to sample sets'),
                              dataTableOutput('connections')
                            )
                          )
                        )
               ),
               # Survival correlations -----------------
               tabPanel('Survival Correlations',
                        fluidPage(
                          sidebarLayout(
                            sidebarPanel(
                              selectInput('choose_cohort_survival','TCGA cohort', choices=tcga.cohorts,selected = 'luad'),
                              selectizeInput('filter_line_survival', 'Filter cell line', choices=c('all','summly','MCF7','A549','A375','PC3','HT29','HA1E','HCC515','VCAP','HEPG2'), selected='summly'),
                              actionButton('update_table_survival', 'Update table'),
                              br(),
                              uiOutput('rows_out_survival'),
                              br(),
                              radioButtons('survival_sign','Pick best __ samples', choices = c('positive', 'negative'), selected = 'positive'),
                              uiOutput('survival_slider'),
                              downloadButton('download_survival_data',"Download current data")
                            ),
                            mainPanel(
                              plotOutput('survival_figure'),
                              dataTableOutput('survival_correlation_table')
                            )
                          )
                        )
               )
    )
  )
)

# some useful JS for swithcing tabs and select options
# no longer needed...
# Some functions for switching pages around
# HTML("<script> function test(cohort) {
#                          tabs = $('.navbar .nav.navbar-nav li')
#                          tabs.each(function() {
#                          $(this).removeClass('active')
#                          })
#                          $(tabs[1]).addClass('active')
#                          
#                          tabsContents = $('.container-fluid .tab-content .tab-pane')
#                          tabsContents.each(function() {
#                          $(this).removeClass('active')
#                          })
#                          $(tabsContents[1]).addClass('active')
#                          $('#cohort_summary').trigger('change').trigger('shown');
#                          
#                          var sel = document.getElementById('cohort_summary_choice');
#                          var opts = sel.options;
#                          for(var opt, j = 0; opt = opts[j]; j++) {
#                              if(opt.value == cohort) {
#                                  sel.selectedIndex = j;
#                                  break;
#                              }
#                          }
# 
#                          }</script>
#                          ")
