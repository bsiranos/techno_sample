# let's figure out this outlier stuff once and for all
tcga.meta <- read.table('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/col_meta.txt', sep='\t', header=T, quote='')
gex.brca <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_PROFILE.L1KAIG_TXONLY_n10255x12276.gctx', 
                       cid=tcga.meta[tcga.meta$cohort=='BRCA', 'tcga_id'])
zs.brca <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KAIG_TXONLY_n10255x12276.gctx', 
                      cid=tcga.meta[tcga.meta$cohort=='BRCA', 'tcga_id'])
id.to.symbol <- gex.brca@rdesc$pr_gene_symbol
names(id.to.symbol) <- gex.brca@rdesc$id
symbol.to.id <- gex.brca@rdesc$id
names(symbol.to.id) <- gex.brca@rdesc$pr_gene_symbol
brca.outliers <- parse.gmt('~/cmap_tcga/enrichment_results_lm_build/cohorts/brca/outliers_adaptive_z3/query_up.gmt')
set.size <- sapply(brca.outliers, function(x) x$len)
gex.nz <- apply(gex.brca@mat, 1, function(x) sum(x==0))[zs.brca@rid]
zs.sd <- rowSd(zs.brca@mat)
gex.sd <- rowSd(gex.brca@mat)[zs.brca@rid]

ad10 <- symbol.to.id['ADAM10']

outlier.thresh <- 3
num.outlier <- apply(zs.brca@mat, 1, function(x) sum(x >= outlier.thresh) + sum(x <= (outlier.thresh * -1)))
num.outlier.pos <- apply(zs.brca@mat, 1, function(x) sum(x >= outlier.thresh))
num.outlier.neg <- apply(zs.brca@mat, 1, function(x) sum(x <= (outlier.thresh * -1)))
num.outlier.4 <- apply(zs.brca@mat, 1, function(x) sum(x >= outlier.thresh+1) + sum(x <= (outlier.thresh+1 * -1)))
num.outlier.pos.4 <- apply(zs.brca@mat, 1, function(x) sum(x >= outlier.thresh+1))
num.outlier.neg.4 <- apply(zs.brca@mat, 1, function(x) sum(x <= (outlier.thresh+1 * -1)))

# number of samples with zero expression
hist(log10(gex.nz), xlab='log10 BRCA samples with zero expression, per gene', breaks=30, ylim=c(0,350), col='grey', main='BRCA samples with zero expression, per gene \n genes with at leaset one zero sample')
hist((gex.nz), xlab='BRCA samples with zero expression, per gene', breaks=30, col='grey', main='BRCA samples with zero expression, per gene')
 # number of outliers vs number of zero samples for that gene
plot(gex.nz, num.outlier, main='BRCA: samples at zero vs outlier samples, per gene', xlab='number of samples with zero values', ylab='number of outlier samples')
plot(gex.nz, num.outlier.pos, main='BRCA: samples at zero vs +outlier samples, per gene', xlab='number of samples with zero values', ylab='number of positive outlier samples')
plot(gex.nz, num.outlier.neg, main='BRCA: samples at zero vs -outlier samples, per gene', xlab='number of samples with zero values', ylab='number of negative outlier samples')
hist(num.outlier)
# gene with the most outlier samples
# 206834_at
# g <- '206834_at'
# g <- '207482_at'
# g <- '207936_x_at'
g <- '205496_at'
g.n <- id.to.symbol[g]
par(mfrow=c(1,2))
hist(gex.brca@mat[g,], main=paste('expression of', g.n), xlab='log2 expression', col='grey')
hist(zs.brca@mat[g,], main = paste('zscore of', g.n), xlab='zscore', col='grey')
# what if we set outlier threshold depending on number of zeros?
# set it as the original threshold / (fraction nonzero)^2
par(mfrow=c(1,1))
outlier.thresh.val <- outlier.thresh / (((1093:1)/1093)**2)
outlier.thresh.val[outlier.thresh.val > 100] <- 100
plot(1:1000/1093, log10(outlier.thresh.val[1:1000]), ylab='log10(outlier threshold)', xlab='nonzero sample fraction', type='l', lwd=5, col='steelblue')
plot(1:200, outlier.thresh.val[1:200], xlab='log10(outlier threshold)', ylab='nonzero samples')

# recalculate the number of outlier samples
outlier.thresh.scaled <- outlier.thresh / (((ncol(gex.brca@mat)-gex.nz)/ncol(gex.brca@mat))**2)
# cap it at 100
outlier.thresh.scaled[outlier.thresh.scaled>100] <- 100
hist(outlier.thresh.scaled)
num.outlier.scaled <- sapply(1:nrow(zs.brca@mat), function(i) sum(zs.brca@mat[i,] >= outlier.thresh.scaled[i]) + sum(zs.brca@mat[i,] <= (outlier.thresh.scaled[i] * -1)))
num.outlier.pos.scaled <- sapply(1:nrow(zs.brca@mat), function(i) sum(zs.brca@mat[i,] >= outlier.thresh.scaled[i]))
num.outlier.neg.scaled <- sapply(1:nrow(zs.brca@mat), function(i)  sum(zs.brca@mat[i,] <= (outlier.thresh.scaled[i] * -1)))
par(mfrow=c(1,1))
plot(gex.nz, num.outlier.scaled, main='BRCA: samples at zero vs outlier samples, per gene \n scaled threshold', xlab='number of samples with zero values', ylab='number of outlier samples')
plot(gex.nz, num.outlier.pos.scaled, main='BRCA: samples at zero vs +outlier samples, per gene \n scaled threshold', xlab='number of samples with zero values', ylab='number of positive outlier samples')
plot(gex.nz, num.outlier.neg.scaled, main='BRCA: samples at zero vs -outlier samples, per gene \n scaled threshold', xlab='number of samples with zero values', ylab='number of negative outlier samples')

# what aravind wants: gene expression plots that justify the outlier sets
# expression of all genes, expression of a gene with outliers, 
# zscores of all genes, zscores of a gene with outliers. 
gex.allden <- density(gex.brca@mat,na.rm = T)
zs.allden <- density(zs.brca@mat,na.rm = T)
p <- subplot(plot_ly(x=gex.allden$x, y=gex.allden$y, mode='lines', fill='tozeroy', name='Gene expression') %>%
               layout(title='Distribution of gene expression in BRCA <br> 1093 samples, 12276 genes',
                      xaxis=list(title='log2 RSEM GEX', range=list(0,20)), yaxis=list(title='Density')),
             plot_ly(x=zs.allden$x, y=zs.allden$y, mode='lines', fill='tozeroy', name='Z-score') %>%
               layout(title='Distribution of expression values in BRCA <br> 1093 samples, 12276 genes',
                      xaxis=list(title='Z-score', range=list(-5,5)), yaxis=list(title='Density')),shareX = F,titleX = T)
             
p
plotly_POST(p, filename = 'BRCA gex Z-score distribution', sharing = 'private')


# some genes are low variance, and dont have few outlier samples above a threshold
# 221264_s_at is the first gene with limited number of zeros
mingene <- '220006_at'
mingene.sym <- ps2genesym(mingene)

zs.brca.ge3 <- apply(zs.brca@mat, 1, function(x) sum(abs(x) >=3))
par(mfrow=c(1,2))
hist(gex.brca@mat[mingene,])
hist(zs.brca@mat[mingene,])
gex.minden <- density(gex.brca@mat[mingene,],na.rm = T)
zs.minden <- density(zs.brca@mat[mingene,],na.rm = T)
p <- subplot(plot_ly(x=gex.minden$x, y=gex.minden$y, mode='lines', fill='tozeroy', name='Gene expression') %>%
               layout(title=paste('Distribution of', mingene.sym, 'expression in 1093 BRCA samples'),
                      xaxis=list(title='log2 RSEM GEX', range=list(0,13)), yaxis=list(title='Density')),
             plot_ly(x=zs.minden$x, y=zs.minden$y, mode='lines', fill='tozeroy', name='Z-score') %>%
               layout(xaxis=list(title='Z-score', range=list(-4,4)), yaxis=list(title='Density')),shareX = F,titleX = T)

p
plotly_POST(p, filename ='non outlier BRCA gex Z-score distribution', sharing = 'private')

# some gene have many outlier samples above a threshold
zs.threshold <- 3
# positive example
posgene.sym <- 'CTTN'
posgene <- symbol.to.id[posgene.sym]
gex.posden <- density(gex.brca@mat[posgene,],na.rm = T)
zs.posden <- density(zs.brca@mat[posgene,],na.rm = T)

outlier.ind <- zs.posden$x > (zs.threshold)
not.outlier.ind <- !outlier.ind
# make them overlap on the last point
not.outlier.ind[head(which(not.outlier.ind==F), n=1)] <- T 
zs.posden.out <- list(x=zs.posden$x[outlier.ind],y=zs.posden$y[outlier.ind])
zs.posden.noout <- list(x=zs.posden$x[not.outlier.ind],y=zs.posden$y[not.outlier.ind])

p <- subplot(plot_ly(x=gex.posden$x, y=gex.posden$y, mode='lines', fill='tozeroy', name='Gene expression', type='scatter') %>%
               layout(title=paste('Distribution of', posgene.sym, 'expression in 1093 BRCA samples'),
                      xaxis=list(title='log2 RSEM GEX', range=list(9,18)), yaxis=list(title='Density')),
             plot_ly(x=zs.posden$x, y=zs.posden$y, mode='lines', fill='tozeroy', name='Z-score', type='scatter') %>%
               add_trace(x=zs.posden.out$x, y=zs.posden.out$y, mode='lines', fill='tozeroy', name='outliers', line=list(color='firebrick')) %>%
               layout(xaxis=list(title='Z-score', range=list(-5,10)), yaxis=list(title='Density')),shareX = F,titleX = T)

p
plotly_POST(p, filename ='positive outlier BRCA gex Z-score distribution', sharing = 'private')

# negative example
neggene.sym <- 'ADAM10'
neggene <- symbol.to.id[neggene.sym]
gex.negden <- density(gex.brca@mat[neggene,],na.rm = T)
zs.negden <- density(zs.brca@mat[neggene,],na.rm = T)

outlier.ind <- zs.negden$x < -(zs.threshold)
not.outlier.ind <- !outlier.ind
# make them overlap on the last point
not.outlier.ind[tail(which(not.outlier.ind==F), n=1)] <- T 

zs.negden.out <- list(x=zs.negden$x[outlier.ind],y=zs.negden$y[outlier.ind])
zs.negden.noout <- list(x=zs.negden$x[not.outlier.ind],y=zs.negden$y[not.outlier.ind])
p <- subplot(plot_ly(x=gex.negden$x, y=gex.negden$y, mode='lines', fill='tozeroy', name='Gene expression', type='scatter') %>%
               layout(title=paste('Distribution of', neggene.sym, 'expression in 1093 BRCA samples'),
                      xaxis=list(title='log2 RSEM GEX', range=list(5,13)), yaxis=list(title='Density')),
             plot_ly(x=zs.negden.noout$x, y=zs.negden.noout$y, mode='lines', fill='tozeroy', type = 'scatter', name='Z-score') %>%
               add_trace(x=zs.negden.out$x, y=zs.negden.out$y,mode='lines', fill='tozeroy', name='outliers', line=list(color='firebrick')) %>%
               layout(xaxis=list(title='Z-score', range=list(-9,4)), yaxis=list(title='Density')),
             shareX = F,titleX = T)

p
plotly_POST(p, filename ='negative outlier BRCA gex Z-score distribution', sharing = 'private')




# also: TSNE plots of how things look before and after zscoring
## t-SNE of gene expresison - TCGA BRCA
ts.brca <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/tsne/bing/brca/tsne_n2x1093.gctx')
ts.brca@rdesc$name_15 <- sapply(ts.brca@rdesc$bar_code, function(x) substr(x, 1,15))
brca.subtypes <- read.table('~/cmap_tcga/clinical/brca_pam50_subtypes.tsv' ,sep='\t', header=T)
brca.subtypes[brca.subtypes[,2]=="",2] <- "NA"
rownames(brca.subtypes) <- brca.subtypes[,1]
ts.brca@rdesc$subtype <- sapply(ts.brca@rdesc$name_15, function(x) brca.subtypes[x,2])
# nmf subtypes
nmf <- read.table('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/nmf/bestcluster_calls.tsv', sep='\t', header=T)
rownames(nmf) <- nmf[,1]
ts.brca@rdesc$cluster <- nmf[ts.brca@rid, 'cluster']
ts.brca.df <- as.data.frame(ts.brca@mat)
ts.brca.df <- cbind(ts.brca.df, ts.brca@rdesc)
ts.brca.df$cluster <- as.factor(ts.brca.df$cluster)

# tsne after zscoring
ts.zs.brca <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/tsne_on_zs/bing/brca/tsne_n2x1093.gctx')
ts.zs.brca@rdesc$name_15 <- sapply(ts.zs.brca@rdesc$bar_code, function(x) substr(x, 1,15))
ts.zs.brca@rdesc$subtype <- sapply(ts.zs.brca@rdesc$name_15, function(x) brca.subtypes[x,2])
ts.zs.brca@rdesc$cluster <- nmf[ts.zs.brca@rid, 'cluster']
ts.zs.brca.df <- as.data.frame(ts.zs.brca@mat)
ts.zs.brca.df <- cbind(ts.zs.brca.df, ts.zs.brca@rdesc)
ts.zs.brca.df$cluster <- as.factor(ts.zs.brca.df$cluster)

# making ggplots
p1 <- ggplot(ts.brca.df) + geom_point(aes(x=TS1, y=TS2, group=subtype, color=subtype)) +
  scale_color_brewer(palette = 'Dark2') + ggtitle('Gene expression') + guides(fill=F) + 
  theme_bw() 
p1.1 <- ggplot(ts.brca.df) + geom_point(aes(x=TS1, y=TS2, group=cluster, color=cluster)) +
  scale_color_brewer(palette = 'Spectral') + guides(fill=F) + # ggtitle('t-SNE on BING gene expression - BRCA data') +
  theme_bw() 
p2 <- ggplot(ts.zs.brca.df) + geom_point(aes(x=TS1, y=TS2, group=subtype, color=subtype)) +
  scale_color_brewer(palette = 'Dark2') + ggtitle('Z-scored expression') +
  theme_bw() 
p2.1 <- ggplot(ts.zs.brca.df) + geom_point(aes(x=TS1, y=TS2, group=cluster, color=cluster)) +
  scale_color_brewer(palette = 'Spectral') + #ggtitle('t-SNE on BING gene expression - BRCA data') +
  theme_bw() 

multiplot(p1,p1.1,p2,p2.1, cols=2)

# old plotly versions
# 
# p1 <- plot_ly(x=ts.brca@mat[,1], y=ts.brca@mat[,2], type='scatter', 
#               # color=as.factor(ts.brca@rdesc$subtype), colors=head(magma(length(unique(ts.brca@rdesc$subtype))+1),length(unique(ts.brca@rdesc$subtype))),
#               color=as.factor(ts.brca@rdesc$subtype), colors='Dark2',
#               mode='markers', text=sapply(ts.brca@rdesc$bar_code, function(x) substr(x,1,15)))
# p1 <- layout(p1, hovermode='closest', title=paste('t-SNE on BING gene expression - BRCA data'),
#              xaxis=list(title='TS1'), yaxis=list(title='TS2'))
# p1
# 
# # color this by transcriptional cluster
# # how does this look with NMF subtypes?
# p1.1 <- plot_ly(x=ts.brca@mat[,1], y=ts.brca@mat[,2], type='scatter', 
#              color=as.factor(ts.brca@rdesc$cluster), colors='Set1',
#              mode='markers', text=sapply(ts.brca@rdesc$bar_code, function(x) substr(x,1,15)))
# p1.1 <- layout(p1.1, hovermode='closest', title=paste('t-SNE of BING gene expression - BRCA data'),
#             xaxis=list(title='TS1'), yaxis=list(title='TS2'))
# p1.1
# 
# 
# 
# p2 <- plot_ly(x=ts.zs.brca@mat[,1], y=ts.zs.brca@mat[,2], type='scatter', 
#              color=as.factor(ts.zs.brca@rdesc$subtype), colors=viridis(length(unique(ts.zs.brca@rdesc$subtype))),
#              mode='markers', text=sapply(ts.zs.brca@rdesc$bar_code, function(x) substr(x,1,15)))
# p2 <- layout(p2, hovermode='closest', title=paste('t-SNE on  gene expression - BRCA data'),
#             xaxis=list(title='TS1'), yaxis=list(title='TS2'))
# p2
# 
# p2.1 <- plot_ly(x=ts.zs.brca@mat[,1], y=ts.zs.brca@mat[,2], type='scatter', 
#              color=as.factor(ts.zs.brca@rdesc$cluster), colors='Set1',
#              mode='markers', text=sapply(ts.zs.brca@rdesc$bar_code, function(x) substr(x,1,15)))
# p2.1 <- layout(p2.1, hovermode='closest', title=paste('t-SNE on z-scored gene expression - BRCA data'),
#             xaxis=list(title='TS1'), yaxis=list(title='TS2'))
# p2.1
# 
# subplot(p1,p2,p1.1,p2.1, nrows = 2,shareX = T, shareY = T)


