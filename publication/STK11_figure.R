# STK11 figures for paper 
# Expression and Z-score 
library(RColorBrewer)
library(gplots)
source('~/projects/techno_sample/shiny_apps/common_functions.R')
overlap.mat <- as.matrix(read.table(return_files('cosmic_overlap_matrix'), sep='\t', header = T, row.names = 1))
jaccard.mat <- as.matrix(read.table(return_files('cosmic_jaccard_matrix'), sep='\t', header = T, row.names = 1))

# extract STK11 rows
STK11.rows <- grep('STK11', rownames(jaccard.mat), value = T)

STK11.overlap <- overlap.mat[STK11.rows, ]
STK11.jaccard <- jaccard.mat[STK11.rows, ]
head(STK11.overlap[6, order(STK11.overlap[6,], decreasing = T)])
head(STK11.jaccard[6, order(STK11.jaccard[6,], decreasing = T)])
overlap.mat[STK11.rows[4:6], STK11.rows[4:6]]

# sample set iformation
ss <- parse.gmt('~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/gene_centric_aggregate_cosmic/query_up.gmt')
STK11.cohorts <- sapply(ss$STK11_PANCAN_NEG_ZMIN4_58$entry, function(x) strsplit(x, ':')[[1]][2])
par(mar=c(6,4,4,4))
getPal <- colorRampPalette(rev(brewer.pal(9,'Blues')))

barplot(sort(table(STK11.cohorts), decreasing = T), las=2, 
        main='Members of STK11 negative outlier set, by cohort', ylab = '# samples', 
        col=getPal(length(table(STK11.cohorts))))

# str(ss[grep('STK11', names(ss))])
# 140 141 142 781 782 783
# cna_gain cna_gain_deep cna_loss cna_loss_deep mutations_all OUTLIER_NEG

# lets make a combined gene set from these
STK11.combined <- list(list(head='STK11_alteration', desc='mutation OR cna_loss_deep OR negative outlier', 
                            entry=unique(c(ss[[781]]$entry, ss[[782]]$entry, ss[[783]]$entry))))

dir.create('~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/misc/STK11_combined')
write.gmt(STK11.combined, '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/misc/STK11_combined/query_up.gmt')


# make a heatmap of how many samples are in commmon with each of these sets
ss.overlap <- matrix(overlap.mat[STK11.rows[4:6], STK11.rows[4:6]], nrow=3, ncol=3,
                     dimnames=list(list('Deep deletion (95)', 'Mutations (69)', 'Negative outlier (58)'),
                                   list('Deep deletion (95)', 'Mutations (69)', 'Negative outlier (58)')))
# ss.overlap[1,2] <- length(intersect(ss[[782]]$entry, ss[[781]]$entry))
# ss.overlap[2,1] <- length(intersect(ss[[782]]$entry, ss[[781]]$entry))
# ss.overlap[1,3] <- length(intersect(ss[[782]]$entry, ss[[783]]$entry))
# ss.overlap[3,1] <- length(intersect(ss[[782]]$entry, ss[[783]]$entry))
# ss.overlap[2,3] <- length(intersect(ss[[781]]$entry, ss[[783]]$entry))
# ss.overlap[3,2] <- length(intersect(ss[[781]]$entry, ss[[783]]$entry))
diag(ss.overlap) <- NA
ss.overlap.text <- as.data.frame(ss.overlap)
diag(ss.overlap.text) <- ''

mybreaks= (0:50)/50
names(mybreaks) <- bluered(101)[51:101]
heatmap.2(ss.overlap, breaks=mybreaks, col=names(mybreaks)[1:50], na.color = 'grey90', trace='none', Rowv=NA, Colv = NA, dendrogram = 'none', 
          cellnote = ss.overlap.text, notecex = 1.2, notecol = 'black', margins = c(11,11), cexRow = 1.2, cexCol = 1.2, key = F,
          rowsep = 1:3, colsep = 1:3, sepwidth = c(0.03, 0.03), sepcolor = 'grey95')

ss.spearman <- matrix(c(NA, .1082, .0508, .1082, NA, 0.4283, 0.0508 , .4283, NA), nrow=3, ncol=3, byrow=T, dimnames = dimnames(ss.overlap))
ss.spearman.text <- as.data.frame(ss.spearman)
diag(ss.spearman.text) <- ''
mybreaks= (0:50)/50
names(mybreaks) <- bluered(101)[51:101]
heatmap.2(ss.spearman, breaks=mybreaks, col=names(mybreaks)[1:50], na.color = 'grey90', trace='none', Rowv=NA, Colv = NA, dendrogram = 'none', 
          cellnote = ss.spearman.text, notecex = 1.2, notecol = 'black', margins = c(11,11), cexRow = 1.2, cexCol = 1.2, key = F,
          rowsep = 1:3, colsep = 1:3, sepwidth = c(0.03, 0.03), sepcolor = 'grey95')


ss.cmap <- matrix(c(91.9, 98.8, 99.9, -99.7, -96.9, -99.5), nrow=2, ncol=3, byrow=T, dimnames = list(list('STK11 KD', 'STK11 OE'), colnames(ss.spearman)))
ss.cmap.text <- matrix(c('91.9 (53)', '98.8 (10)', '99.9 (1)', '-99.7 (24)', '-96.9 (149)', '-99.5 (11)'), nrow=2, ncol=3, byrow = T)

mybreaks= (-100:100)
names(mybreaks) <- "#FFFFFF"
names(mybreaks)[191:201] <- bluered(22)[11:21]
names(mybreaks)[1:10] <- bluered(22)[2:11]
heatmap.2(ss.cmap, breaks=mybreaks, col=names(mybreaks)[1:200], na.color = 'grey90', trace='none', Rowv=NA, Colv = NA, dendrogram = 'none', 
          cellnote = ss.cmap.text, notecex = 1.2, notecol = 'black', margins = c(11,11), cexRow = 1.2, cexCol = 1.2, key = F,
          rowsep = 1:3, colsep = 1:3, sepwidth = c(0.03, 0.03), sepcolor = 'grey95')


