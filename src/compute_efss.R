# compute the effective set size ratio for a number of sample sets against the CMap*TCGA matrix
library(argparser)
source('~/projects/techno_sample/shiny_apps/gsea_plot.R')
parser <- arg_parser('compute the effective set size ratio for a number of sample sets against the CMap*TCGA matrix')
parser <- add_argument(parser, 'gmt', 'gmt file of sets of TCGA samples')
parser <- add_argument(parser, 'outfile', 'name of output file')
parser <- add_argument(parser, '--num_cores', 'parallelize computation across this many cores', default = 1)
parser <- add_argument(parser, '--cmaptcga', 'sepecify a different cmap*tcga matrix here', default='/cmap/data/vdb/pdex/lm/build/pdex_cmap_ps_n82902x10255.gctx')
parser <- add_argument(parser, '--lfeu', 'compute the leading fraction instead of the efsr', flag=T)

arguments <- parse_args(parser)

# read cmap*tcga matrix
ct <- parse.gctx(arguments$cmaptcga, cid=limit.pert)
# ensure ct has no NAs
if(any(is.na(ct@mat))){
  warning('CMap*TCGA matrix has NA values. these are being converted to zeros')
  ct@mat[is.na(ct@mat)] <- 0
}

# read gmt file
gmt <- parse.gmt(arguments$gmt)
set.names <- sapply(gmt, function(x) x$head)

if(arguments$num_cores >1){
  message(paste('Starting computation in parallel mode with', arguments$num_cores, 'cores.'))
  library(parallel)
  cl <- makeCluster(arguments$num_cores, type="FORK")
  start.time <- Sys.time()
  
  if (!arguments$lefu){
    # compute EFSR
    sap.mat <- parSapply(cl, 1:length(gmt), function(i) {
      this.set <- gmt[[i]]$entry
      # only the relevant samples
      ct.sub <- ct@mat[this.set,]
      apply(ct.sub, 2, function(x) sum(x!=0)) / length(this.set)
    })
  } else {
    # compute LEFU
    sap.mat <- t(parSapply(cl, 1:ncol(ct@mat), function(i){
      ord <- order(ct@mat[,i], decreasing = T)
      sapply(gmt, function(x) GSEA.EnrichScore(ct@rid[ord], x$entry, ct@mat[ord,i], do.lefu=T)$LEFU)
    }))
    colnames(sap.mat) <- set.names
    
  }
  
  stopCluster(cl)
  end.time <- Sys.time()
  total.time <- end.time-start.time
  message("Time for calculation:")
  print(total.time)
  message()
} else {
  message('Starting computation in single core mode.')
  start.time <- Sys.time()
  
  if(!arguments$lefu){
    # compute EFSR
    sap.mat <- sapply(1:length(gmt), function(i) {
      this.set <- gmt[[i]]$entry
      # only the relevant samples
      ct.sub <- ct@mat[this.set,]
      apply(ct.sub, 2, function(x) sum(x!=0)) / length(this.set)
    })
  } else {
    # compute leading fraction of the UP queries
    sap.mat <- t(sapply(1:ncol(ct@mat), function(i){
      ord <- order(ct@mat[,i], decreasing = T)
      sapply(gmt, function(x) GSEA.EnrichScore(ct@rid[ord], x$entry, ct@mat[ord,i], do.lefu=T)$LEFU)
    }))
    colnames(sap.mat) <- set.names
  
  }
  
  end.time <- Sys.time()
  total.time <- end.time-start.time
  message("Time for calculation:")
  print(total.time)
  message()
}


# set up a gctx to store the results
efss <- subset.gct(ct, 1, 1)
rownames(sap.mat) <- ct@cid
colnames(sap.mat) <- set.names
efss@mat <- sap.mat
efss@rid <- ct@cid
efss@rdesc <- ct@cdesc
efss@cid <- set.names
efss@cdesc <- data.frame(id=set.names, desc=sapply(gmt, function(x) x$desc))

write.gctx(efss, arguments$outfile)
message('DONE! :)')


# LEFU DEBUGGING ARGS
# arguments <- list(gmt='~/cmap_tcga/cmap_queries_lm/scale_LEFU/benchmark_alteration_sets/query_up.gmt',
#                   outfile='~/cmap_tcga/cmap_queries_lm/scale_LEFU/benchmark_alteration_sets/TEST_LEFU.gctx' ,
#                   cmaptcga='/cmap/data/vdb/pdex/lm/build/pdex_cmap_ps_n82902x10255.gctx',
#                   lefu=T, num_cores=4)
# limit.pert <- 1:1000
# lefu.compare <- parse.gctx('~/cmap_tcga/cmap_queries_lm/scale_LEFU/benchmark_alteration_sets/result_WTCS.CUSTOM.LEFU_n18x82902.gctx', rid=limit.pert)@mat
# cs <- parse.gctx('~/cmap_tcga/cmap_queries_lm/scale_LEFU/benchmark_alteration_sets/result_WTCS.CUSTOM.COMBINED_n18x82902.gctx', rid=limit.pert)@mat


# i <- 1
# this.set <- gmt[[i]]$entry
# # only the relevant samples
# ct.sub <- ct@mat[this.set,]
# efss@mat[,i] <- apply(ct.sub, 2, function(x) sum(x!=0)) / length(this.set)
# 
# microbenchmark(this.set <- gmt[[i]]$entry)
# ctmat <- ct@mat
# microbenchmark(ct.sub <- ct@mat[this.set,])
# microbenchmark(ct.sub <- ctmat[this.set,])
# microbenchmark(efss@mat[,i] <- apply(ct.sub, 2, function(x) sum(x!=0)) / length(this.set))
# 
# 
# # set up a gctx to store the results
# efss <- subset.gct(ct, 1, 1)
# efss@mat <- matrix(0, nrow=ncol(ct@mat), ncol=length(gmt), dimnames=list(ct@cid, set.names))
# efss@rid <- ct@cid
# efss@rdesc <- ct@cdesc
# efss@cid <- set.names
# efss@cdesc <- data.frame(id=set.names, desc=sapply(gmt, function(x) x$desc))
# 
# microbenchmark(sap.mat <- parSapply(cl, 1:100, function(i) {
#   this.set <- gmt[[i]]$entry
#   # only the relevant samples
#   ct.sub <- ct@mat[this.set,]
#   apply(ct.sub, 2, function(x) sum(x!=0)) / length(this.set)
# }))
# 
# sap.mat2 <- sapply(1:10, function(i) {
#   this.set <- gmt[[i]]$entry
#   # only the relevant samples
#   ct.sub <- ct@mat[this.set,]
#   apply(ct.sub, 2, function(x) sum(x!=0)) / length(this.set)
# })
