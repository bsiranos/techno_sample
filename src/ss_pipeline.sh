#!/bin/bash
source /cmap/tools/bashtk/bashtk -q
SCRIPT="${0##*/}"

# directory with techno_sample
tdir=~/projects/techno_sample/

# Defaults
percentileScores=""
ranks=""
sampleSets=""
targets=""
arf=$PWD
gmtAlt=""
ncores="1"

# #debugging
# #ARF to save in
# arf=query_result/mutations_10
# # limit samples to this grp
# relevantSamples=$cohort'_samples_mutations.grp'
# # gmt file to use
# gmt=query_gmt/mutations_10.gmt
# # gmt alterations file
# gmtalt=query_gmt/mutations_alterations_10.gmt
# # expected targets file
# targets=query_gmt/mutations_targets.txt
# # number of cores
# ncores=8

USAGE_HELP="--------------------------------------------------
$SCRIPT: Run the techno sample pipeline for a sample set
Using query results that have already been computed.
Author: Ben Siranosian [bsiranos@broadinstitute.org]
--------------------------------------------------
Usage: $SCRIPT -pert_cell_ <percentileScores> -r <ranks> -s <sampleSets> -t <targets> [options]

Options:
-p <gctx> transposed file of percentile scores from query of sample signatures against CMap. CMap perturbagens are in rows, samples are in columns. REQUIRED
-r <gctx> ranked percentile scores file. CMap perturbagens are in rows, samples are in columns. REQUIRED
-s <gmt> gmt file containing sample sets to test for enrichment, one set per row. REQUIRED
-t <grp> text file containing names of genes that correspond to expected targets for each sample set. REQUIRED
-l <grp> text file containing samples that have had the necessary analyses performed on them, might be a subset of all the samples in scores. REQUIRED
-o <path> output path. default is $arf 
-a <gmt> gmt file containing what alteration each sample in each set has. Useful for mutations to see what mutation a sample has in the figures. Must be ordered the sample as the sampleSets file. 
-n <integer> parallelize pvalue and other computations across this many cores.
-d Run a demo of the script using files in the /cmap/tools/sig_tools/assets folder

Description:
This script is used to run the techno_sample pipleine with a new sample set file. It uses query results that have already been computed. First, sig_query_tool is used to get wtcs values for each sample sets enrichment to each perturbagen. Then p-values are calculated using the WKS procedure. Finally the results are analyzed using a custom pipeline. The result is a folder that has all the necessary files to be loaded into the visualization webapp

Example:
$SCRIPT -p wtcs.gctx -r rank.gctx -s sampleSets.gmt -t targets.grp -r limitSamples.grp -o arf

"

# main 
# getopt stuff
getopt -T >/dev/null 2>&1
if [[ $? == 4 ]]; then
	OPTS=$(getopt -o p:r:s:t:l:o:a:n:dh -- "$@")
	eval set -- "$OPTS"
		while true ; do
		case "$1" in
				-d)
			percentileScores=/cmap/tools/sig_tools/assets/sig_splitpvalue_tool/pert_cell_brca_n1093x82087.gctx
			sampleSets=/cmap/tools/sig_tools/assets/sig_splitpvalue_tool/outliers.gmt
			arf=/cmap/tools/sig_tools/assets/sig_splitpvalue_tool/demo_out
			shift; break;;
				-p) 
			percentileScores="$2"
			shift 2;;
				-r) 
			ranks="$2"
			shift 2;;
				-s) 
			sampleSets="$2"
			shift 2;;
				-t) 
			targets="$2"
			shift 2;;
				-l) 
			relevantSamples="$2"
			shift 2;;
				-o) 
			arf="$2"
			shift 2;;
				-a) 
			gmtAlt="$2"
			shift 2;;
				-n) 
			ncores="$2"
			shift 2;;
				-h)
			msg_help;;
		--) shift; break;;
			*) echo "Internal error!"
			exit 1 ;;
		esac
		done

	if [[ -f $percentileScores && -f $ranks && -f $sampleSets && -f $targets ]]; then
	if [[ ! -d $arf ]]; then
		mkdir -p $arf
	fi


	echo "ps: "$percentileScores
	echo "ranks: "$ranks
	echo "ss: "$sampleSets
	echo "targets : "$targets
	echo "relevantSamples : "$relevantSamples
	echo "arf: "$arf
	echo "gmtAlt: "$gmtAlt
	echo "ncores: "$ncores

	# query to get enrichment.
	rum sig_query_tool  --score $percentileScores --rank $ranks --es_tail up --metric wtcs --row_space custom --rid $relevantSamples --column_space full --save_minimal 1  --save_query 0 --uptag $sampleSets --out $arf --mkdir 0 || exit 1

	# copy things into the arf
	cp $sampleSets $arf/sample_sets.gmt
	cp $relevantSamples $arf/relevant_samples.grp
	cp $targets $arf/expected_targets.txt
	if [[ -f $gmtAlt ]]; then 
		cp $gmtalt $arf/sample_sets_alterations.gmt
	fi

	# calculate pvalues
	Rscript $tdir/tools/calculate_pvalues_wks.R $percentileScores $sampleSets $arf --num_cores $ncores || exit 1 

	# analyze enrichment 
	Rscript $tdir/tools/analyze_enrichment.R $pert_t_mut $arf/sample_sets.gmt $arf/result_WTCS.CUSTOM.COMBINED_n*x82891.gctx  $arf/WKS_pvalues_n*x82891.gctx $arf --expected_connections --relevant_line all --expected_inames $arf/expected_targets.txt --num_cores $ncores || exit 1
	else
	[[ -f $percentileScores ]] || msg "percentileScores file not found: $percentileScores" red;
	[[ -f $ranks ]] || msg "ranks file not found: $ranks" red;
	[[ -f $sampleSets ]] || msg "sampleSets file not found: $sampleSets" red;
	[[ -f $targets ]] || msg "targets file not found: $targets" red;
	msg_help "Invalid Input"
	fi

else
	echo "Unsupported getopt found. Requires GNU-Getopt"
fi

exit $PIPE_STATUS
