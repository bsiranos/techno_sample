#!/bin/bash
source /cmap/tools/bashtk/bashtk -q
SCRIPT="${0##*/}"

# Defaults
percentileScores=""
sampleSets=""
outPath=$PWD
runScript="run_split_pvalue_wks.sh"
nsim="10000"
sampleDim="col"
batchPerts=500

USAGE_HELP="--------------------------------------------------
$SCRIPT: Setup WKS pvalue computation for parallel execution
Author: Ben Siranosian [bsiranos@broadinstitute.org]
--------------------------------------------------
Usage: $SCRIPT -p <percentileScores> -s <sampleSets> [options]

Options:
--ps|-p <gctx> file of percentile scores from query of sample signatures against CMap. Samples are in columns, CMap perturbagens are in rows. REQUIRED
--ss|-s <gmt> gmt file containing sample sets to test for enrichment, one set per row.
--out|-o <path> output path. default is $outPath
--nsim|-n <integer> The number of simulated random walks in the WKS procedure. Will give more stratified p-values at a cost of increased computation time. Default is 10000
--sampleDim|-d <row, col> Dimension of the percentile scores file corresponding to samples. Default is col. 
--batchPerts|-b <integer> number of perturbagens to calculate pvalues per batch. Default is $batchPerts
--demo|-m Run a demo of the script using files in the /cmap/tools/sig_tools/assets folder

Description:
This tool takes a percentile score file and sample sets and computes pvalues for enrichmnent using the WKS procedure in a parallelized fashion. The computation is split up into sets of perturbagens of the specified size

Example:
$SCRIPT --ps wtcs.gctx --ss outlier_samples.gmt --out outpath

"

function CreateRunScript(){
## Generate run script for executing single set o pvalue calcs
## Usage: CreateRunScript percentileScores sampleSets outPath nsim sampleDim
# Template variables
cat <<EOF
#!/bin/bash

function usage() {
    echo "\${0##*/} joblist.grp outPath [job_index]"
}


function get_cluster_type() {
    cluster_type=""
    if [[ -n \$SGE_CLUSTER_NAME ]]; then
	cluster_type='sge'
    elif [[ -n \$LSF_CLUSTER_NAME ]]; then
	cluster_type='lsf'
    fi
    echo \$cluster_type
}

cluster_type=\$(get_cluster_type)
case "\$cluster_type" in
    'sge')
	job_index=\${SGE_TASK_ID-\$3}	    
	;;
    'lsf')
	job_index=\${LSB_JOBINDEX-\$3}
	;;
    *)
	job_index=\$3
	;;
esac

if [[ -n "\$job_index" && -f "\$1"  && -n "\$2" ]]; then
    joblist=\$1
    outPath=\$2
    if [[ ! -d \$outPath ]]; then 
	mkdir -p "\$outPath"
    fi
    # get info on the start and end position
    job_info=\$(sed "\${job_index}q;d" \$joblist)
    set_index=\$(echo \$job_info | cut -d " " -f 1)
    pertStart=\$(echo \$job_info | cut -d " " -f 2)
    pertEnd=\$(echo \$job_info | cut -d " " -f 3)

    # should have something about the location of this script...
    Rscript ~/projects/techno_sample/tools/calculate_pvalues_wks.R \
    ${percentileScores} \
    ${sampleSets} \
    \$outPath/input_\${set_index} \
    --nsim ${nsim} \
    --sample_dim ${sampleDim} \
    --pert_start \$pertStart \
    --pert_end \$pertEnd
else
    [[ -n \$job_index ]] || echo 'ERROR! job array index not found or specified'
    usage
fi
EOF
}


# main 
# getopt stuff
getopt -T >/dev/null 2>&1
if [[ $? == 4 ]]; then
	OPTS=$(getopt -o p:s:o:n:d:b:mh --long ps:,ss:,out:,nsim:,sampleDim:,batchPerts,demo,help -- "$@")
	eval set -- "$OPTS"
		while true ; do
		case "$1" in
				-m|--demo)
			percentileScores=/cmap/tools/sig_tools/assets/sig_splitpvalue_tool/pert_cell_brca_n1093x82087.gctx
			sampleSets=/cmap/tools/sig_tools/assets/sig_splitpvalue_tool/outliers.gmt
			outPath=/cmap/tools/sig_tools/assets/sig_splitpvalue_tool/demo_out
			shift; break;;
				-p|--ps) 
			percentileScores="$2"
			shift 2;;
				-s|--ss) 
			sampleSets="$2"
			shift 2;;
				-o|--out) 
			outPath="$2"
			shift 2;;
				-n|--nsim) 
			nsim="$2"
			shift 2;;
				-d|--sampleDim)
			sampleDim="$2"
			shift 2;;
				-b|--batchPerts)
			batchPerts="$2"
			shift 2;;
				-h|--help)
			msg_help;;
		--) shift; break;;
			*) echo "Internal error!"
			exit 1 ;;
		esac
	    done

	if [[ -f $percentileScores && -f $sampleSets ]]; then
	if [[ ! -d $outPath ]]; then
	    mkdir -p $outPath
	fi
	if [[ ! -d $outPath/jobs ]]; then
	    mkdir $outPath/jobs
	fi

	echo "ps: "$percentileScores
	echo "ss: "$sampleSets
	echo "out: "$outPath
	echo "nsim: "$nsim
	echo "sampleDim: "$sampleDim
	echo "batchPerts: "$batchPerts

	# Find the number of perturbagens in the dataset
	# probably going to be 82087 but for future use...
	if [ "$sampleDim" == "col" ]; then
		psPerts=$( get_gctx_meta $percentileScores row | wc -l )
	elif [ "$sampleDim" == "row" ]; then
		psPerts=$( get_gctx_meta $percentileScores col | wc -l )
	else 
		echo "sampleDim must be one of {row, col}" ; exit 1
	fi 

	# create joblist 
	# remove it exists already
	if [ -f "$outPath/joblist.grp" ]; then
		echo "$outPath/joblist.grp exists, removing..."
		rm "$outPath/joblist.grp"
	fi
	# loop to create job and pet indices
	i=1
	while [[ $(( $i * $batchPerts )) -lt $psPerts ]]; do
		start=$(( $(( $i - 1 )) * $batchPerts + 1 ))
		end=$(( $i * $batchPerts ))
		echo -e $i'\t'$start'\t'$end >> "$outPath/joblist.grp"
		i=$(( i + 1 ))
	done
	# add last line if necessary
	if [ $(( $psPerts % $batchPerts )) -gt 0 ]; then
		start=$(( $(( $i - 1 )) * $batchPerts + 1 ))
		echo -e $i'\t'$start'\t'$psPerts >> "$outPath/joblist.grp"
	fi


	# Generate run script
	runScriptFile="$outPath/$runScript"
	msg "Generating run script: $runScriptFile"
	CreateRunScript $percentileScores $sampleSets $outPath $nsim $sampleDim > $runScriptFile
	[[ ${PIPESTATUS[0]} -eq 0 ]] || { msg "Error generating run script" red; status_die 1; }
	chmod +x $runScriptFile
	msg "Done." green

	msg "Verify the following invocation of query tool, make any changes to $runScriptFile" cyan 
	str_rep '#' 30
	grep -A 11 rum $runScriptFile
	str_rep '#' 30
	msg "The output is in: $(abs_path $outPath)" cyan
	msg "To execute queries on the cluster, use:"
	msg "batchx -i joblist.grp -p jobs -j ./run_single_query.sh -o <output path> -d <data path>" green
    else
	[[ -f $percentileScores ]] || msg "percentileScores file not found: $percentileScores" red;
	[[ -f $sampleSets ]] || msg "sampleSets file not found: $sampleSets" red;
	msg_help "Invalid Input"
    fi
else
    echo "Unsupported getopt found. Requires GNU-Getopt"
fi

exit $PIPE_STATUS
