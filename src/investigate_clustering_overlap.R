# examining outlier sample set clustering and overlap
options(stringsAsFactors = F)
source('~/projects/techno_sample/shiny_apps/common_functions.R')
arfs <- c('~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/mutations/',
          '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/cna_gain/',
          '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/cna_gain_deep/',
          '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/cna_loss/',
          '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/cna_loss_deep/')
arf.names <- c('mutations', 'cna_gain', 'cna_gain_deep', 'cna_loss', 'cna_loss_deep')
# arf <- '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/'

for (i in 1:length(arfs)){
  arf <- arfs[i]
  arf.name <- arf.names[i]
  print(paste('Starting:', arf.name))
  
  arf.files <- return_arf_files(arf)
  arf.data <- load_required_arf_files(arf.files, enrichment = F, pdex = F, ns = F, computed.ae=F)
    
  # summary statistics ------
  # task 3: do some summary stats on the sample stats for showing later
  # for each pair of sample sets, compute the intersection fraction 
  # as a function of the smaller one
  do.intersection <- T 
  nc <- length(arf.data$query_up)
  ss.lengths <- arf.data$query_stats$up_size[1:nc]
  ss.names <- arf.data$query_stats$query_id[1:nc]
  # ss.gene_dir <- sapply(ss.names, function(x) paste(strsplit(x, '_')[[1]][c(1,3)], collapse='_'))
  ss.dir <- sapply(ss.names, function(x) strsplit(x, '_')[[1]][c(3)])
  if(do.intersection){
    if(length(arf.data$query_up) > 1){
      message('doing overlap and jaccard calculations...')
      # to.compare <- combn(length(arf.data$query_up), 2)
      to.compare <- combn(nc, 2)
      ss.overlap <- apply(to.compare, 2, function(x) {
        length(intersect(arf.data$query_up[[x[1]]]$entry, arf.data$query_up[[x[2]]]$entry)) / min(ss.lengths[x[1]], ss.lengths[x[2]])
      })
      
      # jaccard index of sample sets
      ss.jaccard <- apply(to.compare, 2, function(x) {
        length(intersect(arf.data$query_up[[x[1]]]$entry, arf.data$query_up[[x[2]]]$entry)) / 
          length(union(arf.data$query_up[[x[1]]]$entry, arf.data$query_up[[x[2]]]$entry))
      })
    } else {
      ss.overlap <- NULL
      ss.jaccard <- NULL
    }
  } else {
    ss.overlap <- NULL
    ss.jaccard <- NULL
  }
  
  overlap.mat <- matrix(0, nrow = nc, ncol = nc, dimnames = list(ss.names, ss.names))
  overlap.mat[lower.tri(overlap.mat, diag = F)] <- ss.overlap
  overlap.mat <- t(overlap.mat)
  overlap.mat[lower.tri(overlap.mat, diag = F)] <- ss.overlap
  jaccard.mat <- matrix(0, nrow = nc, ncol = nc, dimnames = list(ss.names, ss.names))
  jaccard.mat[lower.tri(jaccard.mat, diag = F)] <- ss.jaccard
  jaccard.mat <- t(jaccard.mat)
  jaccard.mat[lower.tri(jaccard.mat, diag = F)] <- ss.jaccard
  
  # overlap.mat.pos <- overlap.mat[ss.dir=='POS', ss.dir=='POS']
  # overlap.mat.neg <- overlap.mat[ss.dir=='NEG', ss.dir=='NEG']
  # jaccard.mat.pos <- jaccard.mat[ss.dir=='POS', ss.dir=='POS']
  # jaccard.mat.neg <- jaccard.mat[ss.dir=='NEG', ss.dir=='NEG']
  
  # write.table(overlap.mat.pos, '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/overlap_mat_pos.txt', sep='\t', quote=F, row.names = T, col.names = T)
  # write.table(overlap.mat.neg, '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/overlap_mat_neg.txt', sep='\t', quote=F, row.names = T, col.names = T)
  # write.table(jaccard.mat.pos, '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/jaccard_mat_pos.txt', sep='\t', quote=F, row.names = T, col.names = T)
  # write.table(jaccard.mat.neg, '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/jaccard_mat_neg.txt', sep='\t', quote=F, row.names = T, col.names = T)
  outdir <- path.join(arf, 'overlap_matrices')
  if(!dir.exists(outdir)){dir.create(outdir)}
  write.table(round(overlap.mat,3), path.join(outdir, 'overlap_mat.txt'), sep='\t', quote=F, row.names = T, col.names = T)
  write.table(round(jaccard.mat,3), path.join(outdir, 'jaccard_mat.txt'), sep='\t', quote=F, row.names = T, col.names = T)
  
  # par(mfrow=c(4,2), mar=c(2,4,4,4))
  # hist(overlap.mat.pos[upper.tri(overlap.mat.pos)], breaks=50, main='Overlap fraction, POS outliers', xlim=c(0,1), xlab='')
  # hist(jaccard.mat.pos[upper.tri(jaccard.mat.pos)], breaks=50, main='Jaccard index, POS outliers', xlim=c(0,1), xlab='')
  # hist(overlap.mat.neg[upper.tri(overlap.mat.neg)], breaks=50, main='Overlap fraction, NEG outliers', xlim=c(0,1), xlab='')
  # hist(jaccard.mat.neg[upper.tri(jaccard.mat.neg)], breaks=50, main='Jaccard index, NEG outliers', xlim=c(0,1), xlab='')
  # hist(apply(overlap.mat.pos,1,max), breaks=50, main=paste('Max overlap of each set,', 'POS outliers'), xlim=c(0,1), xlab='')
  # hist(apply(jaccard.mat.pos,1,max), breaks=50, main=paste('Max jaccard of each set,', 'POS outliers'), xlim=c(0,1), xlab='')
  # hist(apply(overlap.mat.neg,1,max), breaks=50, main=paste('Max overlap of each set,', 'NEG outliers'), xlim=c(0,1), xlab='')
  # hist(apply(jaccard.mat.neg,1,max), breaks=50, main=paste('Max jaccard of each set,', 'NEG outliers'), xlim=c(0,1), xlab='')
  png(path.join(outdir, 'overlap_hist.png'),width = 800, height=600)
  par(mfrow=c(2,2), mar=c(2,4,4,4))
  hist(overlap.mat[upper.tri(overlap.mat)], breaks=50, main=paste('Overlap fraction,',arf.name), xlim=c(0,1), xlab='')
  hist(jaccard.mat[upper.tri(jaccard.mat)], breaks=50, main=paste('Jaccard index,',arf.name), xlim=c(0,1), xlab='')
  hist(apply(overlap.mat,1,max), breaks=50, main=paste('Max overlap of each set,', arf.name), xlim=c(0,1), xlab='')
  hist(apply(jaccard.mat,1,max), breaks=50, main=paste('Max jaccard of each set,', arf.name), xlim=c(0,1), xlab='')
  dev.off()
  # 
  # # find the top n correlated pairs
  # overlap.mat.pos.nb <- overlap.mat.pos
  # overlap.mat.pos.nb[lower.tri(overlap.mat.pos.nb)] <- 0
  # overlap.mat.neg.nb <- overlap.mat.neg
  # overlap.mat.neg.nb[lower.tri(overlap.mat.neg.nb)] <- 0
  # jaccard.mat.pos.nb <- jaccard.mat.pos
  # jaccard.mat.pos.nb[lower.tri(jaccard.mat.pos.nb)] <- 0
  # jaccard.mat.neg.nb <- jaccard.mat.neg
  # jaccard.mat.neg.nb[lower.tri(jaccard.mat.neg.nb)] <- 0
  # 
  # pick.mats <- list(overlap.mat.pos.nb, overlap.mat.neg.nb, jaccard.mat.pos.nb, jaccard.mat.neg.nb)
  # outnames <- c('~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/top_overlap_pos.txt', 
  #               '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/top_overlap_neg.txt', 
  #               '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/top_jaccard_pos.txt', 
  #               '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/outliers_adaptive_z4/overlap_matrices/top_jaccard_neg.txt')
  # for (i in 1:4){
  #   pick.mat <- pick.mats[[i]]
  #   idx.col <- matrix(colnames(pick.mat), nrow=nrow(pick.mat), ncol=ncol(pick.mat), byrow = T)
  #   idx.row <- matrix(rownames(pick.mat), nrow=nrow(pick.mat), ncol=ncol(pick.mat), byrow = F)
  #   idx <- matrix(paste(idx.col, idx.row, sep = '/'), nrow=nrow(pick.mat), ncol=ncol(pick.mat), byrow = T)
  #   topn <- 100
  #   topinds <- order(pick.mat, decreasing = T)[1:topn]
  #   topv <- pick.mat[topinds]
  #   names(topv) <- idx[topinds]
  #   # topv
  #   genes1 <- sapply(names(topv), function(x) strsplit(strsplit(x, '/')[[1]][1], '_')[[1]][1])
  #   genes2 <- sapply(names(topv), function(x) strsplit(strsplit(x, '/')[[1]][2], '_')[[1]][1])
  #   topvdf <- data.frame(gene1=genes1, gene2=genes2, metric=round(topv,3))
  #   write.table(topvdf, outnames[i],
  #               sep='\t', col.names = T, row.names = F, quote=F)
  # }
  
  # find the top n correlated pairs
  overlap.mat.nb <- overlap.mat
  overlap.mat.nb[lower.tri(overlap.mat.nb)] <- 0
  jaccard.mat.nb <- jaccard.mat
  jaccard.mat.nb[lower.tri(jaccard.mat.nb)] <- 0
  
  pick.mats <- list(overlap.mat.nb, jaccard.mat.nb)
  outnames <- c(path.join(outdir, 'top_overlap.txt'),path.join(outdir, 'top_jaccard.txt'))
                
  for (j in 1:2){
    pick.mat <- pick.mats[[j]]
    idx.col <- matrix(colnames(pick.mat), nrow=nrow(pick.mat), ncol=ncol(pick.mat), byrow = T)
    idx.row <- matrix(rownames(pick.mat), nrow=nrow(pick.mat), ncol=ncol(pick.mat), byrow = F)
    idx <- matrix(paste(idx.col, idx.row, sep = '/'), nrow=nrow(pick.mat), ncol=ncol(pick.mat), byrow = T)
    topn <- 100
    topinds <- order(pick.mat, decreasing = T)[1:topn]
    topv <- pick.mat[topinds]
    names(topv) <- idx[topinds]
    # topv
    genes1 <- sapply(names(topv), function(x) strsplit(strsplit(x, '/')[[1]][1], '_')[[1]][1])
    genes2 <- sapply(names(topv), function(x) strsplit(strsplit(x, '/')[[1]][2], '_')[[1]][1])
    rownames(arf.data$query_stats) <- arf.data$query_stats$query_id
    gene1_set_size <- arf.data$query_stats[sapply(names(topv), function(x) strsplit(x, '/')[[1]][1]), 'up_size']
    gene2_set_size <- arf.data$query_stats[sapply(names(topv), function(x) strsplit(x, '/')[[1]][2]), 'up_size']
    topvdf <- data.frame(gene1=genes1, gene1_set_size=gene1_set_size, gene2=genes2, gene2_set_size=gene2_set_size, metric=round(topv,3))
    write.table(topvdf, outnames[j],
                sep='\t', col.names = T, row.names = F, quote=F)
  }
  

}

# heatmap.2(jaccard.mat, trace='none')
# od <- dist(overlap.mat.pos)
plot(hclust(as.dist(1-overlap.mat.pos)))
plot(hclust(as.dist(1-jaccard.mat.pos)), labels = NULL)
heatmap.2(jaccard.mat.pos, trace='none', distfun = function(x) as.dist(1-x))
#

# omp.cor <- bigcor(overlap.mat.pos, nblocks = 8)
omp.cor <- cor(overlap.mat.pos)




bigcor <- function(x, nblocks = 10, verbose = TRUE, ...)
{
  library(ff, quietly = TRUE)
  NCOL <- ncol(x)
  
  ## test if ncol(x) %% nblocks gives remainder 0
  if (NCOL %% nblocks != 0) stop("Choose different 'nblocks' so that ncol(x) %% nblocks = 0!")
  
  ## preallocate square matrix of dimension
  ## ncol(x) in 'ff' single format
  corMAT <- ff(vmode = "single", dim = c(NCOL, NCOL))
  
  ## split column numbers into 'nblocks' groups
  SPLIT <- split(1:NCOL, rep(1:nblocks, each = NCOL/nblocks))
  
  ## create all unique combinations of blocks
  COMBS <- expand.grid(1:length(SPLIT), 1:length(SPLIT))
  COMBS <- t(apply(COMBS, 1, sort))
  COMBS <- unique(COMBS)
  
  ## iterate through each block combination, calculate correlation matrix
  ## between blocks and store them in the preallocated matrix on both
  ## symmetric sides of the diagonal
  for (i in 1:nrow(COMBS)) {
    COMB <- COMBS[i, ]
    G1 <- SPLIT[[COMB[1]]]
    G2 <- SPLIT[[COMB[2]]]
    if (verbose) cat("Block", COMB[1], "with Block", COMB[2], "\n")
    flush.console()
    COR <- cor(x[, G1], x[, G2], ...)
    corMAT[G1, G2] <- COR
    corMAT[G2, G1] <- t(COR)
    COR <- NULL
  }
  
  gc()
  return(corMAT)
}












# signature strength computation -------
if(do.sigstrength){
  message('doing signature strenght calculations')
  # Things related to signature strength
  # make a bunch of random sample sets - 2x the number with the same dist of sizes
  random.sizes <- c(sapply(ss.lengths, function(x) as.integer(rnorm(2,x,1))))
  # make sure none below 0
  random.sizes[random.sizes<2] <- 1
  # get sets of these sizes
  random.sets <- sapply(random.sizes, function(x) sample(global.cmap.meta$id, x))
  # do in parallel if desired
  if (num.cores != 1){
    if (num.cores == 0){
      library(parallel)
      num.cores <- detectCores() -1
    }
    library(parallel)
    message('Starting signature strength computation in parallel mode')
    message(paste('Using', num.cores, 'cores.'))
    cl <- makeCluster(num.cores, type="FORK")
    start.time <- Sys.time()
    
    # strengths of sample sets
    ss.strengths <- parLapply(cl, arf.data$query_up, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x$entry)]],
                                                                     ex@mat[,ex@cid[!(ex@cid %in% x$entry)]])[1,3])
    # compute random strengths
    random.strengths <- parLapply(cl, random.sets, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x)]], ex@mat[,ex@cid[!(ex@cid %in% x)]])[1,3])
    random.strength.mat <- as.matrix(cbind(random.sizes,random.strengths))
    end.time <- Sys.time()
    total.time <- end.time-start.time
    message("finished computation.")
    print(total.time)
    # done with computation
    stopCluster(cl)
  } else {
    # just do in single core
    message('Starting signature strength computation in single-core mode')
    start.time <- Sys.time()
    # strengths of sample sets
    ss.strengths <- sapply(arf.data$query_up, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x$entry)]],
                                                              ex@mat[,ex@cid[!(ex@cid %in% x$entry)]])[1,3])
    # compute random strengths
    random.strengths <- sapply(random.sets, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x)]], ex@mat[,ex@cid[!(ex@cid %in% x)]])[1,3])
    random.strength.mat <- as.matrix(cbind(random.sizes,random.strengths))
    end.time <- Sys.time()
    total.time <- end.time-start.time
    message("finished computation.")
    print(total.time)
  }
}