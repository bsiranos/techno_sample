# nmf clustering of a gene expression dataset
# inputs: 
#   log2 scaled gene expression matrix (gctx)
#   output folder s
#   (optional) subset of cid to run on 
#   (optional) values for k 
#   (optional) select x most variable genes
#   (optional) select these RIDs
library(argparser)
library(NMF)
msg.trap <- capture.output(suppressMessages(library(roller)))

parser <- arg_parser('Compute NMF clustering for a gene expression dataset. By default clustering is done on the 1500 most variable genes, 
                     with k ranging from 2-8. A consensus is generated from 20 repetitions of clustering at different initial
                     starting positions.')
parser <- add_argument(parser, 'ds', help='Gene expression dataset, log2 transformed gctx. Samples are in columns, genes are in rows.', default=NA)
parser <- add_argument(parser, 'out', help='Output folder for clustering results. Will be created if it does not exist.', default=NA)
parser <- add_argument(parser, '--kstart', help='Perform clustering with k in this range', default=2)
parser <- add_argument(parser, '--kend', help='Perform clustering with k in this range', default=8)
parser <- add_argument(parser, '--nvar', help='Select the top nvar variable genes to cluster on', default=1500)
parser <- add_argument(parser, '--nrun', help='Repeat clustering this many times to get a consensus', default=20)
parser <- add_argument(parser, '--cid', help='Subset to a particular set of samples in this grp file', default=NA)
parser <- add_argument(parser, '--rid', help='By default clustering is done on the 1500 most variable genes.
                       Specify this option to subset to a particular set of genes in this grp file', default=NA)
parser <- add_argument(parser, '--ncores', help='Parallelize across this many CPU cores.', default=NA)
parser <- add_argument(parser, '--plotonly', help='specify a precomputed .rds file to load the results
                       and produce plots.', default=NA)
parser <- add_argument(parser, '--noplot', help='suppress plotting of figures', flag = T)
arguments <- parse_args(parser)

# test arguments
# arguments <- list(ds='/cmap/projects/stacks/STK035_BASE_RNASEQ/clinedb_QNORM_L1KAIG_n1022x12103.gctx', out='/cmap/projects/stacks/STK035_BASE_RNASEQ/bswork/ccle_nmf/',
#                   kstart=2, kend=10, nvar=1500, nrun=20, cid=NA, rid=NA, ncores=5, plotonly=NA, noplot=T, ncores=5)
# arguments <- list(ds='/cmap/data/vdb/cline/cline_gene_n1515x12716.gctx', out='/cmap/projects/stacks/STK035_BASE_RNASEQ/bswork/ccle_nmf_affy/',
#                   kstart=2, kend=10, nvar=1500, nrun=20, cid=NA, rid=NA, ncores=5, plotonly=NA, noplot=T, ncores=5)
# arguments <- list( kstart=2, kend=10,
#                    plotonly='/cmap/projects/stacks/STK035_BASE_RNASEQ/bswork/ccle_nmf/k2_10_nmf_result.rds', out='/cmap/projects/stacks/STK035_BASE_RNASEQ/bswork/ccle_nmf')
# arguments <- list( kstart=2, kend=10,
#                    plotonly='/cmap/projects/stacks/STK035_BASE_RNASEQ/bswork/ccle_nmf_affy/k2_10_nmf_result.rds', out='/cmap/projects/stacks/STK035_BASE_RNASEQ/bswork/ccle_nmf_affy/')


# arguments <- list(ds='~/cmap_tcga/partnerships_extra/sma/data/QNORM/only_blood/SMA_QNORM_BING_COMBAT_AGG_n1370x10174.gctx', out='~/cmap_tcga/partnerships_extra/sma/data/QNORM/only_blood/nmf_test',
#                   kstart=2, kend=3, nvar=1500, nrun=3, cid=NA, rid=NA, ncores=5, plotonly=NA, noplot=T, ncores=7)

# simple argument checks
# kstart is less than kend, both positive
if ((arguments$kend < arguments$kstart) | (arguments$kstart <2) | (arguments$kend < 2)){
  stop('nonsensical arguments for number of clusters')
}
if (is.na(arguments$cid)){arguments$cid <- NULL}
if (is.na(arguments$rid)){arguments$rid <- NULL}

# create output directory if it does not exists
if (!(dir.exists(arguments$out))){
  message('Creating output directory...')
  dir.create(arguments$out,recursive = T)
}

if (is.na(arguments$plotonly)){
  # load up expression file, subsetting to the parameters
  ds <- parse.gctx(arguments$ds, rid=arguments$rid, cid=arguments$cid)
  
  # select variable genes if rid is not specified
  if ((is.null(arguments$rid))){
    ds <- subset.gct(ds, rid=order(apply(ds@mat, 1, function(x) var(x, na.rm = T)), decreasing = T)[1:arguments$nvar])
  }
  
  # where to save the binary result file
  res.file <-  path.join(arguments$out, paste('k', arguments$kstart, '_', arguments$kend,'_nmf_result.rds', sep=''))
  
  # do clustering
  # use multiple cores if specified
  if (!(is.na(arguments$ncores))){
    # core.opt <- paste('vP', arguments$ncores, sep='')
    res <- nmf(ds@mat, arguments$kstart:arguments$kend, nrun=arguments$nrun,  .options=list(parallel=arguments$ncores))
  } else {
    res <- nmf(ds@mat, arguments$kstart:arguments$kend, nrun=arguments$nrun, .options=list(parallel=F))
  }

  # consensus culstering labels
  consensus.label.file <- path.join(arguments$out,
                                    paste('k', arguments$kstart, '_', arguments$kend, '_labels.tsv', sep=''))
  consensus.labels <- matrix(0, nrow=length(ds@cid), ncol=length(arguments$kstart:arguments$kend), dimnames=list(ds@cid, arguments$kstart:arguments$kend))
  for (k in names(res$consensus)){
    kint <- as.integer(k)
    klabel <- cutree(hclust(dist(res$consensus[[k]]), method='ward.D2'), kint)
    consensus.labels[names(klabel), k] <- klabel
  }
  write.table(consensus.labels, consensus.label.file, sep='\t', quote=F, row.names = T, col.names = T)
  
  # save the result
  saveRDS(res, res.file)
} else {
  message('loading precomputed result')
  res <- readRDS(arguments$plotonly)
}

if (!arguments$noplot){
  # save some figures related to clustering
  # rank statistics
  rank.file <- path.join(arguments$out,
                         paste('k', arguments$kstart, '_', arguments$kend, '_rank.png', sep=''))
  png(rank.file, width = 12, height=8, units = 'in', res = 300)
  a <- plot(res)
  print(a)
  dev.off()
  
  # consensus matrix
  consensus.file <- path.join(arguments$out,
                              paste('k', arguments$kstart, '_', arguments$kend, '_consensus%02d.png', sep=''))
  png(consensus.file, width = 12, height=8, units = 'in', res = 300)
  consensusmap(res)
  dev.off()
}

print('DONE! :)')


# # Some code to do this with annotations from the tumor types in TARGET
# res <- readRDS('~/cmap_tcga/external_datasets/target/target_data/nmf_clustering/fpkm/all/k2_8_nmf_result.rds')
# ds <- parse.gctx('~/cmap_tcga/external_datasets/target/target_data/target_QNORM_L1KAIG_n481x12244.gctx',rid=1)
# 
# consensus.file <- path.join('~/cmap_tcga/external_datasets/target/target_data/nmf_clustering/fpkm/all/',
#                             paste('k', 2, '_', 8, '_consensus%02d.png', sep=''))
# png(consensus.file, width = 12, height=8, units = 'in', res = 300)
# consensusmap(res, annCol=as.factor(ds@cdesc$tumor_type))
# dev.off()

