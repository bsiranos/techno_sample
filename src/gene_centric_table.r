# Gene-centric view of TCGA connections
# for each gene, how mnay samples have a given feature
# then compile a dataset or webpage for each?
source('~/projects/techno_sample/shiny_apps/common_functions.R')

gene.meta <- read.table(return_files('','','global_gex_meta'), sep='\t', header=T, quote='')
# limit this to things that have a symbol
gene.meta <- gene.meta[gene.meta$pr_gene_symbol != "", ]
rownames(gene.meta) <- gene.meta$pr_gene_symbol

basedir <- '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan'
dnames <- c("cna_gain","cna_gain_deep","cna_loss","cna_loss_deep","mutations","outliers_adaptive_z4")

qs.files <- paste(basedir, dnames, 'query_stats.txt', sep='/')
qs <- lapply(qs.files, function(x) {
  a <- read.table(x, sep='\t', header=T, quote = '')
  a$gene <- sapply(a$query_id, function(y) strsplit(y, '_')[[1]][1])
  a
  })
  
names(qs) <- dnames
# separate the outlier positive and negative outlier sets
qs[['outliers_positive']] <- qs[["outliers_adaptive_z4"]][qs[["outliers_adaptive_z4"]]$query_desc=='OUTLIER_POS', ]
qs[['outliers_negative']] <- qs[["outliers_adaptive_z4"]][qs[["outliers_adaptive_z4"]]$query_desc=='OUTLIER_NEG', ]
qs[["outliers_adaptive_z4"]] <- NULL 
for (i in 1:length(qs)){
  rownames(qs[[i]]) <- qs[[i]]$gene 
}

# unique genes in qs, might not be in the tcga gene.meta
qs.genes <- unique(unlist(sapply(qs, function(x) x$gene)))
new.genes <- qs.genes[!(qs.genes %in% gene.meta$pr_gene_symbol)]
# add new genes
gene.meta <- rbind(gene.meta, data.frame(pr_id=NA, pr_gene_id=NA, pr_gene_symbol=new.genes, pr_gene_title=NA, pr_is_lm=NA, pr_is_bing=NA, tcga_id=NA))
rownames(gene.meta) <- gene.meta$pr_gene_symbol
# add these numbers to the gene meta table 

gene.stats <- cbind(gene.meta, 
                    as.data.frame(matrix(0, nrow=nrow(gene.meta), ncol=length(qs), dimnames = list(rownames(gene.meta), names(qs)))))
# massive loop to fill this bs in 
for (i in qs.genes){
  for (j in names(qs)){
    to.add <- qs[[j]][qs[[j]]$gene==i, 'up_size']
    if (length(to.add)==0){
      gene.stats[i,j] <- 0
    } else {
      gene.stats[i,j] <- to.add 
    }
  }
}

# make this into a gct file to make parsing easier
# g <- parse.gctx('~/Dropbox/js/small_ex_n500x50.gct')
# g@mat <- as.matrix(gene.stats[, c(3,13,14,8:12)])
# g@rid <- rownames(gene.stats)
# g@rdesc <- data.frame(id=rownames(gene.stats), pr_id=gene.stats$pr_id, pr_gene_title=gene.stats$pr_gene_title, pr_is_lm=gene.stats$pr_is_lm,
#                       pr_is_bing=gene.stats$pr_is_bing)
# g@cid <- colnames(gene.stats)[c(3,13,14,8:12)]
# g@cdesc <- data.frame(id=g@cid, nsets=c(0, apply(g@mat[,2:8], 2, function(x) sum(x>0))))
# write.gct(g, '~/Dropbox/js/gene_centric_stats.gct')

write.table(gene.stats, '~/cmap_tcga/rand0_enrichment_results_lm_build/pancan/gene_centric_aggregate_all/gene_centric_stats.txt', sep='\t', quote=F, row.names=F, col.names = T)
