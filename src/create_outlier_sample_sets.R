# Create sample sets from outlier gene expression across a dataset
source('~/projects/techno_sample/shiny_apps/common_functions.R')

# testing arguments
# arguments=list(ds='/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KAIG_TXONLY_n10255x12276.gctx',
#                out='~/cmap_tcga/outlier_sample_sets/tx_only_z3/outlier_sets_z3_brca_new.gmt',
#                limit_genes=NA,
#                limit_columns=NA,
#                limit_cohort='BRCA',
#                threshold=3,
#                scale_threshold=T, 
#                min_size=5,
#                adaptive_min_size=0.01,
#                always_report=F, 
#                include_name=NA, 
#                numzero_matrix='/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ_NZ_n31x12276.gctx')

# arguments <- list(scale_threshold=FALSE, always_report=FALSE, run_tests=FALSE, run_demo=FALSE, opts=NA, limit_genes=NA, limit_cohort=NA, limit_columns=NA, threshold=4, min_size=15, adaptive_min_size=NA, include_name=NA, numzero_matrix="/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ_NZ_n31x12276.gctx", ds="~/cmap_tcga/ccle/ccle_nmf_affy/zs_ccle_affy_nmfcohort_n1515x12716.gctx", out="~/cmap_tcga/ccle/outlier_sample_sets/ccle_outliers_zs4.gmt")

# MakeOutlierSampleSets(arguments): picks outlier samples in both directions
# for the input dataset. Returns a data structure to be written out to a gmt file. 
MakeOutlierSampleSets <- function(arguments){
  # print(arguments)
  ds.file <- arguments$ds
  out.file <- arguments$out
  limit.genes.file <- arguments$limit_genes
  limit.columns.file <- arguments$limit_columns
  limit.cohort <- toupper(arguments$limit_cohort)
  threshold.val <- arguments$threshold
  scale.threshold <- arguments$scale_threshold
  min.size <- arguments$min_size
  adaptive.min.size <- arguments$adaptive_min_size
  always.report <- arguments$always_report
  include.name <- arguments$include_name
  nz.file <- arguments$numzero_matrix
  
  # sample metadata
  sample.meta <- read.table(return_files('global_tcga_meta'), sep='\t', header=T, quote='')
  cohorts <- unique(sample.meta$cohort)
  cohort.nums <- table(sample.meta$cohort)
  # gex metadata
  gex.meta <- read.table(return_files('global_gex_meta'), sep='\t', header=T, quote='')
  
  # ensure limit cohort is present
  if(!is.na(limit.cohort)){
    if(!(limit.cohort %in% cohorts)){
      stop(paste('limit_cohort is not valid. must be one of', paste(cohorts, collapse = ', ')))
    }
    # get number of samples in this cohort
    num.limit.samples <- cohort.nums[limit.cohort]
  }
  
  # ensure that the provided limits match valid ids
  # genes
  if (!(is.na(limit.genes.file))){
    limit.genes <- parse.grp(limit.genes.file)
    limit.genes <- limit.genes[limit.genes %in% gex.meta$pr_id]
    if(length(limit.genes)==0){
      stop('No matching gene identifiers')
    }
  } else { limit.genes <- NA }
  # samples
  if (!(is.na(limit.columns.file))){
    limit.columns <- parse.grp(limit.columns.file)
    limit.columns <- limit.columns[limit.columns %in% sample.meta$tcga_id]
    if(length(limit.samples)==0){
      stop('No matchin sample identifiers')
    }
  } else if(!is.na(limit.cohort)){
    limit.columns <- sample.meta[sample.meta$cohort==limit.cohort, 'tcga_id']
  } else { limit.columns <- NA}
  
  # if we are going to do adaptive scaling
  if(scale.threshold){
    # right now can't use limit columns
    # stop('TODO')
    # no cohort is specified, then assume this is PANCAN
    if(!(is.na(limit.columns.file))){
      stop('cant do adaptive scaling with limiting custom columns now. Request this feature if you will use it')
    }

    # read in the number of zero samples per cohort
    gex.nz <- parse.gctx(nz.file)
    if(!(is.na(limit.cohort))){
      gex.nz.limit <- gex.nz@mat[, limit.cohort]
    } else {
      gex.nz.limit <- gex.nz@mat[, 'PANCAN']
    }
    scale.threshold.values <- threshold.val / (((num.limit.samples-gex.nz.limit)/num.limit.samples)**2)
    # cap it at 100
    scale.threshold.values[scale.threshold.values>100] <- 100
  } else {
    scale.threshold.values <- rep(threshold.val, times=nrow(gex.meta))
  }
  
  # load up the data.
  if(!is.na(limit.genes) & !all(is.na(limit.columns))){
    ds <- parse.gctx(ds.file, rid=limit.genes, cid=limit.columns)
  } else if (!all(is.na(limit.columns))){
    ds <- parse.gctx(ds.file, cid=limit.columns)
  } else if (!is.na(limit.genes)){
    ds <- parse.gctx(ds.file, rid=limit.genes)
  } else {
    ds <- parse.gctx(ds.file)
  }
  
  if(is.na(limit.cohort)){
    num.limit.samples <- ncol(ds@mat)
  }
  
  if (!is.na(adaptive.min.size)){
    # decide on minimum sample size. max(adaptive.min.size * num.limit.samples, min.size)
    use.adaptive.min.size <- round(adaptive.min.size * num.limit.samples)
    use.min.size <- max(use.adaptive.min.size, min.size)
  } else {
    use.min.size <- min.size
  }
  
  # ensur scale.threshold.values is in the same order as ds
  # scale.threshold.values <- scale.threshold.values[ds@rid]
  
  # dictionary to make accessing signs/names easier
  signs <- c(1,-1)
  names(signs) <- c('POS', 'NEG')
  
  #create data structure to save results
  save.outlier.names <- list()
  
  # loop over each row and pick the outliers
  for (r in 1:length(ds@rid)){
    # print(r)
    all.vals <- ds@mat[r,]
    # for positive and negative in the row   
    for (pick.sign in signs){
      dir <- names(signs)[pick.sign]
      vals <- abs(all.vals[sign(all.vals) == pick.sign])
      
      # Make sure we have enough outlier samples, or even enough that just go in the right direction
      if (length(vals) >= use.min.size){
        above.z <- vals[vals >= scale.threshold.values[r]]
        if (length(above.z) >= use.min.size){
          # write this set out to a gmt
          if (!is.na(include.name)){
            write.name <- paste(ds@rdesc[r,'pr_gene_symbol'], include.name, dir, paste('ZMIN',round(scale.threshold.values[r]), sep=''), length(above.z), sep='_')
          } else {
            write.name <- paste(ds@rdesc[r,'pr_gene_symbol'], dir, paste('ZMIN',round(scale.threshold.values[r]), sep=''), length(above.z), sep='_')
          }
          to.write <- c(write.name, paste('OUTLIER_', dir, sep=''), names(above.z))
          save.outlier.names <- c(save.outlier.names, list(to.write))
        # pick best min.size if option is selected
        } else if(always.report){
          above.z <- sort(vals, decreasing = T)[1:min.size]
          if (!is.na(include.name)){
            write.name <- paste(ds@rdesc[r,'pr_gene_symbol'], include.name, dir, paste('ZMIN',round(min(above.z),2), sep=''), length(above.z), sep='_')
          } else {
            write.name <- paste(ds@rdesc[r,'pr_gene_symbol'], dir, paste('ZMIN',round(min(above.z),2), sep=''), length(above.z), sep='_')
          }
          to.write <- c(write.name, '', names(above.z))
          save.outlier.names <- c(save.outlier.names, list(to.write))
        }
      } else {
        # print(paste(ds@rid[r], '<5 zvalues for', dir))
      }
    }
  }
  return(save.outlier.names)
}

# write the results to a gmt file
SaveOutlierResults <- function(results, out.file){
  if(file.exists(out.file)){
    warning('output file exists. REMOVING.')
    file.remove(out.file)
  }
  for(res in results){
    write.table(t(as.matrix(res)), out.file, sep='\t', quote=F, row.names = F, col.names = F, append = T)
  }
}

# Tests 
runOutlierTests <- function() {
  message("---- RUNNING TESTS ----")
  
  message("-- runAnalysis with no args --")
  tryCatch(MakeOutlierSampleSets(), error=function(e) print(e)) 
  
  message("-- runAnalysis with empty list as args --")
  tryCatch(MakeOutlierSampleSets(list()), error=function(e) print(e))
  
  message("-- runAnalysis with incorrect names in args --")
  tryCatch(MakeOutlierSampleSets(list("foo"=c(1, 2, 3))), error=function(e) print(e))
  
  message("-- runAnalysis with args as a character --")
  tryCatch(MakeOutlierSampleSets("args"), error=function(e) print(e))
}

# demo function. Params defined in the config file
runOutlierDemo <- function() {
  library(yaml)
  message("---- RUNNING DEMO ----")
  message("Calling MakeOutlierSampleSets as defined in config file")
  message("")
  configFile <- "~/projects/techno_sample/tools/resources/create_outlier_sample_sets/config.yaml"
  
  # fix some args, translate to logical
  fixDemoArgs <- function(demo) {
  if(demo$limit_genes=='NA'){demo$limit_genes <- NA}
  if(demo$limit_columns=='NA'){demo$limit_columns <- NA}
  if(demo$include_name=='NA'){demo$include_name <- NA}
  demo$always_report <- as.logical(demo$always_report)
  demo$run_tests <- as.logical(demo$run_tests)
  demo$run_demo <- as.logical(demo$run_demo)
  return(demo)
  }
  
  # load, run, save results
  demo1 <- fixDemoArgs(yaml.load_file(configFile)$demo)
  demo2 <- fixDemoArgs(yaml.load_file(configFile)$demo2)
  
  message('############ RUNNING DEMO 1 ############')
  results1 <- MakeOutlierSampleSets(demo1)
  SaveOutlierResults(results1,demo1$out)
  message('############ DEMO 1 COMPLETE ############')
  
  message('############ RUNNING DEMO 2 ############')
  results2 <- MakeOutlierSampleSets(demo2)
  SaveOutlierResults(results2,demo2$out)
  message('############ DEMO 2 COMPLETE ############')
}



# initialize the SigTool object, including the methods defined above
CreateOutlierSampleSets <- new("SigTool",
                               name = "create_outlier_sample_sets",
                               configFile = "~/projects/techno_sample/tools/resources/create_outlier_sample_sets/config.yaml",
                               runAnalysis = MakeOutlierSampleSets,
                               saveResults = SaveOutlierResults,
                               runTests = runOutlierTests,
                               runDemo = runOutlierDemo,
                               skip_message = T
)
