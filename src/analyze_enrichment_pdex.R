# tool to analyze the result of by-sample enrichment - SOURCE file
# Made for use with TCGA data, but should be able to be used
# with any external expression dataset. 

# should just need to take in the name of an arf to do all the analysis 
# Now that things are standardized
# needs to be present: 
  # query_up.gmt : names of samples in the different sets
  # query_stats.txt : sats on query sizes
    # query_result/result_WTCS.CUSTOM.COMBINED_n*x82902.gctx : wtcs scores of samples sets connecting to cmap
    # OR  
    # pdex_result/cs.gctx : wtcs scores of samples sets connecting to cmap
    # pdex_result/cs.gctx will take precidence if both are found. 
  # pdex_result/ps.gctx : pdex scores of sample sets to 
  # targes.gmt : annotated targets to match for each sample set

source('~/projects/techno_sample/shiny_apps/common_functions.R')
# debugging 
# arguments <- list(arf='~/cmap_tcga/cmap_queries_lm/benchmark_alteration_sets/',
#                    out='~/cmap_tcga/cmap_queries_lm/benchmark_alteration_sets/analyze_enrichment_results/',
#                    cmap_tcga='/cmap/data/vdb/pdex/lm/build/pdex_cmap_ps_n82902x10255.gctx',
#                    expression='/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KAIG_TXONLY_n10255x12276.gctx',
#                    cp_targets='/cmap/projects/cmap_tcga/cp_targets.tsv',
#                    num_cores=1, do_correlation=F, do_sigstrength=F, do_meangutc = F, do_overallbest=F, do_bestpert=F, do_intersection=F)

# Analysis of sample set enrichment in CMap connections
# Big pipeline to to all assigned tasks...
DoAnalyzeEnrichment <- function(arguments){

  # extract args
  num.cores <- arguments$num_cores
  no.correlation <- arguments$no_correlation
  cp.targets.file <- arguments$cp_targets
  ex.file <- arguments$expression
  # place to save results
  out.folder <- arguments$out

  # validate and load ARF
  arf.files <- return_arf_files(arguments$arf)
  arf.data <- load_required_arf_files(arf.files, enrichment = T, pdex = T, ns = T, computed.ae=F)
  
  global.cmap.meta <- read.table(return_files(NA, NA, 'global_cmap_meta'), sep='\t', header = T, quote='')
  global.tcga.meta <- read.table(return_files(NA, NA, 'global_tcga_meta'), sep='\t', header = T, quote='')
  
  # arguments to do different parts of the analysis
  do.correlation <- arguments$do_correlation
  do.meangutc <- arguments$do_meangutc
  do.sigstrength <- arguments$do_sigstrength
  do.overallbest <- arguments$do_overallbest
  do.bestpert <- arguments$do_bestpert
  do.intersection <- arguments$do_intersection
  
  # load expression dataset if doing signature strength
  if (do.sigstrength){ ex <- parse.gctx(ex.file) }
  # load cmap*tcga matrix if doing mean gutc score calculation
  if (do.meangutc){ cmap.tcga <- parse.gctx(return_files(NA, NA, 'global_cmap_tcga')) }
  
  # if (is.na(limit.perts)){
  #   # parse in required files... takes a lot of memory!
  #   ps <- parse.gctx(ps.file)
  #   ss <- parse.gmt(ss.file)
  #   ss.list <- sapply(ss, function(x) x$entry)
  #   ss.lengths <- sapply(ss, function(x) x$len)
  #   enr <- parse.gctx(enr.file)
  #   pv <- parse.gctx(pv.file)
  # } else {
  #   nlim <- limit.perts
  #   # parse in required files... takes a lot of memory!
  #   ps <- parse.gctx(ps.file, cid=1:nlim)
  #   ss <- parse.gmt(ss.file)
  #   ss.list <- sapply(ss, function(x) x$entry)
  #   ss.lengths <- sapply(ss, function(x) x$len)
  #   enr <- parse.gctx(enr.file, rid=1:nlim)
  #   pv <- parse.gctx(pv.file, rid=1:nlim)
  # }

  # list of sample set entries
  ss.list <- sapply(arf.data$query_up, function(x) x$entry)
  # fix errors that occur when a single sample set is loaded
  if(length(arf.data$query_up) == 1){
    ss.list <- list(ss.list)
    names(ss.list) <- arf.data$query_up[[1]]$head
  }
  ss.lengths <- sapply(arf.data$query_up, function(x) x$len)
  ss.names <- sapply(arf.data$query_up, function(x) x$head)
  
  #percentile rank computation. Single-core for now because bugs with collation of results
  # load if already calculated
  prank.files <- grep('percentile_ranks_n', list.files(out.folder), value = T)
  if (length(prank.files) ==1){
    message('Loading pre-computed percentile ranks')
    prank.gctx <- parse.gctx(path.join(out.folder, prank.files))
    prank <- prank.gctx@mat
  } else {
    start.time <- Sys.time()
    message('Starting percentile rank computation.')
    prank <- perc_rank_line(arf.data$enrichment,1)
    end.time <- Sys.time()
    total.time <- end.time-start.time
    message("Time for percentile rank calculation:")
    print(total.time)
    message()
    
    # put this in gct format to save it later
    prank.gctx <- arf.data$enrichment
    prank.gctx@mat <- prank
  }
    
  # calculate mean gutc scores for each sample set connecting to each perturbagen
  mean.gutc.files <- grep('mean_gutc_scores_n', list.files(out.folder), value = T)
  # load up file if it exists already, for debugging
  if (length(mean.gutc.files) ==1){
    message('Loading pre-computed mean gutc scores')
    mean.gutc.gctx <- parse.gctx(path.join(out.folder, mean.gutc.files))
    mean.gutc <- mean.gutc.gctx@mat
  } else {
    if (do.meangutc){
      message('calculating mean gutc scores...')
      # slightly faster way to do the calculations
      # index of which samples are in each set
      index.array <- as.matrix(sapply(ss.list, function(x) global.cmap.meta$id %in% x))
      
      start.time <- Sys.time()
      # do in parallel if desired
      if (num.cores != 1){
        library(parallel)
        if (num.cores == 0){
          num.cores <- detectCores() -1
        }
        message('Starting mean gutc score computation in parallel mode')
        message(paste('Using', num.cores, 'cores.'))
        cl <- makeCluster(num.cores, type="FORK")
        mean.gutc <- parApply(cl,index.array, 2, function(y) rowMeans(t(apply(cmap.tcga@mat, 2, function(x) x[y]))))
        rownames(mean.gutc) <- cmap.tcga@cid
        stopCluster(cl)
      } else {
        message('Starting mean gutc score computation in single-core mode')
        mean.gutc <- apply(index.array, 2, function(y) rowMeans(t(apply(cmap.tcga@mat, 2, function(x) x[y]))))
        rownames(mean.gutc) <- cmap.tcga@cid
      }
      end.time <- Sys.time()
      total.time <- end.time-start.time
      message("Time for mean gutc score calculation:")
      print(total.time)
      message()
      
      # put this in gct format to save it later
      mean.gutc.gctx <- arf.data$enrichment
      mean.gutc.gctx@mat <- mean.gutc
    } else {
    message('Not computing gutc scores. All will be zero.')
    # put this in gct format to save it later
    mean.gutc.gctx <- arf.data$enrichment
    mean.gutc <- matrix(0, nrow=nrow(mean.gutc.gctx@mat), ncol=ncol(mean.gutc.gctx@mat), dimnames = list(mean.gutc.gctx@rid, mean.gutc.gctx@cid))
    mean.gutc.gctx@mat <- mean.gutc
    }
  }
  
  # best overall -----
  # task one: Examine best performing connections
  # overall report of top 1000, all sample sets and perts
  if (do.overallbest){
    message('gathering overall best connections...')
    # overall best connections
    num.report <- 1000
    # best pdex scores
    # look for everything less than this
    pdex.thresh <- sort(c(arf.data$pdex@mat))[num.report]
    pdex.limit.idx <- which(arf.data$pdex@mat <= pdex.thresh,arr.ind = T)
    pdex.limit <- apply(pdex.limit.idx, 1, function(x) arf.data$pdex@mat[x[1], x[2]])
    prank.limit <- apply(pdex.limit.idx, 1, function(x) prank[x[1], x[2]])
    mean.gutc.limit <- apply(pdex.limit.idx, 1, function(x) mean.gutc[x[1], x[2]])
    enr.limit <- apply(pdex.limit.idx, 1, function(x) arf.data$enrichment@mat[x[1], x[2]])
    # new statistic
    new.stat <- prank.limit*mean.gutc.limit
    all.stats <- data.frame(pert_id=arf.data$pdex@rdesc$id[pdex.limit.idx[,1]],
                            pert_iname=arf.data$pdex@rdesc$pert_iname[pdex.limit.idx[,1]],
                            pert_type=arf.data$pdex@rdesc$pert_type[pdex.limit.idx[,1]],
                            cell_id=arf.data$pdex@rdesc$cell_id[pdex.limit.idx[,1]],
                            ss=names(ss.list)[pdex.limit.idx[,2]],
                            enrichment = enr.limit,
                            pdex=pdex.limit,
                            prank=prank.limit,
                            mean_gutc=mean.gutc.limit,
                            newstat=new.stat,
                            size=ss.lengths[pdex.limit.idx[,2]])

    # sort results pdex then newstat
    to.report <- all.stats[order(abs(all.stats[,"pdex"]), all.stats[,"newstat"], decreasing = T),]
    overall.best <- to.report[1:num.report, ]
    rownames(overall.best) <- NULL
  } else {
    overall.best <- NULL
  }
  
  # best to ss ----------
  message('gathering best connections to each sample set...')
  # best connections to each sample set
  # store a list of connections to each
  num.report.ss <- 100
  ss.best.list <- list()
  for (i in 1:length(ss.list)){
    pdex.thresh <- sort(c(arf.data$pdex@mat[,i]))[num.report.ss]
    pdex.limit.idx <- which(arf.data$pdex@mat[,i] <= pdex.thresh)
    pdex.limit <- arf.data$pdex@mat[pdex.limit.idx,i]
    prank.limit <- prank[pdex.limit.idx,i]
    mean.gutc.limit <- mean.gutc[pdex.limit.idx, i]
    enr.limit <- arf.data$enrichment@mat[pdex.limit.idx,i]
    ns.limit <- arf.data$ns@mat[pdex.limit.idx,i]
    # new statistic 
    new.stat <- prank.limit*mean.gutc.limit
    all.stats <- data.frame(pert_id=arf.data$pdex@rdesc$id[pdex.limit.idx],
                            pert_iname=arf.data$pdex@rdesc$pert_iname[pdex.limit.idx],
                            pert_type=arf.data$pdex@rdesc$pert_type[pdex.limit.idx],
                            cell_id=arf.data$pdex@rdesc$cell_id[pdex.limit.idx],
                            ss = names(ss.list)[i],
                            enrichment=enr.limit,
                            ns=ns.limit,
                            pdex=pdex.limit, 
                            prank=prank.limit, 
                            mean_gutc=mean.gutc.limit, 
                            newstat=new.stat,
                            size=length(ss.list[[i]]))
    
    # sort results pdex then newstat
    to.report <- all.stats[order(abs(all.stats[,"pdex"]), all.stats[,"newstat"], decreasing = T),]
    ss.best <- to.report[1:num.report.ss, ]
    rownames(ss.best) <- NULL
    ss.best.list[[names(ss.list)[i]]] <- ss.best
  }
  
  # best to pert ------
  if(do.bestpert){
    # best connections to each pert
    # how many to keep for each? 10 or 20% of the number of sample sets, whichever is less
    num.report.pert <- 10 
    if (length(ss.list) < (num.report.pert * 5)){
      num.report.pert <- round(length(ss.list) / 5)
    }
    message('gathering best connections to each perturbagen...')
    pert.best.list <- list()
    # so many perts!
    for (i in 1:nrow(global.cmap.meta)){
      pert.id <- global.cmap.meta[i, 'id']
      pdex.thresh <- sort(c(arf.data$pdex@mat[i,]))[num.report.pert]
      pdex.limit.idx <- which(arf.data$pdex@mat[i,] <= pdex.thresh)
      if(length(pdex.limit.idx) > 0){
        pdex.limit <- arf.data$pdex@mat[i,pdex.limit.idx]
        prank.limit <- prank[i, pdex.limit.idx]
        mean.gutc.limit <- mean.gutc[i,pdex.limit.idx]
        enr.limit <- arf.data$enrichment@mat[i,pdex.limit.idx]
        ns.limit <- arf.data$enrichment@mat[i,pdex.limit.idx]
        # new statistic
        new.stat <- prank.limit*mean.gutc.limit
        all.stats <- data.frame(pert_id=arf.data$pdex@rdesc$id[i],
                                pert_iname=arf.data$pdex@rdesc$pert_iname[i],
                                pert_type=arf.data$pdex@rdesc$pert_type[i],
                                cell_id=arf.data$pdex@rdesc$cell_id[i],
                                ss=names(ss.list)[pdex.limit.idx],
                                enrichment=enr.limit,
                                ns=ns.limit,
                                pdex=pdex.limit,
                                prank=prank.limit,
                                mean_gutc=mean.gutc.limit,
                                newstat=new.stat,
                                size=ss.lengths[pdex.limit.idx])

        # sort results pdex then newstat
        to.report <- all.stats[order(abs(all.stats[,"pdex"]), all.stats[,"newstat"], decreasing = T),]
        pert.best <- to.report[1:num.report.pert, ]
        rownames(pert.best) <- NULL
        pert.best.list[[pert.id]] <- pert.best
      } else {
        pert.best.list[[pert.id]] <- NULL
      }
    }
  } else {
    pert.best.list <- NULL
  }
  
  # expected connections ----------------
  # task 2: filter and produce a list of expected connections
  message('filtering to expected connections...')
  # load up the targets file 
  cp.targets <- read.table(cp.targets.file, sep = '\t', header=T)
  # expected connections defined from the targets gmt file
  expected.inames <- sapply(arf.data$targets, function(x) x$entry)
  start.time <- Sys.time()
  expected.connections <- generate_expected(c('all'),c('all'), arf.data$enrichment, arf.data$ns,
                                            prank, arf.data$pdex, mean.gutc, arf.data$query_up,
                                            expected.inames, cp.targets)
  end.time <- Sys.time()
  total.time <- end.time-start.time
  message("Time for expected connections:")
  print(total.time)
  
  # start.time <- Sys.time()
  # expected.connections2 <- generate_expected2(c('all'),c('all'), arf.data$enrichment, arf.data$ns,
  #                                           prank, arf.data$pdex, mean.gutc, arf.data$query_up,
  #                                           expected.inames, cp.targets)
  # end.time <- Sys.time()
  # total.time <- end.time-start.time
  # message(paste("Time for excon V2:", total.time))
  # print(total.time)
  # 
  # # these two objects should be identical
  # print(paste('excon objects are identical?', identical(expected.connections, expected.connections2)))
  # print(head(expected.connections))
  # message()
  # print(head(expected.connections2))
  # print(tail(expected.connections))
  # message()
  # print(tail(expected.connections2))
  # 
  
  # summary statistics ------
  # task 3: do some summary stats on the sample stats for showing later
  # for each pair of sample sets, compute the intersection fraction 
  # as a function of the smaller one
  if(do.intersection){
    if(length(arf.data$query_up) > 1){
      message('doing overlap and jaccard calculations...')
      to.compare <- combn(length(arf.data$query_up), 2)
      ss.overlap <- apply(to.compare, 2, function(x) {
        length(intersect(arf.data$query_up[[x[1]]]$entry, arf.data$query_up[[x[2]]]$entry)) / min(ss.lengths[x[1]], ss.lengths[x[2]])
        })
      # jaccard index of sample sets
      ss.jaccard <- apply(to.compare, 2, function(x) {
        length(intersect(arf.data$query_up[[x[1]]]$entry, arf.data$query_up[[x[2]]]$entry)) / 
          length(union(arf.data$query_up[[x[1]]]$entry, arf.data$query_up[[x[2]]]$entry))
        })
    } else {
      ss.overlap <- NULL
      ss.jaccard <- NULL
    }
  } else {
    ss.overlap <- NULL
    ss.jaccard <- NULL
  }
  
  # signature strength computation -------
  if(do.sigstrength){
    message('doing signature strenght calculations')
    # Things related to signature strength
    # make a bunch of random sample sets - 2x the number with the same dist of sizes
    random.sizes <- c(sapply(ss.lengths, function(x) as.integer(rnorm(2,x,1))))
    # make sure none below 0
    random.sizes[random.sizes<2] <- 1
    # get sets of these sizes
    random.sets <- sapply(random.sizes, function(x) sample(global.cmap.meta$id, x))
    # do in parallel if desired
    if (num.cores != 1){
      if (num.cores == 0){
        library(parallel)
        num.cores <- detectCores() -1
      }
      library(parallel)
      message('Starting signature strength computation in parallel mode')
      message(paste('Using', num.cores, 'cores.'))
      cl <- makeCluster(num.cores, type="FORK")
      start.time <- Sys.time()

      # strengths of sample sets
      ss.strengths <- parLapply(cl, arf.data$query_up, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x$entry)]],
                                                        ex@mat[,ex@cid[!(ex@cid %in% x$entry)]])[1,3])
      # compute random strengths
      random.strengths <- parLapply(cl, random.sets, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x)]], ex@mat[,ex@cid[!(ex@cid %in% x)]])[1,3])
      random.strength.mat <- as.matrix(cbind(random.sizes,random.strengths))
      end.time <- Sys.time()
      total.time <- end.time-start.time
      message("finished computation.")
      print(total.time)
      # done with computation
      stopCluster(cl)
    } else {
      # just do in single core
      message('Starting signature strength computation in single-core mode')
      start.time <- Sys.time()
      # strengths of sample sets
      ss.strengths <- sapply(arf.data$query_up, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x$entry)]],
                                                 ex@mat[,ex@cid[!(ex@cid %in% x$entry)]])[1,3])
      # compute random strengths
      random.strengths <- sapply(random.sets, function(x) s2n(ex@mat[,ex@cid[(ex@cid %in% x)]], ex@mat[,ex@cid[!(ex@cid %in% x)]])[1,3])
      random.strength.mat <- as.matrix(cbind(random.sizes,random.strengths))
      end.time <- Sys.time()
      total.time <- end.time-start.time
      message("finished computation.")
      print(total.time)
    }
  }
  
  # mean pdex across all perturbagens
  ss.pdex.mean <- rowMeans(t(arf.data$pdex@mat))
  
  # save these in a list to be exported
  summary.stats <- list(ss.lengths=ss.lengths, ss.overlap=ss.overlap, ss.jaccard=ss.jaccard, 
                        ss.pdex.mean=ss.pdex.mean)
  if (do.correlation & length(arf.data$query_up) >1){
    # clustering heatmaps ----
    # create a matrix of signed newstat
    newstat.sign <- abs(prank) * mean.gutc
    # calculate spearman correlation for several different metrics
    message('Computing correlations between sample sets.....')
    start.time <- Sys.time()
    enr.spear <- cor(arf.data$enrichment@mat, method='spear')
    pdex.spear <- cor(arf.data$pdex@mat, method='spear')
    pdex.sign.spear <- cor(arf.data$pdex@mat*sign(arf.data$enrichment@mat), method='spear')
    # save correlations in list to be saved
    set.correlation <- list(enr.spear=enr.spear, 
                            pdex.spear=pdex.spear, 
                            pdex.sign.spear=pdex.sign.spear)
    # how long did all this take?
    end.time <- Sys.time()
    total.time <- end.time-start.time
    message("Time for correlation calculation:")
    print(total.time)
  } else {
    set.correlation <- NA
  }
  
  # returns a list of all these. Save to different spots in the save results function. 
  return(list(overall.best=overall.best, ss.best.list=ss.best.list,
              pert.best.list=pert.best.list, expected.connections=expected.connections,
              mean.gutc=mean.gutc.gctx, prank=prank.gctx, summary.stats=summary.stats,
              set.correlation=set.correlation))
}

SaveEnrichmentResults <- function(enrResults, out.folder){
  out.overall <- path.join(out.folder, 'overall_best_connections.tsv')
  out.ss <- path.join(out.folder, 'sample_set_best_connections.rda')
  out.pert <- path.join(out.folder, 'perturbagen_best_connections.rda')
  out.summary.stats <- path.join(out.folder, 'sample_set_summary_stats.rda')
  out.correlation <- path.join(out.folder, 'sample_set_correlations.rda')
  out.expected <- path.join(out.folder, 'expected_connections.tsv')
  out.mean.gutc <- path.join(out.folder, 'mean_gutc_scores.gctx')
  out.prank <- path.join(out.folder, 'percentile_ranks.gctx')
  out.heatmaps <- path.join(out.folder, 'correlation_heatmaps')
  
  # save each in a specified format
  message('writing results...')
  if (!is.null(enrResults[['overall.best']])){
    write.table(enrResults[['overall.best']], out.overall, quote = F, row.names = F, col.names = T, sep='\t')
  }
  if (!is.null(enrResults[['pert.best.list']])){
    saveRDS(enrResults[['pert.best.list']], file=out.pert)
  }
  saveRDS(enrResults[['ss.best.list']], file=out.ss)
  # saveRDS(enrResults[['pert.best.list']], file=out.pert)
  saveRDS(enrResults[['summary.stats']], file=out.summary.stats)
  if (!is.null(enrResults[['expected.connections']])){
    write.table(enrResults[['expected.connections']], out.expected, quote = F, row.names = F, col.names = T, sep='\t')
  }
  # write.gctx(enrResults[['mean.gutc']], out.mean.gutc)
  write.gctx(enrResults[['prank']], out.prank)
  
  
  if (!(is.na(enrResults[['set.correlation']]))){
    message('Saving clustering heatmaps')
    # save each of these to a separate png file
    tryCatch({
      # save the plots here 
      if (!file.exists(out.heatmaps)){
        dir.create(out.heatmaps, recursive = T)
      }
      
      png(path.join(out.heatmaps, 'enr_spear_heatmap.png'), width = 20, height=20, units = 'in', res=300, pointsize = 4)
      hm1<- heatmap.2(enrResults[['set.correlation']][['enr.spear']], trace='none', 
                      hclustfun = function(x) hclust(x, method = 'ward.D2'), distfun = function(y) as.dist(1-y), col=rev(redblue(64)))
      # hm1
      dev.off()
      
      png(path.join(out.heatmaps, 'pvalue_spear_heatmap.png'), width = 20, height=20, units = 'in', res=300, pointsize = 4)
      hm2 <- heatmap.2(enrResults[['set.correlation']][['pdex.spear']], trace='none', 
                       hclustfun = function(x) hclust(x, method = 'ward.D2'), distfun = function(y) as.dist(1-y), col=rev(redblue(64)))
      # hm2
      dev.off()
      
      png(path.join(out.heatmaps, 'pvalue_sign_spear_heatmap.png'), width = 20, height=20, units = 'in', res=300, pointsize = 4)
      hm3 <- heatmap.2(enrResults[['set.correlation']][['pdex.sign.spear']], trace='none', 
                       hclustfun = function(x) hclust(x, method = 'ward.D2'), distfun = function(y) as.dist(1-y), col=rev(redblue(64)))
      # hm3
      dev.off()
      enrResults[['set.correlation']]$enr.spear.den <- hm1$colDendrogram
      enrResults[['set.correlation']]$pdex.spear.den <- hm2$colDendrogram
      enrResults[['set.correlation']]$pdex.sign.spear.den <- hm3$colDendrogram
    }, error=function(e) print(e),
    warning=function(w) print(w))
    # save correlation and dendro
    saveRDS(enrResults[['set.correlation']], file=out.correlation)
  }
  
}


# perc_rank_signed(data): calculate the percentile ranks from -1 to 1
# for a numeric vector data. helper function for perc_rank_line
perc_rank_signed <- function(data) {
  names(data) <- 1:length(data)
  # separate and calc ranks
  bg.pos <- data[data>=0]
  bg.neg <- data[data<0]
  rnk.pos <- rank(bg.pos)
  rnk.neg <- rank(abs(bg.neg))
  # calc percent ranks
  prank.pos <- rnk.pos/length(rnk.pos)
  prank.neg <- (rnk.neg/length(rnk.neg)) * -1
  # make a big vector and order based on original position
  prank.big <- c(prank.pos, prank.neg)
  to_return <- prank.big[order(as.numeric(names(prank.big)))]
  return(to_return)
}


# perc_rank_line(ds, num.cores): calculate percentile ranks for each column in the gct ds.
# returns a matrix with dim equal to dim(ds@mat), percentile ranks
# are calculated for each column, within each cell line. 
# if num.cores >1, a cluster is made and used to do the computation
# Crappy implementation of parallel computation... but this function
# doesnt take too long anyway 
perc_rank_line <- function(ds, num.cores){
  lines <- unique(ds@rdesc$cell_id)
  to_return <- matrix(0, nrow=nrow(ds@mat), ncol=ncol(ds@mat))
  rownames(to_return) <- ds@rid
  colnames(to_return) <- ds@cid
  # for each column, do this in parallel now
  if (num.cores > 1){
    cl <- makeCluster(num.cores, type="FORK")
    parSapply(cl, 1:ncol(ds@mat), function(j) {
      for (l in 1:length(lines)){
        line <- lines[l]
        sub <- ds@mat[ds@rdesc$cell_id==line, j]
        # to_return[ds@rdesc$cell_id==line, j] <- perc_rank_signed(sub)
        tryCatch(to_return[ds@rdesc$cell_id==line, j] <- perc_rank_signed(sub), error=function(e) return(0))
      }
    })
    stopCluster(cl)
  } else {
    for (j in 1:ncol(ds@mat)){
      # for each cell line
      for (l in 1:length(lines)){
        line <- lines[l]
        sub <- ds@mat[ds@rdesc$cell_id==line, j]
        # to_return[ds@rdesc$cell_id==line, j] <- perc_rank_signed(sub)
        tryCatch(to_return[ds@rdesc$cell_id==line, j] <- perc_rank_signed(sub), error=function(e) return(0))
      }
    }
  }
  return(to_return)
}

# perc_rank_line_type(ds): calculate percentile ranks for each column in the gct ds.
# returns a matrix with dim equal to dim(ds@mat), percentile ranks
# are calculated for each column, within each cell line, within each pert type. 
perc_rank_line_type <- function(ds){
  lines <- unique(ds@rdesc$cell_id)
  ptypes <- unique(ds@rdesc$pert_type)
  to_return <- matrix(0, nrow=nrow(ds@mat), ncol=ncol(ds@mat))
  rownames(to_return) <- ds@rid
  colnames(to_return) <- ds@cid
  for (j in 1:ncol(ds@mat)){
    # for each cell line
    for (l in 1:length(lines)){
      line <- lines[l]
      for (p in 1:length(ptypes)){
        pt <- ptypes[p]
        sub <- ds@mat[(ds@rdesc$cell_id==line & ds@rdesc$pert_type==pt), j]
        tryCatch(to_return[(ds@rdesc$cell_id==line & ds@rdesc$pert_type==pt), j] <- perc_rank_signed(sub), error=function(e) return(0))
      }
    }
  }
  return(to_return)
}
# generate table of connections to expected perturbagens. 
# cells: vector of cell lines to consider. if cells==c('all'), all lines are considered
# pert.types: vector of pert types to consider. if pert.types==c('all'), all are considered
# enrichment: gctx  object from by-sample enrichment, wtcs scores. . Assumes column names are the same as the 
#  head entry in gmt, but uppercase
# ns: gctx  object from by-sample enrichment, normalized scores
# mean.gutc: result from calculating the mean gutc ranks of the connections
# prank: matrix of percentile ranks from perc_rank_line function
# pdex: gctx of pdexalues from external calculation
# ss: gmt object of sample sets, used to test for enrichment
# expected.inames: list of length same as gmt. each entry in the list is a vector of names
#                  to look for in genetic perturbations and targets of chemical perturbations
# targets: df from expected targets file
generate_expected <- function(cells, pert.types, enrichment, ns, prank, pdex, mean.gutc, ss, expected.inames, targets){
  if ('all' %in% cells){ cells <- unique(enrichment@rdesc$cell_id)}
  if ('all' %in% pert.types){ pert.types <- unique(enrichment@rdesc$pert_type)}
  # print(cells)
  # print(pert.types)
  rownames(enrichment@rdesc) <- enrichment@rdesc$id
  # df to store results
  return_expected <- data.frame()
  # list to strore expected targets 
  expected.targets <- expected.inames
  
  # add targets of genes in the expected.inames 
  if ('trt_cp' %in% pert.types){
    expected.targets <- lapply(1:length(expected.targets), function(x) c(expected.targets[[x]], 
                                                                         targets[targets$gene_target %in% expected.inames[[x]],'pert_iname']))
  }
  
  # print(expected.targets)
  # get rows of expected connections, add results to df
  for (i in 1:length(ss)){
    # print(paste(i, ss[[i]]$head))
    # all rows of enrichment that have connections we're interested in
    ex_rows <- enrichment@rid[(enrichment@rdesc$pert_iname %in% expected.targets[[i]]) & 
                                (enrichment@rdesc$cell_id %in% cells) & 
                                (enrichment@rdesc$pert_type %in% pert.types)]
    # if theres any connections...
    if (length(ex_rows > 0)){
      # print(ex_rows)
      to_add <- data.frame(pert_id=enrichment@rdesc[ex_rows,'id'], 
                      pert_iname=enrichment@rdesc[ex_rows,'pert_iname'], 
                      pert_type=enrichment@rdesc[ex_rows,'pert_type'], 
                      cell_id=enrichment@rdesc[ex_rows,'cell_id'], 
                      ss=ss[[i]]$head, 
                      enrichment=enrichment@mat[ex_rows, toupper(ss[[i]]$head)],
                      ns=ns@mat[ex_rows, toupper(ss[[i]]$head)],
                      pdex=pdex@mat[ex_rows,toupper(ss[[i]]$head)], 
                      prank=prank[ex_rows,toupper(ss[[i]]$head)],
                      mean_gutc = mean.gutc[ex_rows,toupper(ss[[i]]$head)],
                      newstat=prank[ex_rows,toupper(ss[[i]]$head)] * mean.gutc[ex_rows,toupper(ss[[i]]$head)], 
                      size=length(ss[[i]]$entry))
      return_expected <- rbind(return_expected, to_add)
    }
  }
  
  # sort it by pdex then ranks
  # print(head(return_expected))
  if(nrow(return_expected) > 0){ 
    return_expected <- return_expected[order(abs(return_expected[,'pdex']),abs(return_expected[,'ns']), decreasing = T), ]
    rownames(return_expected) <- NULL
  } else {
    return(NULL)
  }
  return(return_expected)
}
# testing ways to improve function
generate_expected2 <- function(cells, pert.types, enrichment, ns, prank, pdex, mean.gutc, ss, expected.inames, targets){
  # eliminate the function to limit to a certain line, type
  if (!(all(('all' %in% cells), ('all' %in% pert.types)))){
    stop('impropper cell or type argument to generate_expected')
  }
  
  # print(cells)
  # print(pert.types)
  rownames(enrichment@rdesc) <- enrichment@rdesc$id
  # df to store results
  return_expected <- data.frame()
  # list to strore expected targets 
  expected.targets <- expected.inames
  
  # add compounds that target this gene
  expected.targets <- lapply(expected.targets, function(x) c(x, targets[targets$gene_target %in% x,'pert_iname']))
  # print(head(expected.targets, 20))
  # message()
  # 
  # row ids that are annotated to target each sample set
  ex.rows <- lapply(expected.targets, function(x) enrichment@rid[(enrichment@rdesc$pert_iname %in% x)])
  ex.rows.unlist <- unlist(ex.rows)
  # print(head(ex.rows.unlist))
  # message()
  # which ex.row corresponds to which sample set
  name.ind <- rep(names(ss), times=sapply(ex.rows, length))
  # print(head(name.ind, 20))
  # message()
  
  return_expected <- as.data.frame(matrix(0, nrow=length(name.ind), ncol=11))
  for (i in 1:length(name.ind)){
    return_expected[i,] <- c(enrichment@rdesc[ex.rows.unlist[i],c('id','pert_iname','pert_type','cell_id')], 
                             ss[[name.ind[i]]]$head, 
                             enrichment@mat[ex.rows.unlist[i], toupper(ss[[name.ind[i]]]$head)],
                             ns@mat[ex.rows.unlist[i], toupper(ss[[name.ind[i]]]$head)],
                             pdex@mat[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)], 
                             prank[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)],
                             mean.gutc[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)],
                             prank[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)] * mean.gutc[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)], 
                             length(ss[[name.ind[i]]]$entry))
  }
  # return_expected <- t(data.frame(sapply(1:length(name.ind), function(i) c(enrichment@rdesc[ex.rows.unlist[i],c('id','pert_iname','pert_type','cell_id')], 
  #                                                                          ss[[name.ind[i]]]$head, 
  #                                                                          enrichment@mat[ex.rows.unlist[i], toupper(ss[[name.ind[i]]]$head)],
  #                                                                          pdex@mat[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)], 
  #                                                                          prank[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)],
  #                                                                          mean.gutc[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)],
  #                                                                          prank[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)] * mean.gutc[ex.rows.unlist[i],toupper(ss[[name.ind[i]]]$head)], 
  #                                                                          length(ss[[name.ind[i]]]$entry)))))
  colnames(return_expected) <- c('pert_id','pert_iname','pert_type','cell_id','ss','enrichment','ns','pdex','prank','mean_gutc','newstat','size')
  # print(head(return_expected))
  
  # sort it by pdex then ranks
  # print(head(return_expected))
  if(nrow(return_expected) > 0){ 
    return_expected <- return_expected[order(abs(as.numeric(return_expected[,'pdex'])), abs(as.numeric(return_expected[,'ns'])), decreasing = T), ]
    rownames(return_expected) <- NULL
  } else {
    return(NULL)
  }
  return(return_expected)
}

# look at biological relevance in the data
# what fraction of perts beat out all random subsets? and have a pvalue <= min_p
# pdex: matrix of pdex from external calculation
# min_p: what's the value perts need to have <= to be considered relevant?
#  typically 1e-4
biological_relevance <- function(pdex, wtcs, min_p){
  avail.lines <- unique(wtcs@rdesc$cell_id)
  to_return <- matrix(0, nrow=ncol(pdex), ncol=length(avail.lines), dimnames=list(colnames(pdex), avail.lines))
  for (i in 1:ncol(pdex)){
    for (j in 1:length(avail.lines)){
      vals <- pdex[wtcs@rdesc$cell_id==avail.lines[j], i]
      to_return[i,j] <- (sum(vals==min.p)/length(vals))
    }
  }
  return(to_return)
}



# Tests 
runEnrichmentTests <- function() {
  message("---- RUNNING TESTS ----")
  
  message("-- runAnalysis with no args --")
  tryCatch(DoAnalyzeEnrichment(), error=function(e) print(e)) 
  
  message("-- runAnalysis with empty list as args --")
  tryCatch(DoAnalyzeEnrichment(list()), error=function(e) print(e))
  
  message("-- runAnalysis with incorrect names in args --")
  tryCatch(DoAnalyzeEnrichment(list("foo"=c(1, 2, 3))), error=function(e) print(e))
  
  message("-- runAnalysis with args as a character --")
  tryCatch(DoAnalyzeEnrichment("args"), error=function(e) print(e))
}

# demo function. Params defined in the config file
runEnrichmentDemo <- function() {
  library(yaml)
  message("---- RUNNING DEMO ----")
  message("Calling AnalyzeEnrichment as defined in config file")
  message("")
  configFile <- "~/projects/techno_sample/tools/resources/analyze_enrichment/config.yaml"
  
  # fix some args, translate to logical
  fixDemoArgs <- function(demo) {
    if(demo$limit_perts=='NA'){demo$limit_perts <- NA}
    demo$expected_connections <- as.logical(demo$expected_connections)
    demo$run_tests <- as.logical(demo$run_tests)
    demo$run_demo <- as.logical(demo$run_demo)
    return(demo)
  }
  
  # load, run, save results
  demo1 <- fixDemoArgs(yaml.load_file(configFile)$demo)
  demo2 <- fixDemoArgs(yaml.load_file(configFile)$demo2)
  
  message('############ RUNNING DEMO 1 ############')
  results1 <- DoAnalyzeEnrichment(demo1)
  SaveEnrichmentResults(results1,demo1$out_folder)
  message('############ DEMO 1 COMPLETE ############')
  # 
  message('############ RUNNING DEMO 2 ############')
  message('Same as demo1 but with multi-core support')
  results2 <- DoAnalyzeEnrichment(demo2)
  SaveEnrichmentResults(results2,demo2$out_folder)
  message('############ DEMO 2 COMPLETE ############')
}



# initialize the SigTool object, including the methods defined above
AnalyzeEnrichment <- new("SigTool",
                           name = "analyze_enrichment",
                           configFile = "~/projects/techno_sample/tools/resources/analyze_enrichment/config.yaml",
                           runAnalysis = DoAnalyzeEnrichment,
                           saveResults = SaveEnrichmentResults,
                           runTests = runEnrichmentTests,
                           runDemo = runEnrichmentDemo,
                           skip_message = T
)


