# UNFINISHED. TURNED OUT I DIDNT NEED THIS CODE
# SAVING INCASE ITS USEFUL LATER....

# sample sets from the downloaded cbioprotal mutation/CNA tables

sample.meta <- read.table('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/col_meta.txt', sep='\t', fill = T, quote='', header=T)
short.to.long <- sample.meta$tcga_id
names(short.to.long) <- sapply(short.to.long, function(x) substr(x, 1,15))
long.to.short <- names(short.to.long)
names(long.to.short) <- sample.meta$tcga_id



fname <- '~/cmap_tcga/alteration_sample_sets/cbio_tables/acc_tcga_CNA Genes (-span id=-number-of-selected-cna-samples--90--span- profiled samples).txt'
ftype <- 'CNA'
min.size <- 5

a <- read.table(fname, sep='\t', fill=T)
a <- a[2:nrow(a), ]
if (ftype=='CNA'){
  colnames(a) <- c('gene','cytoband','CNA','num','freq','gistic','samples')
  a$freq <- sapply(a$freq, function(x) strsplit(x,split='%')[[1]][1])
  sample.comma <- a$samples
  names(sample.comma) <- a$gene
  a <- a[,c(-7)]
} else {
  
}

# make list of samples mutated for different genes
sample.list <- sapply(sample.comma, function(x) c(sapply(strsplit(x,split=','[[1]]), function(y) short.to.long[y])))


