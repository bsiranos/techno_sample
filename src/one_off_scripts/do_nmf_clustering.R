# do NMF on the large dataset
`ex.file <- '/cmap/projects/stacks/STK039_TCGA_RNASEQ/TCGA_RNASEQ.RSEM_PROFILE.L1KAIG_n14114x12276.gctx'
ex.all <- parse.gctx(ex.file)
cluster.files.path <- '~/sig_tcga/bsiranos/tcga_mrna_clusters/analyses__2015_08_21/bestclus/'
cluster.files <- list.files(cluster.files.path)
# default out path
out.path <- '~/sig_tcga/bsiranos/nmf_clustering_results_TP/'
plot.path <- '~/sig_tcga/bsiranos/nmf_clustering_results_TP/'

cohorts <- unique(ex.all@cdesc$cohort)
for (cohort in cohorts[1:length(cohorts)]){
  print(paste('Starting for', cohort))
  print(' -------------------------------------- ')
  
  # Load up file with clustering
  if(cohort=='LGG'){
    cluster.file <- '/cmap/projects/sig_tcga/bsiranos/tcga_mrna_clusters/analyses__2015_08_21/bestclus/LGG-TP.bestclus.txt'
  } else if (cohort=='GBMLGG'){
    cluster.file <- '/cmap/projects/sig_tcga/bsiranos/tcga_mrna_clusters/analyses__2015_08_21/bestclus/GBMLGG-TP.bestclus.txt'
   } else if (cohort=='READ'){
    cluster.file <- '/cmap/projects/sig_tcga/bsiranos/tcga_mrna_clusters/analyses__2015_08_21/bestclus/READ-TP.bestclus.txt'
  } else{
    cluster.file <- path.join(cluster.files.path, grep(paste(cohort,'-',sep=''), cluster.files, value=T))
  }
  gdac.clusters <- read.table(cluster.file, sep='\t', row.names=1)
  
  # subset to members of this cohort
  ex <- subset.gct(ex.all, cid = (ex.all@cdesc$cohort==cohort & ex.all@cdesc$sample_type_abbr=='TP'))
  # some rows can be all 0, this leads to NMF failing
  # remove those rows before continuing
  id.zero <- rowSums(ex@mat)==0
  if (any(id.zero)){
    ex <- subset.gct(ex, rid=!id.zero)
  }
  ex.names.short <- sapply(ex@cid, function(x) substr(x,1,15))
  # limit this to lm genes
  ex.lm <- subset.gct(ex, rid=ex@rid[ex@rdesc$pr_is_lm==1])
  # limit to most variable genes
  gene.vars <- apply(ex@mat, 1, var)
  ngene <- 1500
  ex.var <- subset.gct(ex, rid=names(gene.vars)[order(gene.vars, decreasing = T)[1:ngene]])
  
  # check if all of the samples in the ex have clusters, and vice versa
  # zero entries for things without clusters 
  no.cluster <- ex.names.short[!(ex.names.short %in% rownames(gdac.clusters))]
  no.cluster.mat <- cbind(rep(0, length(no.cluster)), rep(0, length(no.cluster)))
  rownames(no.cluster.mat) <- no.cluster
  colnames(no.cluster.mat) <- colnames(gdac.clusters)
  gdac.clusters <- rbind(gdac.clusters, no.cluster.mat)
  # eliminate entries not in ex and sort 
  gdac.clusters <- gdac.clusters[ex.names.short, ]
  
  # LANDMARK genes NMF clustering
  # where to save results
  lm.out.path <- path.join(out.path, tolower(cohort), 'lm_genes')
  lm.plot.path <- path.join(plot.path, tolower(cohort), 'lm_genes')
  if(!dir.exists(lm.out.path)){
    dir.create(lm.out.path, recursive = T)
  }
  if(!dir.exists(lm.plot.path)){
    dir.create(lm.plot.path, recursive = T)
  }
  # doing NMF clustering: k 2-8 with 20 repetitions per size
  # check if we already did clustering and load data if so.
  res.file <-  path.join(lm.out.path, 'nmf_result.rds')
  if (file.exists(res.file)){
    # print('Loading pre-computed result')
    res <- readRDS(res.file)
  } else {
    # use 8 cores to parallelize
    res <- nmf(ex.lm@mat, 2:7, nrun=20, .opt='vP8')
    print('breaking!')
    next()
  }
  
  # save the object 
  saveRDS(res, path.join(lm.out.path, 'nmf_result.rds'))
  
  # # save some figures related to clustering
  # png(path.join(lm.plot.path,'k2_8_rank.png'), width = 12, height=8, units = 'in', res = 300)
  # plot(res)
  # dev.off()

  # png(path.join(lm.out.path,'k2_8_consensus%02d.png'), width = 12, height=8, units = 'in', res = 300)
  # consensusmap(res, annCol=as.factor(gdac.clusters[,1]))
  # dev.off()
  
  # consensus culstering labels
  # for each k, do hclust on the consensus matrix (euclidean distance, ward.D2 method)
  # cut the tree at k and use that as cluster assignment 
  # consensus.labels <- matrix(0, nrow=length(ex@cid), ncol=length(2:7), dimnames=list(ex@cid, 2:7))
  # for (k in names(res$consensus)){
  #   kint <- as.integer(k)
  #   klabel <- cutree(hclust(dist(res$consensus[[k]]), method='ward.D2'), kint)
  #   consensus.labels[names(klabel), k] <- klabel
  # }
  # write.table(consensus.labels, path.join(lm.out.path,'k2_8_labels.tsv'), sep='\t', quote=F, row.names = T, col.names = T)
  # 
  # VARIABLE genes NMF clustering
  # where to save results
  variable.out.path <- path.join(out.path, tolower(cohort), '978_variable_genes')
  variable.plot.path <- path.join(plot.path, tolower(cohort), '978_variable_genes')
  if(!dir.exists(variable.out.path)){
    dir.create(variable.out.path, recursive = T)
  }
  if(!dir.exists(variable.plot.path)){
    dir.create(variable.plot.path, recursive = T)
  }
  
  # doing NMF clustering: k 2-8 with 20 repetitions per size
  # check if we already did clustering and load data if so.
  res.var.file <-  path.join(variable.out.path, 'nmf_result.rds')
  if (file.exists(res.var.file)){
    print('Loading pre-computed result')
    res.var <- readRDS(res.var.file)
  } else {
    # use 8 cores to parallelize
    res.var <- nmf(ex.var@mat, 2:7, nrun=20, .opt='vP8')
    # print('breaking!')
    # next()
  }

  # save the object 
  saveRDS(res.var, res.var.file)
  
  # # save some figures related to clustering
  # png(path.join(variable.plot.path,'k2_8_rank.png'), width = 12, height=8, units = 'in', res = 300)
  # plot(res.var)
  # dev.off()
  # 
  # png(path.join(variable.out.path,'k2_8_consensus%02d.png'), width = 12, height=8, units = 'in', res = 300)
  # consensusmap(res.var, annCol=as.factor(gdac.clusters[,1]))
  # dev.off()

  # consensus culstering labels
  consensus.labels.var <- matrix(0, nrow=length(ex@cid), ncol=length(2:7), dimnames=list(ex@cid, 2:7))
  for (k in names(res.var$consensus)){
    kint <- as.integer(k)
    klabel <- cutree(hclust(dist(res.var$consensus[[k]]), method='ward.D2'), kint)
    consensus.labels.var[names(klabel), k] <- klabel
  }
  write.table(consensus.labels.var, path.join(variable.out.path,'k2_8_labels.tsv'), sep='\t', quote=F, row.names = T, col.names = T)
  
  
}

# evaluation of cluster calls and generation of the big table 
bestk <- read.table('~/sig_tcga/bsiranos/nmf_clustering_results_TP/relevant_cohort_best_k.tsv', sep='\t', header=T)
rownames(bestk) <- bestk[,1]
# cohorts <- unique(ex.all@cdesc$cohort)
res <- matrix(nrow=0, ncol=3)
for (cohort in rownames(bestk)){
  k <- bestk[cohort,2]
  nmf.file <- path.join('~/sig_tcga/bsiranos/nmf_clustering_results_TP/', tolower(cohort), 'variable_genes/k2_7_labels.tsv')
  nmf.clusters <- read.table(nmf.file, sep='\t', header=T)
  
  nmf.best <- as.matrix(nmf.clusters[,k-1])
  nmf.best <- cbind(sapply(rownames(nmf.clusters), function(x) strsplit(x, split=":")[[1]][1]), cohort, nmf.best)
  rownames(nmf.best) <- rownames(nmf.clusters)
  colnames(nmf.best) <- c('sample_id', 'cohort','cluster')
  res <- rbind(res, nmf.best)
  }

write.table(res, '/cmap/projects/stacks/STK039_TCGA_RNASEQ/nmf/bestcluster_calls.tsv', sep='\t', row.names = T, col.names = T, quote = F)

# table of clustering results for each cohort
res2 <- matrix(nrow=length(unique(res[,'cohort'])), ncol=(3+as.integer(max(res[,'cluster']))))
rownames(res2) <- rownames(bestk)
for (cohort in rownames(bestk)){
 one.cohort <- res[res[,'cohort']==cohort,] 
 res2[cohort,1] <- cohort
 res2[cohort,2] <- nrow(one.cohort)
 res2[cohort,3] <- max(as.integer(one.cohort[,'cluster']))
 res2[cohort,4:ncol(res2)] <- sapply(1:7, function(x) sum(as.integer(one.cohort[,'cluster'])==x))
}

colnames(res2) <- c('cohort', 'nsamp', 'bestk', 1:7)
res2[res2=="0"] <- ""
write.table(res2, '/cmap/projects/stacks/STK039_TCGA_RNASEQ/nmf/bestcluster_stats.tsv', sep='\t', row.names = F, col.names = T, quote=F)

# examining how many landmarks are in the 1500/978 most variable genes in each cohort
# samples in cohorts were using
relevant.samples <- parse.grp('/cmap/projects/stacks/STK039_TCGA_RNASEQ/sample_names_without_duplicate_cohorts.grp')
ex.rel <- subset.gct(ex.all, cid=relevant.samples)

cohorts <- unique(ex.rel@cdesc$cohort)
out.path <- '~/sig_tcga/bsiranos/nmf_clustering_results/'

for (cohort in cohorts[1:length(cohorts)]){
  # subset to members of this cohort
  ex <- subset.gct(ex.all, cid = ex.all@cdesc$cohort==cohort)
  # some rows can be all 0, this leads to NMF failing
  # remove those rows before continuing
  id.zero <- rowSums(ex@mat)==0
  if (any(id.zero)){
    ex <- subset.gct(ex, rid=!id.zero)
  }
  ex.names.short <- sapply(ex@cid, function(x) substr(x,1,15))
  # limit this to lm genes
  # ex.lm <- subset.gct(ex, rid=ex@rid[ex@rdesc$pr_is_lm==1])
  # limit to most variable genes
  gene.vars <- apply(ex@mat, 1, function(x) var(x, na.rm = T))
  gene.means <- apply(ex@mat, 1, function(x) mean(x, na.rm = T))
  gene.vars.sorted <- sort(gene.vars, decreasing = T)
  gene.means.sorted <- sort(gene.means, decreasing = T)
  lm.ind<-ex@rdesc[names(gene.vars.sorted), 'pr_is_lm']
  lm.ind.mean<-ex@rdesc[names(gene.means.sorted), 'pr_is_lm']
  
  # ngene <- 1500
  # ex.var <- subset.gct(ex, rid=names(gene.vars)[order(gene.vars, decreasing = T)[1:ngene]])
  # 
  # ngene2 <- 978
  # ex.var2 <- subset.gct(ex, rid=names(gene.vars)[order(gene.vars, decreasing = T)[1:ngene2]])
  # # intersection of lm and these two lists
  # 
  # int1  <- sum(ex.var@rdesc$pr_is_lm)

  big.plot.path <- path.join(out.path, tolower(cohort), paste('variability_stats','.pdf', sep=''))
  pdf(big.plot.path,pointsize = 8)
  par(mfrow=c(3,2))
  
  nplot <- 1500
  # plot genes sorted by variance, highlight landmarks
  lm.ind.nplot <-ex@rdesc[names(gene.vars.sorted)[1:nplot], 'pr_is_lm']
  lm.ind.mean.nplot <-ex@rdesc[names(gene.means.sorted)[1:nplot], 'pr_is_lm']
  
  # plot.path <- path.join(out.path, tolower(cohort), paste('lm_genes_in_variable_',nplot,'.png', sep=''))
  # png(plot.path)
  plot((1:nplot), gene.vars.sorted[1:nplot], col=rgb(0,0,0,0.1), pch=1,
       main=paste(cohort, 'sorted gene expression variance'), ylab='variance of gene', xlab='gene index')
  points((1:nplot)[!!lm.ind.nplot], gene.vars.sorted[1:nplot][!!lm.ind.nplot], col='red3', pch='l', cex=1.5)
  text(x=nplot*.75, y=gene.vars.sorted[1], paste('landmarks in subset: ', sum(lm.ind.nplot), '/',nplot, sep=''))
  legend(x=nplot*.75, y=gene.vars.sorted[1] - ((gene.vars.sorted[1] - gene.vars.sorted[nplot])*0.1), 
         legend = c('all genes','landmarks'), col=c('black','red3'), lwd=2, box.lty = 0)
  print(paste(cohort, ': landmarks in var subset: ', sum(lm.ind.nplot), '/',nplot, sep=''))
  # dev.off()
  
  # plot genes sorted by expression, highlight landmarks
  plot((1:nplot), gene.means.sorted[1:nplot], col=rgb(0,0,0,0.1), pch=1,
       main=paste(cohort, 'sorted gene expression mean'), ylab='meane expression of gene', xlab='gene index')
  points((1:nplot)[!!lm.ind.mean.nplot], gene.means.sorted[1:nplot][!!lm.ind.mean.nplot], col='red3', pch='l', cex=1.5)
  text(x=nplot*.75, y=gene.means.sorted[1], paste('landmarks in subset: ', sum(lm.ind.mean.nplot), '/',nplot, sep=''))
  legend(x=nplot*.75, y=gene.means.sorted[1] - ((gene.means.sorted[1] - gene.means.sorted[nplot])*0.1), 
         legend = c('all genes','landmarks'), col=c('black','red3'), lwd=2, box.lty = 0)
  print(paste(cohort, ': landmarks in mean subset: ', sum(lm.ind.mean.nplot), '/',nplot, sep=''))
  # dev.off()
  
  # placement in bing genes?
  ex.bing <- subset.gct(ex, rid=ex@rid[ex@rdesc$pr_is_bing==1])
  gene.vars.bing <- gene.vars[ex@rdesc$pr_is_bing==1]
  gene.vars.bing.sorted <- sort(gene.vars.bing, decreasing = T)
  lm.ind.bing <-ex@rdesc[names(gene.vars.bing.sorted), 'pr_is_lm']

  # do a CDF plot of how many genes it takes to get all 
  # x = # of all genes
  # y = # landmarks
  # plot.path2 <- path.join(out.path, tolower(cohort), paste('lm_palcement_variable','.png', sep=''))
  # png(plot.path2)
  plot(which(lm.ind==1)/length(lm.ind), (1:sum(lm.ind))/sum(lm.ind), type='l', lwd=2,
       xlab='fraction of genes, sorted by var', ylab='fraction of landmarks, sorted by var',
       main=paste(cohort, 'landmark placement in genes sorted by variance',sep='\n'))
  lines(which(lm.ind.bing==1)/length(lm.ind.bing), (1:sum(lm.ind.bing))/sum(lm.ind.bing), type='l', col='blue', lwd=2)
  abline(a=0, b=1, col='red1')
  legend(0,1, c('landmarks in all', 'landmarks in bing', 'expected'),col = c('black', 'blue', 'red1'), lwd = 1,box.lwd = 0)
  # dev.off()
  
  # how do variance and mean play in these cohorts
  # plot.path3 <- path.join(out.path, tolower(cohort), paste('mv_scatter','.png', sep=''))
  # png(plot.path3)
  plot(gene.means, gene.vars, col=rgb(0,0,0,0.5), 
       main=paste(cohort,'mean-variance scatter'), xlab='mean expression', ylab='gene variance')
  points(gene.means[!!ex@rdesc[,'pr_is_lm']], gene.vars[!!ex@rdesc[,'pr_is_lm']],
         col='red1', pch=20, cex=0.75)
  legend(max(gene.means)*0.75, max(gene.vars), c('all genes', 'landmarks'), col = c('black', 'red1'), pch = c(1,20), box.lwd = 0)
  # dev.off()
  
  plot(gene.means, sqrt(gene.vars)/gene.means, col=rgb(0,0,0,0.5), 
       main=paste(cohort,'mean-CV scatter'), xlab='mean expression', ylab='gene CV')
  points(gene.means[!!ex@rdesc[,'pr_is_lm']], sqrt(gene.vars[!!ex@rdesc[,'pr_is_lm']])/gene.means[!!ex@rdesc[,'pr_is_lm']],
         col='red1', pch=20, cex=0.75)
  legend(max(gene.means)*0.75, max(sqrt(gene.vars)/gene.means), c('all genes', 'landmarks'), col = c('black', 'red1'), pch = c(1,20), box.lwd = 0)
  # dev.off()
  
  # plot.path4 <- path.join(out.path, tolower(cohort), paste('var_gene_boxplot','.png', sep=''))
  # png(plot.path4)
  boxplot(t(ex@mat[names(gene.vars.sorted)[1:20],]), names=ex@rdesc[names(gene.vars.sorted)[1:20], 'pr_gene_symbol'], las=2,
          main=paste(cohort,' 20 most variable genes'), ylab='log2 expression')
  dev.off()
  
}

# variable genes vs top expressed
lm.var <- c(88,65,64,57,53,51,50,48,47,47,46,45,44,44,39,39,38,38,37,36,36,36,32,29,29,28,27,26,25,23)
lm.mean <- c(186,180,201,192,188,217,179,203,200,200,199,213,208,205,213,203,203,208,199,214,206,210,192,217,201,209,201,223,210,198)

lm.stats <- data.frame(var=lm.var, mean=lm.mean)
rownames(lm.stats) <- c('LAML','GBMLGG','ACC','LIHC','THCA','CHOL','UVM','DLBC','KICH','THYM','PCPG','SKCM','BRCA','KIRC','BLCA','KIRP','PRAD','SARC','MESO','CESC','COADREAD','LUAD','TGCT','LUSC','UCS','UCEC','PAAD','HNSC','STES','OV')
boxplot(lm.stats, main='lm genes in 1500 top, across cohorts', names = c('top variable','top expressed'), ylab='number of lm genes')
sd(lm.stats$var)/mean(lm.stats$var)
sd(lm.stats$mean)/mean(lm.stats$mean)

# gene lists across cohorts
lm.inds <- lapply(cohorts, function(x) {
ex <- subset.gct(ex.all, cid = ex.all@cdesc$cohort==x)
# some rows can be all 0, this leads to NMF failing
# remove those rows before continuing
id.zero <- rowSums(ex@mat)==0
if (any(id.zero)){
  ex <- subset.gct(ex, rid=!id.zero)
}
gene.vars <- apply(ex@mat, 1, function(y) var(y, na.rm = T))
gene.means <- apply(ex@mat, 1, function(y) mean(y, na.rm = T))
gene.vars.sorted <- sort(gene.vars, decreasing = T)
gene.means.sorted <- sort(gene.means, decreasing = T)
lm.ind<-ex@rdesc[names(gene.vars.sorted), 'pr_is_lm']
lm.ind.mean<-ex@rdesc[names(gene.means.sorted), 'pr_is_lm']
cbind(names(gene.vars.sorted),lm.ind,names(gene.means.sorted),lm.ind.mean)
})
names(lm.inds) <- cohorts

num <- 1500
# top most variable
top.var.mat <- sapply(lm.inds, function(x) x[1:num, 1])
top.var.mat.ind <- sapply(lm.inds, function(x) x[1:num, 2])

comb.ind <- combn(30, 2)
pair.intersect <- apply(comb.ind, 2, function(x) length(intersect(top.var.mat[,x[1]],top.var.mat[,x[2]])))
intersect.mat <- matrix(0, nrow=30, ncol=30, dimnames=list(cohorts, cohorts))
# is this right? Why always triangles...
intersect.mat[lower.tri(intersect.mat)] <- pair.intersect
intersect.mat <- t(intersect.mat)
intersect.mat[lower.tri(intersect.mat)] <- pair.intersect
diag(intersect.mat) <- NA
heatmap.2(intersect.mat, trace='none', col=heat.colors(64))

pair.intersect.lm <- apply(comb.ind, 2, function(x) 
  length(intersect(top.var.mat[,x[1]][top.var.mat.ind[,x[1]]>0],
                   top.var.mat[,x[2]][top.var.mat.ind[,x[2]]>0])) / 
    min(length(top.var.mat[,x[1]][top.var.mat.ind[,x[1]]>0]), 
        length(top.var.mat[,x[2]][top.var.mat.ind[,x[2]]>0])))

intersect.mat.lm <- matrix(0, nrow=30, ncol=30, dimnames=list(cohorts, cohorts))
# lower.tri corresponds to combn ordering
intersect.mat.lm[lower.tri(intersect.mat.lm)] <- pair.intersect.lm
intersect.mat.lm <- t(intersect.mat.lm)
intersect.mat.lm[lower.tri(intersect.mat.lm)] <- pair.intersect.lm
diag(intersect.mat.lm) <- NA
heatmap.2(intersect.mat.lm, trace='none', col=heat.colors(64))

# Are certain genes variable across cohorts? 
# top globally variable genes also variable across multiple?
global.var <- apply(ex.all@mat, 1, var)
global.nzero <- apply(ex.all@mat, 1, function(x) sum(x==0))
global.var <- sort(global.var, decreasing = T)
hist(ex.all@mat[names(global.var)[1],])
plot(global.var, global.nzero[names(global.var)]/ncol(ex.all@mat), col=rgb(0,0,0,0.5), xlab='Gene variance across all samples',
     ylab='Fraction of samples with 0 expression', main='Global variance relationships')

# compute variance in each cohort ------
bestk <- read.table('~/sig_tcga/bsiranos/nmf_clustering_results/relevant_cohort_best_k.tsv', sep='\t', header=T)
cohorts <- rownames(bestk)
ex.rel <- subset.gct(ex.all, cid=ex.all@cid[ex.all@cdesc$cohort %in% cohorts])
cohort.var <- sapply(cohorts, function(x) apply(ex.all@mat[,ex.all@cdesc$cohort==x], 1, var))
cohort.var <- cbind(cohort.var, global.var[rownames(cohort.var)])
cohort.var.rank <- apply(cohort.var, 2, function(x) rank(-x))
rownames(cohort.var.rank) <- rownames(cohort.var)

nplot <- 1500
par(mfrow=c(1,2))
plot(global.var[1:nplot], rowMeans(cohort.var[names(global.var)[1:nplot],]), col=rgb(0,0,0,0.5),
     xlab='Global variance', ylab='Mean variance of cohorts', main='Global vs mean cohort variance')
plot(global.var[1:nplot], rowMedians(cohosrt.var[names(global.var)[1:nplot],]), col=rgb(0,0,0,0.5),
     xlab='Global variance', ylab='Median variance of cohorts', main='Global vs median cohort variance')

# where to golbally most variable genes rank in each cohort?
cohort.var.positions <- cohort.var.rank[names(global.var)[1:10],]

cohort.var.rank[names(sort(cohort.var[,1], decreasing = T)[1:10]),]

# boxplots of genes in different cohorts ------
plot_var_global <- function(n) {
gene.id <- names(global.var)[n]
# get dataframe form matrix
boxlist <- sapply(cohorts, function(x) ex.rel@mat[gene.id, ex.rel@cid[ex.rel@cdesc$cohort==x]])
boxlist[[31]] <- ex.rel@mat[gene.id,]
names(boxlist)[31] <- 'GLOBAL'
boxplot(boxlist, las=2, main=paste(gene.id, ' expression  |  global rank: ', which(names(global.var)==gene.id), sep=''),
        ylim=c(0,max(ex.rel@mat[gene.id, ])+3), ylab='RSEM expression log2')

text(x=1:31, y=max(ex.rel@mat[gene.id, ])+2, labels = cohort.var.rank[gene.id,],
     col='red3', cex =1, srt=90)
}

# examine if normal samples all appear in the same NMF cluster for different cohorts ------
bestk <- read.table('~/sig_tcga/bsiranos/nmf_clustering_results/relevant_cohort_best_k.tsv', sep='\t', header=T)
bestclus <- read.table('/cmap/projects/stacks/STK039_TCGA_RNASEQ/nmf/bestcluster_calls.tsv', header=T, fill=T)
rownames(bestclus) <- bestclus[,1]
cohorts <- unique(bestclus$cohort)
sample.meta <- read.table('/cmap/projects/stacks/STK039_TCGA_RNASEQ/col_meta.txt', sep='\t', header=T, quote='')
rownames(sample.meta) <- sample.meta$tcga_id
relevant.samples <- parse.grp('/cmap/projects/stacks/STK039_TCGA_RNASEQ/sample_names_without_duplicate_cohorts.grp')
sample.meta.rel <- sample.meta[rownames(sample.meta) %in% relevant.samples, ]

cat((sort(table(sample.meta.rel[sample.meta.rel$sample_type_abbr=='TP', 'cohort']), decreasing=T)), sep='\n')
cat(names(sort(table(sample.meta.rel[sample.meta.rel$sample_type_abbr=='TP', 'cohort']), decreasing=T)), sep='\n')
cat((sort(table(sample.meta.rel[sample.meta.rel$sample_type_abbr=='NT', 'cohort']), decreasing=T)), sep='\n')
cat(names(sort(table(sample.meta.rel[sample.meta.rel$sample_type_abbr=='NT', 'cohort']), decreasing=T)), sep='\n')
cat((sort(table(sample.meta.rel[sample.meta.rel$sample_type_abbr=='TM', 'cohort']), decreasing=T)), sep='\n')
cat(names(sort(table(sample.meta.rel[sample.meta.rel$sample_type_abbr=='TM', 'cohort']), decreasing=T)), sep='\n')

for (cohort in cohorts){
  k <- bestk[cohort,1]
  primary.samples <- sample.meta[(sample.meta$cohort==cohort & sample.meta$sample_type_abbr=='TP'), 'tcga_id']
  normal.samples <- sample.meta[(sample.meta$cohort==cohort & sample.meta$sample_type_abbr=='NT'), 'tcga_id']
  primary.clusters <- factor(bestclus[primary.samples, 'cluster'], levels=1:k)
  normal.clusters <- factor(bestclus[normal.samples, 'cluster'], levels=1:k)
  message(cohort)
  print(table(primary.clusters))
  print(table(normal.clusters))
  message('--------------')
  }









