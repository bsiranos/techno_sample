# diff consensus matrix of 978 variable and LM
# how much do we lose by switching to LMs?
call.file <- read.table('~/sig_tcga/bsiranos/nmf_clustering_results/relevant_cohort_best_k.tsv', sep='\t')
cohorts <- rownames(call.file)
cohort <- 'GBMLGG'
nmf.file.var <- path.join('~/sig_tcga/bsiranos/nmf_clustering_results/', tolower(cohort), '978_variable_genes/nmf_result.rds')
nmf.file.lm <- path.join('~/sig_tcga/bsiranos/nmf_clustering_results/', tolower(cohort), 'lm_genes/nmf_result.rds')
nmf.var <- readRDS(nmf.file.var)
nmf.lm <- readRDS(nmf.file.lm)

k <- call.file[cohort,1]


mat1 <- nmf.var$consensus[[k-1]]
mat2 <- nmf.lm$consensus[[k-1]]
# order these by the clustering of mat1
hc <- hclust(dist(mat1), method='ward.D2')
mat1 <- mat1[hc$order, hc$order]
mat2 <- mat2[hc$order, hc$order]

subtr <- mat1 - mat2   
heatmap.2(subtr, trace='none', col=redblu.colors(256), Rowv=NA, Colv=NA)