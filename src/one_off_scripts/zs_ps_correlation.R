# try this on some simple CMap data

# first expression zscore and cmap correlation - simple linear
# Start with BRCA data
zs <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KBING_TXONLY_n10255x10147.gctx', cid='~/cmap_tcga/cohort_grp/brca_samples.grp')
ps <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/gutc_results/gutc/pert_pcl_all_n10255x82902.gctx', cid='~/cmap_tcga/cohort_grp/brca_samples.grp')
ns <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/gutc_results/gutc/ns_pert_summary.gctx', cid='~/cmap_tcga/cohort_grp/brca_samples.grp')
# zs.old <- parse.gctx('~/sig_tcga/ds/TCGA_RNASEQ.RSEM_ZSCORE.SUBTYPE_ALL.L1KBING_n2493x7438.gctx', cid='~/cmap_tcga/cohort_grp/brca_samples.grp')
# ps.old <- parse.gctx('~/sig_tcga/by_sample_clustered/gutcv2_cohorts/brca/pert_cell_brca_n1093x82891.gctx')
# ps <- ps.old
# zs <- zs.old

# try adam10
gene <- 'ADAM10'
pert.rid <- ps@rid[ps@rdesc$cell_id=='summly' &
                   ps@rdesc$pert_iname==gene &
                   ps@rdesc$pert_type=='trt_sh.cgs']
hist(zs@mat[zs@rid[zs@rdesc$pr_gene_symbol==gene],])
plot(zs@mat[zs@rid[zs@rdesc$pr_gene_symbol==gene],], ps@mat[pert.rid,])

adam.model <- data.frame(zs=zs@mat[zs@rid[zs@rdesc$pr_gene_symbol==gene],], 
                         ps=ps@mat[pert.rid,])
lf <- lm(ps ~ zs, adam.model)
plot(lf)

# which gene is the best fit for adam10 kd?
# find genes with SHRNAs
genes.shrna <- intersect(zs@rdesc$pr_gene_symbol, ps@rdesc$pert_iname[ps@rdesc$pert_type=='trt_sh.cgs' & ps@rdesc$cell_id=='summly'])
zs.rids <- sapply(genes.shrna, function(x) zs@rid[zs@rdesc$pr_gene_symbol==x])
ps.rids <- sapply(genes.shrna, function(x) ps@rid[ps@rdesc$pert_iname==x & ps@rdesc$pert_type=='trt_sh.cgs' & ps@rdesc$cell_id=='summly'])
ps.shrna <- as.data.frame(t(ps@mat[ps.rids,zs@cid]))
ns.shrna <- as.data.frame(t(ns@mat[ps.rids,zs@cid]))
zs.shrna <- as.data.frame(t(zs@mat[zs.rids,zs@cid]))
colnames(ps.shrna) <- sapply(genes.shrna, function(x) paste('ps_', gsub('-','_',x), sep=''))
colnames(ns.shrna) <- sapply(genes.shrna, function(x) paste('ns_', gsub('-','_',x), sep=''))
colnames(zs.shrna) <- sapply(genes.shrna, function(x) gsub('-','_',x))
fit.data <- cbind(zs.shrna, ps.shrna)
fit.data.ns <- cbind(zs.shrna, ns.shrna)

r2mat <- matrix(0, nrow=length(genes.shrna), ncol=length(genes.shrna))
for (p in 1:length(genes.shrna)){
  print(p)
  for (g in 1:length(genes.shrna)){
    lf <- lm(as.formula(paste(colnames(fit.data)[p+length(genes.shrna)], '~', colnames(fit.data)[g])), data=fit.data)
    r2mat[p,g] <- summary(lf)$r.squared
  }
}

# simpler - only do matched cor
r2.matched <- rep(0, times=length(genes.shrna))
lf.matched <- lapply(1:length(genes.shrna), function(p) {
  print(p)
  lm(as.formula(paste(colnames(fit.data)[p+length(genes.shrna)], '~', colnames(fit.data)[p])), data=fit.data)
})
lf.matched.summary <- as.data.frame(t(sapply(lf.matched, function(x) c(x$coefficients[2], summary(x)$r.squared))))
colnames(lf.matched.summary) <- c('coefficient', 'R2')
rownames(lf.matched.summary) <- genes.shrna
lf.matched.summary[order(lf.matched.summary$R2, decreasing = T),][1:20,]

# matched cor with ns
lf.matched.ns <- lapply(1:length(genes.shrna), function(p) {
  print(p)
  lm(as.formula(paste(colnames(fit.data.ns)[p+length(genes.shrna)], '~', colnames(fit.data.ns)[p])), data=fit.data.ns)
})
lf.matched.ns.summary <- as.data.frame(t(sapply(lf.matched.ns, function(x) c(x$coefficients[2], summary(x)$r.squared))))
colnames(lf.matched.ns.summary) <- c('coefficient', 'R2')
rownames(lf.matched.ns.summary) <- genes.shrna
lf.matched.ns.summary[order(lf.matched.ns.summary$R2, decreasing = T),][1:20,]
# compare correlation done with ps and with ns
plot(lf.matched.summary$R2, lf.matched.ns.summary$R2)
# pretty much the same
summary(lm(a~b, data.frame(a=lf.matched.summary$R2, b=lf.matched.ns.summary$R2)))

# matrix of all r2 values
r2mat <- as.matrix(read.table('~/cmap_tcga/connectivity_correlations/expression_ps_correlation/r2_all_shrna.txt', sep='\t'))
r2.high <- which(r2mat>0.40, arr.ind = T)
r2.diag <- diag(r2mat)
boxplot(c(r2mat), r2.diag)
t.test(c(r2mat), r2.diag)

# some sample set information - those big tables I made earlier!
long.table.cna <- read.table('~/cmap_tcga/alteration_sample_sets/long_table_alterations_tx_only.tsv', sep='\t', header=T)
long.table.mut <- read.table('~/cmap_tcga/alteration_sample_sets/long_table_mutation_alterations_tx_only.tsv', sep='\t', header=T)
long.table.out <- read.table('~/cmap_tcga/outlier_sample_sets/long_table_outliers_tx_only.tsv', sep='\t', header = T)
long.table.all <- rbind(long.table.cna, long.table.mut, long.table.out)
write.table(long.table.all, '~/cmap_tcga/long_table_alterations.txt', sep='\t', row.names = F, col.names = T, quote = F)
brca.table.cna <- subset(long.table.cna, subset = cohort=='BRCA')
brca.table.mut <- subset(long.table.mut, subset = cohort=='BRCA')
brca.table.out <- subset(long.table.out, subset = cohort=='BRCA')
brca.table <- rbind(brca.table.cna, brca.table.mut, brca.table.out)
rownames(brca.table) <- NULL
length(unique(brca.table$tcga_id))
unique(brca.table$alteration_type)
subset(brca.table, altered_gene=='ADAM10')
subset(brca.table, altered_gene=='TP53')

# okay, let's look at TP53 mutations. 
# mutation, cna loss each contibute 1 value
tp53.table <- subset(brca.table, altered_gene=='TP53')
mut.code <- data.frame(tp53=rep(0, times=nrow(ps.shrna)))
rownames(mut.code) <- rownames(ps.shrna)
# add 1 for each mutation
mut.samples <- subset(tp53.table, alteration_type=='mutation')
mut.code[mut.samples$tcga_id, 'tp53'] <- mut.code[mut.samples$tcga_id, 'tp53']+1  
cna.samples  <- subset(tp53.table, alteration_type=='CNA_LOSS')
mut.code[cna.samples$tcga_id, 'tp53'] <- mut.code[cna.samples$tcga_id, 'tp53'] +1
# gain.samples  <- subset(tp53.table, alteration_type=='CNA_GAIN')
# mut.code[gain.samples$tcga_id, 'tp53'] <- mut.code[gain.samples$tcga_id, 'tp53']+1  
# outlier.neg.samples  <- subset(tp53.table, alteration_type=='OUTLIER_NEG')
# mut.code[outlier.neg.samples$tcga_id, 'tp53'] <- mut.code[outlier.neg.samples$tcga_id, 'tp53']+1  

tp53.shrna <- subset(ps@rdesc, pert_type=='trt_sh.cgs' & pert_iname=='TP53' & cell_id=='summly', select=id)[,1]
tp53.df <- data.frame(ps=ps@mat[tp53.shrna,], mut.code=as.factor(mut.code$tp53))
plot(tp53.df$mut.code, tp53.df$ps)
lf <- lm(ps ~ mut.code, tp53.df)
summary(lf)
table(tp53.df$mut.code)

# do for all KD perts
ps.shrna.tp53 <- cbind(ps.shrna, mut.code=tp53.df$mut.code)
lf.tp53.all <- lapply(1:length(genes.shrna), function(i) {
  lm(as.formula(paste(colnames(ps.shrna.tp53)[i], '~ mut.code')), data=ps.shrna.tp53)
})
names(lf.tp53.all) <- genes.shrna
all.tp53.r2 <- sapply(lf.tp53.all, function(x) summary(x)$r.squared)




