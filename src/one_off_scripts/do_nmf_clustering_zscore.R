# nmf of some of the zscored data
ds.zs <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KBING_n11070x10147.gctx')
cluster.files.path <- '~/sig_tcga/bsiranos/tcga_mrna_clusters/analyses__2015_08_21/bestclus/'
cluster.files <- list.files(cluster.files.path)
# default out path
out.path <- '~/sig_tcga/bsiranos/nmf_clustering_zscore_results/'
plot.path <- '~/sig_tcga/bsiranos/nmf_clustering_zscore_results/'

cohort <- 'BRCA'
ex <- subset.gct(ds.zs, cid=ds.zs@cid[ds.zs@cdesc$cohort==cohort])
gdac.clusters <- read.table(cluster.file, sep='\t', row.names=1)

id.zero <- rowSums(ex@mat)==0
if (any(id.zero)){
  ex <- subset.gct(ex, rid=!id.zero)
}
ex.names.short <- sapply(ex@cid, function(x) substr(x,1,15))

# limit this to lm genes
ex.lm <- subset.gct(ex, rid=ex@rid[ex@rdesc$pr_is_lm==1])
# limit to most variable genes
gene.vars <- apply(ex@mat, 1, var)
ngene <- 1500
ex.var <- subset.gct(ex, rid=names(gene.vars)[order(gene.vars, decreasing = T)[1:ngene]])

# check if all of the samples in the ex have clusters, and vice versa
# zero entries for things without clusters 
no.cluster <- ex.names.short[!(ex.names.short %in% rownames(gdac.clusters))]
no.cluster.mat <- cbind(rep(0, length(no.cluster)), rep(0, length(no.cluster)))
rownames(no.cluster.mat) <- no.cluster
colnames(no.cluster.mat) <- colnames(gdac.clusters)
gdac.clusters <- rbind(gdac.clusters, no.cluster.mat)
# eliminate entries not in ex and sort 
gdac.clusters <- gdac.clusters[ex.names.short, ]

# LANDMARK genes NMF clustering
# where to save results
lm.out.path <- path.join(out.path, tolower(cohort), 'lm_genes')
lm.plot.path <- path.join(plot.path, tolower(cohort), 'lm_genes')
if(!dir.exists(lm.out.path)){
  dir.create(lm.out.path, recursive = T)
}
if(!dir.exists(lm.plot.path)){
  dir.create(lm.plot.path, recursive = T)
}
# doing NMF clustering: k 2-8 with 20 repetitions per size
# check if we already did clustering and load data if so.
res.file <-  path.join(lm.out.path, 'nmf_result.rds')
if (file.exists(res.file)){
  print('Loading pre-computed result')
  # res <- readRDS(res.file)
} else {
  # use 8 cores to parallelize
  res <- nmf(ex.lm@mat, 2:8, nrun=20, .opt='vP8')
  print('breaking!')
  next()
}

# save the object 
saveRDS(res, path.join(lm.out.path, 'nmf_result.rds'))

# # save some figures related to clustering
# png(path.join(lm.plot.path,'k2_8_rank.png'), width = 12, height=8, units = 'in', res = 300)
# plot(res)
# dev.off()

# png(path.join(lm.out.path,'k2_8_consensus%02d.png'), width = 12, height=8, units = 'in', res = 300)
# consensusmap(res, annCol=as.factor(gdac.clusters[,1]))
# dev.off()

# consensus culstering labels
# for each k, do hclust on the consensus matrix (euclidean distance, ward.D2 method)
# cut the tree at k and use that as cluster assignment 
# consensus.labels <- matrix(0, nrow=length(ex@cid), ncol=length(2:8), dimnames=list(ex@cid, 2:8))
# for (k in names(res$consensus)){
#   kint <- as.integer(k)
#   klabel <- cutree(hclust(dist(res$consensus[[k]]), method='ward.D2'), kint)
#   consensus.labels[names(klabel), k] <- klabel
# }
# write.table(consensus.labels, path.join(lm.out.path,'k2_8_labels.tsv'), sep='\t', quote=F, row.names = T, col.names = T)
# 
# VARIABLE genes NMF clustering
# where to save results
variable.out.path <- path.join(out.path, tolower(cohort), 'variable_genes')
variable.plot.path <- path.join(plot.path, tolower(cohort), 'variable_genes')
if(!dir.exists(variable.out.path)){
  dir.create(variable.out.path, recursive = T)
}
if(!dir.exists(variable.plot.path)){
  dir.create(variable.plot.path, recursive = T)
}

# doing NMF clustering: k 2-8 with 20 repetitions per size
# check if we already did clustering and load data if so.
res.var.file <-  path.join(variable.out.path, 'nmf_result.rds')
if (file.exists(res.var.file)){
  print('Loading pre-computed result')
  res.var <- readRDS(res.var.file)
} else {
  # use 8 cores to parallelize
  res.var <- nmf(ex.var@mat, 2:8, nrun=20, .opt='vP8')
  print('breaking!')
  next()
}

# save the object 
saveRDS(res.var, res.var.file)

# # # save some figures related to clustering
# png(path.join(variable.plot.path,'k2_8_rank.png'), width = 12, height=8, units = 'in', res = 300)
# plot(res.var)
# dev.off()
# # 
# png(path.join(variable.out.path,'k2_8_consensus%02d.png'), width = 12, height=8, units = 'in', res = 300)
# consensusmap(res.var, annCol=as.factor(gdac.clusters[,1]))
# dev.off()

# consensus culstering labels
consensus.labels.var <- matrix(0, nrow=length(ex@cid), ncol=length(2:8), dimnames=list(ex@cid, 2:8))
for (k in names(res.var$consensus)){
  kint <- as.integer(k)
  klabel <- cutree(hclust(dist(res.var$consensus[[k]]), method='ward.D2'), kint)
  consensus.labels.var[names(klabel), k] <- klabel
}
write.table(consensus.labels.var, path.join(variable.out.path,'k2_8_labels.tsv'), sep='\t', quote=F, row.names = T, col.names = T)
