# get survival information for TCGA samples from cbioportal 
# need this later in the apps and better to get it now than downloading everything by hand...
library(cgdsr)
# read in the col meta to filter to sample we have and are interested in 
# lets use tx only- that more likely to be ambiguous for patient ids
tcga.samples <- parse.grp('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/tcga_id.grp')
short.to.long <- tcga.samples
names(short.to.long) <- sapply(tcga.samples, function(x) substr(x,1,15))

cohorts=c("acc","blca","brca","cesc","chol","coadread","dlbc","esca","gbm","hnsc","kich","kirc","kirp","laml","lgg","lihc","luad","lusc","meso","ov","paad","pcpg","prad","sarc","skcm","stad","tgct","thca","thym","ucec","ucs","uvm" )

for (input.cohort in cohorts){
  outf <- path.join('~/cmap_tcga/clinical/survival/', paste(input.cohort, 'survival.txt', sep='_'))
  
  mycgds = CGDS("http://www.cbioportal.org/public-portal/")
  # available cancer studies
  cancer.studies <- getCancerStudies(mycgds)
  # limit to provisional tcga studies
  tcga.studies <- grep('tcga',cancer.studies[,1], value=T)
  tcga.studies <- grep('pub', tcga.studies, value = T, invert = T)
  
  # match the user given cohort to the cbio type  
  cohort <- grep(input.cohort, tcga.studies, value = T)
  clin <- getClinicalData(mycgds, paste(cohort, 'all', sep='_'))
  surv <- clin[,c('OS_STATUS','OS_MONTHS','DFS_STATUS','DFS_MONTHS','VITAL_STATUS')]
  rownames(surv) <- sapply(rownames(surv), function(x) gsub('\\.', '\\-',x))
  surv$short_id <- rownames(surv)
  surv$tcga_id <- short.to.long[surv$short_id]
  surv <- surv[,c(6,7,1,2,3,4,5)]
  write.table(surv, outf, sep='\t', col.names = T, row.names = F, quote = F )
  
}

# combined the individual tables into one in bash
# now sort through the large table and fix things with NA ids, etc
pc <- read.table('~/cmap_tcga/clinical/survival/pancan.txt', sep='\t', header=T)
pc.filt <- pc[!(is.na(pc$tcga_id)),]

# two samples are missing survival data
tcga.samples[!(tcga.samples %in% pc.filt$tcga_id)]
tcga.samples[!(names(short.to.long) %in% pc.filt$short_id)]

# missing survival data for these two samples
# [1] "TCGA-AA-3970-01A-01R-1022-07:COADREAD"
# [2] "TCGA-XV-AB01-06A-12R-A40A-07:SKCM"    
# add them to the table
pc.filt <- rbind(pc.filt, c('TCGA-AA-3970-01', 'TCGA-AA-3970-01A-01R-1022-07:COADREAD',NA,NA,NA,NA,NA))
pc.filt <- rbind(pc.filt, c('TCGA-XV-AB01-06', 'TCGA-XV-AB01-06A-12R-A40A-07:SKCM',NA,NA,NA,NA,NA))

# add cohort annotation
pc.filt$cohort <- sapply(pc.filt$tcga_id, function(x) strsplit(x, split='\\:')[[1]][2])
# write out df
write.table(pc.filt, '~/cmap_tcga/clinical/survival/pancan_survival.txt', sep='\t', row.names = F, col.names = T, quote=F)
file.remove('~/cmap_tcga/clinical/survival/pancan.txt')
