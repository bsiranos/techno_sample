
# ps <- parse.gctx('~/projects/techno_sample/resources/query/ps_brca_small_n1093x10.gctx')
# # ps2 <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/gutc_results/gutc/pert_pcl_all_n10255x82902.gctx', rid=1:100)
# ss <- parse.gmt('~/projects/techno_sample/resources/query/outlier_ss_brca_100.gmt')
ss <- parse.gmt('~/sig_tcga/by_sample_clustered/gutcv2_cohorts/brca/query_gmt/outliers.gmt')
gsets <- lapply(ss, function(x) x$entry)

nsim <- 10000
ndiscr <- ncol(ps@mat)
sdev <- 1/sqrt(ndiscr)
incrs <- sapply(1:nsim, function(x) rnorm(n=ndiscr,sd=sdev))

# newres <- lapply(1:nrow(ps@mat), function(x) WKS.test(ps@mat[x,], gsets, nsim=nsim, incrs=incrs))
# plot(-log10(r1$pvals), -log10(newres[[2]]$pvals))
# plot(-log10(g1$pvals), -log10(newres[[2]]$pvals))

#profiling
library(profvis)
p2 <- profvis(WKS.test(ps@mat[2,], gsets, incrs=incrs, nsim=nsim))
p2


WKS.test <- function(w,db,nsim=10000,ndiscr=NULL,ranked=FALSE,alternative="two.sided", incrs=NULL){
  #   Takes a named vector w and a database db. Performs
  #   the WKS test with alternative "two.sided", "less", or "greater". 
  #   The limiting distribution is estimated through nsim Monte-Carlo 
  #   simulations, the number of discretization points being ndiscr. 
  #   If ndiscr is missing, then the number of discretization points
  #   is equal to the size of w.
  #   Calculates the p-values (upper bound of the one-sided confidence 
  #   interval with level 0.95) 
  #   
  
  correl.vector <- sort(w,decreasing=TRUE) # sort the vector in decreasing order 
  acv <- abs(correl.vector)
  lg <- length(correl.vector)
  gene.list <- names(correl.vector)        # all genes
  
  # Precompute G(t): cumulated sum of weights. This stays the same for all gene sets.
  Gt <- cumsum(acv)/sum(acv)
  # Evaluate the WKS statistic
  wks.eval <- unlist(lapply(db,function(x) WKS.EnrichScore(gene.list, x, acv, alternative, Gt)))
  # observed test statistic
  ots <- wks.eval
  # eliminate NA values corresponding to gene sets with an empty intersection with the list
  ots <- na.omit(ots)               
  
  # if not provided ndiscr
  if (is.null(ndiscr)){
    ndiscr <- length(w)
  }
  
  # if not provided incrs
  if (is.null(incrs)){
    sdev <- 1/sqrt(ndiscr)
    incrs <- sapply(1:nsim, function(x) rnorm(n=ndiscr,sd=sdev))
  }
  
  # mb <- microbenchmark(
  ld <- {lim.distr(weights=abs(correl.vector),
                   nsim=nsim,alternative, incrs)} # estimation of limiting distribution
  # , times = 50)
  
  pvalsm <- sapply(abs(ots),function(x) sum(ld>=x))
  
  # upper bound one-sided confidence
  pvalsci <- sapply(pvalsm,function(x) # interval with level 0.95
  {prop.test(x,nsim,alternative="less")$conf.int[2]})
  
  # return(pvalsci)
  #  now return a list of pvalues and ES
  return(list(pvals=pvalsci, statistic=ots))
}                                       # end function WKS.test



GSEA.test <- function(w,db,nsim=1000){
  #   Takes a named vector w and a database db. Performs the GSEA test. 
  #   The number of Monte-Carlo simulations for every gene set 
  #   is nsim. Calculates the p-values (upper bound of the one-sided confidence 
  #   interval with level 0.95), sorts them and returns them as a vector.
  #
  #   Usage: dcol <- readRDS("liver.rds")
  #          C2 <- readRDS("C2.rds")
  #          pvgsea <- GSEA.test(dcol,C2)
  # 
  weights <- sort(w,decreasing=TRUE) # sort weights in decreasing order
  gene.list <- names(weights)        # gene names
  db <- reduce.symbols(gene.list,db) # reduce database to symbols in gene.list
  Ng <- length(db)                   # number of gene sets
  Obs.ES <- vector(length=Ng, mode="numeric")
  lg <- vector(length=Ng, mode="numeric")
  lens <- lapply(db,length)
  for (i in 1:Ng) {
    Obs.ES[i] <- {GSEA.EnrichScore(gene.list, 
                                   db[[i]],weights)}
  }
  phi <- matrix(nrow = Ng, ncol = nsim)
  for (r in 1:nsim) {
    rdb <- random.database(lens,gene.list)# take random gene sets
    for (i in 1:Ng) {
      # calculate test statistic
      phi[i, r] <- {GSEA.EnrichScore(gene.list,
                                     rdb[[i]],weights)}   
    }
  }
  
  p.vals <- matrix(0, nrow = Ng, ncol = 1)
  
  for (i in 1:Ng) {
    ES.value <- Obs.ES[i]
    if (ES.value >= 0) {                    # estimate significance according to the
      # sign of the observed test statistic
      temp <- phi[i,which(phi[i,]>=0)]
      p.vals[i, 1] <- {signif(length(which(temp >= ES.value))/
                                length(temp), digits=5)}
    } else {
      temp <- phi[i,which(phi[i,]<0)]
      p.vals[i, 1] <- {signif(length(which(temp <= ES.value))/
                                length(temp), digits=5)}
    }
  }
  indna <- which(is.na(p.vals))             # replace the na p-values
  p.vals[indna] <- 0                        # with zero
  
  # upper bound of the one-sided confidence
  p.vals <- unlist(lapply(p.vals*nsim,function(x) # interval with level 0.95
  {prop.test(x,nsim,alternative="less")$conf.int[2]}))  
  names(p.vals) <- names(db)
  return(list(pvals=p.vals, statistic=Obs.ES))
}                                         # end function GSEA.test

random.database <- function(lens,gene.list){
  #   Returns random vectors of sizes given in list
  #   lens out of gene.list.
  #
  return(lapply(lens,function(n){sample(gene.list,n)}))
}                                       # end function random.database



reduce.symbols <- function(pv,db){
  #   Reduces database db to symbols present in vector pv.
  #   Returns the reduced database.
  #
  rdb <- {lapply(db,function(v){          # apply to all gene sets
    return(find.matching(v,pv))})}     # reduce gene set to symbols in pv
  l <- lapply(rdb,length)                 # new gene set lengths
  rdb <- rdb[which(l>0)]                  # remove empty gene sets
  return(rdb)
}                                       # end function reduce.symbols

find.matching <- function(v1,v2){
  #    returns the character chains common to
  #    v1 and v2, any two vectors of character chains
  #
  return(v2[which(v2 %in% v1)])     
}                                      # end function find.matching

order.matching <- function(v1,v2){
  #    returns the indices of those entries of v1 found in v2,
  #    v1 and v2 being any two vectors of charater chains
  #
  return(which(v1 %in% v2))
}                                      # end function order.matching

normal.function <- function(weights){
  #   Takes a vector weights which contains the discretized
  #   values of a function g. Approximates the integral and
  #   normalizes it so that it sums up to 1.
  #
  distr1 <- cumsum(weights)               # calculate (approximate) the integral and
  distr <- distr1/distr1[length(weights)] # normalize it so that it sums up to 1
  return(distr)
}                                       # end function normal.function


calc_max_diff <- function(weights,s,alternative){
  #   Takes a sample s and a vector weights.
  #   Calculates and returns the value of the test
  #   statistic used for the test with alternative
  #   hypothesis alternative.
  #
  ss <- sort(s)                           # sorted sample
  lg <- length(s)                         # sample size
  wss <- weights[ss]                      # corresponding weights
  rts <- cumsum(wss)                      # random value of the test statistic
  rtsn <- rts/rts[lg]                     # same normalized
  distr <- normal.function((weights))       # normalized integral
  trval <- distr[ss]                      # expected weighted distribution
  switch(alternative,
         # "two.sided"={diff1 <- rtsn-trval      # difference at the discontinuity points 
         "two.sided"={diff1 <- rtsn-trval      # difference at the discontinuity points 
         if (lg>=2){      
           diff2 <- {   c(trval[1],trval[2:lg]-rtsn[1:(lg-1)])}
           # and at one point before
         } else{
           diff2 <- trval[1]
         }
         # diff <- diff1},
         diff <- (c(diff1,diff2))},
         "greater"={diff <- rtsn-trval},
         "less"={
           if (lg>=2){                   
             diff <- {c(trval[1],trval[2:lg]-rtsn[1:(lg-1)])}
             # at one point before
           } else{
             diff <- trval[1]
           }})
  
  # added by BSIRANOS
  # convert to two-tailed statistic by looking
  # for negative and positive values.
  # Similar to GSEA
  # max.ES <- max(diff,na.rm = T)
  # min.ES <- min(diff, na.rm = T)
  # print(paste('MAX:',max.ES,'MIN:', min.ES))
  # if (max.ES > -min.ES) {
  #   m <- max.ES
  #   
  # } else {
  #   m <- min.ES
  # }
  m <- max(diff)                          # take the maximum
  return(m)
}                                       # end function calc_max_diff


WKS.EnrichScore <- function(gene.list, gene.set, weights,alternative, Gt){
  #   Calculates the value of the WKS test statistic 
  #   when the alternative hypothesis is alternative for 
  #   given numeric data weights, indexed by the
  #   genes in gene.list and a given set of genes gene.set.
  #   Returns the calculated observed test statistic.
  
  cgi <- order.matching(gene.list,gene.set)# common gene indices 
  if (length(cgi)>0){ 
    # rewrite this function to calculate max diff
    # use logic similar to GSEA that I understand better...
    # taken from function below
    
    tag.indicator <- sign(match(gene.list, gene.set, nomatch=0))
    no.tag.indicator <- 1 - tag.indicator 
    N <- length(gene.list)                # length of gene list
    Nh <- length(gene.set)                # gene set length
    Nm <-  N - Nh                         # difference between the two
    correl.vector <- abs(weights)
    sum.correl.tag    <- sum(correl.vector[tag.indicator == 1])
    norm.tag    <- 1.0/sum.correl.tag     # normalization factor
    norm.no.tag <- 1.0/Nm                 # 1/(N-Nh)
    # calculate S(t)
    St <- cumsum(tag.indicator*correl.vector * norm.tag)
    
    # calculate G(t) : cumulative sum of all weights
    # normalized by the sum of all weights
    
    # Gt <- cumsum(weights)/sum(weights)
    wRES <- St-Gt
    
    max.wES <- max(wRES)
    min.wES <- min(wRES)
    if (max.wES > - min.wES) {
      m <- signif(max.wES, digits = 5)
    } else {
      m <- signif(min.wES, digits=5)
    }
    
  } else m <- NA
  return(m*sqrt(length(cgi)))             # scale it               
}   

GSEA.EnrichScore <- function(gene.list, gene.set, weights) {  
  #   Calculates the value of the GSEA test statistic for 
  #   given numeric data weights, ranked in decreasing order, indexed by the
  #   genes in gene.list and a given set of genes gene.set.
  #   Returns the calculated observed test statistic.
  #   Part of the functions encoded by the Broad Institute, have been used.
  #   Usage: dcol <- readRDS("kidney.rds")
  #          C2 <- readRDS("C2.rds")
  #          weights <- sort(dcol,decreasing=TRUE)
  #          gene.list <- names(weights)
  #          gene.set <- C2[[16]]
  #          GSEA.EnrichScore(gene.list, gene.set, weights)
  #
  # get signs
  tag.indicator <- sign(match(gene.list, gene.set, nomatch=0))
  no.tag.indicator <- 1 - tag.indicator 
  N <- length(gene.list)                # length of gene list
  Nh <- length(gene.set)                # gene set length
  Nm <-  N - Nh                         # difference between the two
  correl.vector <- abs(weights)
  sum.correl.tag    <- sum(correl.vector[tag.indicator == 1])
  norm.tag    <- 1.0/sum.correl.tag     # normalization factor
  norm.no.tag <- 1.0/Nm                 # 1/(N-Nh)
  # calculation of the test statistic
  RES <- {cumsum(tag.indicator*correl.vector * norm.tag
                 - no.tag.indicator * norm.no.tag)}      
  max.ES <- max(RES)
  min.ES <- min(RES)
  if (max.ES > - min.ES) {
    ES <- signif(max.ES, digits = 5)
  } else {
    ES <- signif(min.ES, digits=5)
  }
  return(ES)    
}                                        # end function GSEA.EnrichScore


rbrmotint <- function(weights,ndiscr, subseq, wsubseq, sdev, lg, incr=NULL){
  #   Takes a vector weights. 
  #   Simulates a trajectory of the stochastic integral
  #   of weights with respect to BM in ndiscr 
  #   discretization points. Returns the result.
  #
  
  # generate if not precomputed. 
  if (is.null(incr)){
    incr <- rnorm(n=ndiscr,sd=sdev)# BM increments
  }
  
  # lg <- length(weights)
  # subseq <- trunc(seq(1,lg,lg/ndiscr))
  # mult <- incr*weights[subseq]            # multiply increments
  
  mult <- incr*wsubseq           # multiply increments
  res <- cumsum(mult)                     # sum of increments
  return(res)
}                                       # end function rbrmotint


lim.distr<-function(weights,nsim,alternative, incrs=NULL){
  #   Takes a vector weights. Simulates nsim trajectories
  #   of the limiting stochastic process (centered BM integral).
  #   The number of discretization points for each is ndiscr.
  #   Returns a vector with the realizations of the limiting 
  #   random variable (maximum of the absolute simulated trajectory
  #   when alternative="two.sided" 
  # remove the option to specify another ndiscr, simplify things
  
  
  lg <- length(weights)
  ndiscr <- lg
  distr1 <- cumsum(weights)               # approximate the integral and
  distr <- distr1/distr1[lg]              # normalize it so that it sums up to 1
  norm_fact <- distr1[lg]/lg             # normalization factor
  
  
  # took this out of rbrmotint because it doesnt change
  sdev <- 1/sqrt(ndiscr)
  
  # okay, pregenerate BM increments. Do only if not provided
  if (is.null(incrs)){
    incrs <- sapply(1:nsim, function(x) rnorm(n=ndiscr,sd=sdev))
    print('ran incrs')
  }
  
  # cary out the other steps from the simulation in vectorized form
  # each column is one simulation of nsamples values
  
  # things done in rbmotint
  mult <- incrs*weights
  res <- apply(mult, 2, cumsum)
  # thing typically done in this funcitons
  res.subtract <- t(matrix(distr, nrow=ncol(res), ncol=nrow(res), byrow = T) * res[ndiscr,])
  resnorm <- res-res.subtract
  # resnorm <- apply(res, 2, function(x) x-distr*x[ndiscr])
  fres <- apply(resnorm, 2, function(x) max(abs(x)))
  
  # fres <- sapply(1:nsim, function(x) {
  #   res <- rbrmotint(weights,ndiscr, subseq, wsubseq, sdev, lg, incrs[x,])       # simulation of the BM integral
  #   # realization of process
  #         #   1093 1093    1    
  #   resnorm <- res-dsubseq*res[ndiscr]
  #   max(abs(resnorm))       
  # })
  
  # for(k in 1:nsim){
  #   res <- rbrmotint(weights,ndiscr, subseq, wsubseq)       # simulation of the BM integral
  #   # realization of process
  #   resnorm <- res-dsubseq*res[ndiscr]
  #     fres[k] <- max(abs(resnorm))         # maximum of the absolute limit
  # }                                       # end for
  return(fres/norm_fact)                  # scale the result
}                                       # end function lim.distr
