# combine the resutls from a gutc query into a single ds
library(argparser)
options(stringsAsFactors = F)

parser <- arg_parser("Script to combine the results of a CMap query into a single dataset. 
                     Percentile Scores for individual cells and summly are combined with PCLs 
                     into a single dataset, named pert_pcl_all.gctx in the input folder.")
parser <- add_argument(parser, 'infolder', 'The name of a gutc result folder. Typically /matrices/gutc/
                       Must include the following files {ps_pert_cell.gctx, ps_pert_summary.gctx, 
                       ps_pcl_cell.gctx, ps_pcl_summary.gctx}', default=NA)
parser <- add_argument(parser, '--keepNA', 'Specify this flag to not replace NA values in the matrices
                       by 0, which is done by default.', flag=T)
parser <- add_argument(parser, '--output_gct', 'Also output a gct file of the results', flag=T)
parser <- add_argument(parser, '--output_pert_summly', 'ps file that has all the summly results', flag=T)

arguments <- parse_args(parser)
infolder <- arguments$infolder
print(arguments)

# load up all the different matrices
# cmap is rows, tcga is in columns 
ps.pert.cell <- parse.gctx(path.join(infolder, 'ps_pert_cell.gctx'))
ps.pert.sum <- parse.gctx(path.join(infolder, 'ps_pert_summary.gctx'))
ps.pcl.cell <- parse.gctx(path.join(infolder, 'ps_pcl_cell.gctx'))
ps.pcl.sum <- parse.gctx(path.join(infolder, 'ps_pcl_summary.gctx'))
# remove things that are ctl_vector, ctl_vehicle
ps.pert.cell <- subset.gct(ps.pert.cell, rid=ps.pert.cell@rid[ps.pert.cell@rdesc$pert_type %in% c('trt_cp', 'trt_oe','trt_sh.cgs')])
ps.pert.sum <- subset.gct(ps.pert.sum, rid=ps.pert.sum@rid[ps.pert.sum@rdesc$pert_type %in% c('trt_cp', 'trt_oe','trt_sh.cgs')])

# add some annotations to make the merge easy 
ps.pert.sum@rdesc <- cbind('summly',ps.pert.sum@rdesc)
colnames(ps.pert.sum@rdesc)[1] <- 'cell_id'
# pcl annotation munging 
ps.pcl.sum@rdesc <- data.frame('summly', ps.pcl.sum@rdesc$id, ps.pcl.sum@rdesc$id, ps.pcl.sum@rdesc$id, 'PCL')
colnames(ps.pcl.sum@rdesc) <- colnames(ps.pert.sum@rdesc)
pcl.cell.cells <- sapply(ps.pcl.cell@rdesc$id, function(x) strsplit(x,':')[[1]][2])
pcl.cell.ids <- sapply(ps.pcl.cell@rdesc$id, function(x) strsplit(x,':')[[1]][1])
ps.pcl.cell@rdesc <- data.frame(pcl.cell.cells, ps.pcl.cell@rdesc$id, pcl.cell.ids, pcl.cell.ids, 'PCL')
colnames(ps.pcl.cell@rdesc) <- colnames(ps.pert.sum@rdesc)

# iteratively merge files
pert.pcl.all <- merge.gct(ps.pert.cell, ps.pert.sum, dimension='row')
pert.pcl.all <- merge.gct(pert.pcl.all, ps.pcl.cell, dimension='row')
pert.pcl.all <- merge.gct(pert.pcl.all, ps.pcl.sum, dimension='row')

# remove NAs in this matrix (ie set them to zero)
# as long as the flag isnt specified. 
if(!arguments$keepNA){
  print('Replacing NA values with 0 in the pert_pcl_all matrix')
  pert.pcl.all@mat[is.na(pert.pcl.all@mat)] <- 0
}

# write out file 
write.gctx(pert.pcl.all, path.join(infolder, 'pert_pcl_all.gctx'))
if(arguments$output_pert_summly){
  write.gctx(merge.gct(ps.pert.sum, ps.pcl.sum, dimension = 'row'), path.join(infolder, 'pert_pcl_sum.gctx'))
}

if(arguments$output_gct){
  write.gct(pert.pcl.all, path.join(infolder, 'pert_pcl_all.gct'))
  if(arguments$output_pert_summly){
    write.gct(merge.gct(ps.pert.sum, ps.pcl.sum, dimension = 'row'), path.join(infolder, 'pert_pcl_sum.gct'))
  }
  
}
message('DONE! :)')

