#----------------------------------------------------------------
#               Weighted Kolmogorov-Smirnov test 
#                    K. Charmpi, B. Ycart,
#               Version 1.0, February 19, 2015
#----------------------------------------------------------------

#---------------------- Variables -------------------------------
#   nsim          integer, number of simulations
#   ndiscr        integer, number of discretization points
#   w             named vector of numeric. It contains the
#                 weights, with the corresponding genes being
#                 given as names
#   weights       named vector of numeric, containing the weights
#                 ordered in decreasing order or a continuous function of it
#   gene.list     vector of character, containing the genes
#                 (names of weights)
#   db            list of vector of character, database of gene sets
#   gene.set      vector of character, gene set 
#   gene.sets     vector of character, gene set names
#   ranked        logical, indicates whether the true values in w should 
#                 be replaced by their ranks or not
#   alternative   character in "two.sided", "greater", "less". Specifies
#                 the alternative hypothesis of the WKS test
#   s             vector of numeric, a sample
#   pv1,pv2       named vectors of numeric, p-values 
#   kwd           character. A keyword, usually a tissue 
#                 (e.g. "liver","breast","lung", etc.) 
#   nid           integer, number of points to be identified on a graph
#   v1,v2,pv      vectors of character
#   lens          list of numeric, containing the gene set sizes
#   title         character, title of a graphic
#   xlab          character, label for the x-axis of a graphic
#   ylab          character, label for the y-axis of a graphic

#---------------------- Functions ------------------------------- 

#------------------- Main functions------------------------------
# Statistical tests
# WKS.test(w,db,nsim=10000,ndiscr,ranked=FALSE,alternative="two.sided")
# GSEA.test(w,db,nsim=1000)
# Graphical comparison of two p-value vectors
# paired.pvalues(pv1,pv2,kwd,nid=0,title="Paired p-values",
#                  xlab="-log10 p-values",
#                  ylab="-log10 p-values")
# Graphical plot of gene sets
# cumulated.weights(w,db,gene.sets,title="Cumulated weights")
# enrichment.plot(w,db,gene.sets,xlab="genes",ylab="ES")

#----------------- Auxiliary functions--------------------------
# Calculation of the test statistics
# WKS.EnrichScore(gene.list, gene.set, weights,alternative)
# GSEA.EnrichScore(gene.list, gene.set, weights)
# Simulation for the limiting stochastic process
# lim.distr(weights,nsim,ndiscr,alternative)
# rbrmotint(weights,ndiscr)
# comparison of gene sets
# order.matching(v1,v2)
#--------------- global graphic parameters ---------------------
colpoint <- "blue3"                  # color for points
coltri <- "red3"                     # color for triangles
colline <- "black"                   # color for lines
colstep <- "blue3"                   # color for step function
colid <- "green4"                    # color for identifying
cexp <- 0.2                          # point size
lth <- 1.5                           # line thickness
lty <- 2                             # line type
cexa <- 1.1                          # axes size
cexl <- 1.1                          # labels size
cexm <- 1.3                          # title size
cext <- 0.7                          # text size
sctick <- 0.1                        # scale for ticks

#---------------------------------------------------------------
#                     tests
#---------------------------------------------------------------

# ps <- parse.gctx('~/projects/techno_sample/resources/query/ps_brca_small_n1093x10.gctx')
# # ps2 <- parse.gctx('/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/gutc_results/gutc/pert_pcl_all_n10255x82902.gctx', rid=1:100)
# ss <- parse.gmt('~/projects/techno_sample/resources/query/outlier_ss_brca_100.gmt')
# gsets <- lapply(ss, function(x) x$entry)

# nsim <- 100000
# ndiscr <- ncol(ps@mat)
# sdev <- 1/sqrt(ndiscr)
# incrs <- sapply(1:nsim, function(x) rnorm(n=ndiscr,sd=sdev))

# newres <- lapply(1:nrow(ps@mat), function(x) WKS.test(ps@mat[x,], gsets, nsim=nsim, incrs=incrs))
# plot(-log10(r1$pvals), -log10(newres[[2]]$pvals))
# plot(-log10(g1$pvals), -log10(newres[[2]]$pvals))
# enrichment.plot(ps@mat[1,], gsets, names(gsets)[1:4])
# 
# #profiling
# l <- lineprof(WKS.test(ps@mat[2,], gsets, incrs=incrs))
# shine(l)

#---------------------------------------------------------------
#                        WKS test
#---------------------------------------------------------------


WKS.test <- function(w,db,nsim=10000,ndiscr=NULL,ranked=FALSE,alternative="two.sided", incrs=NULL, mask.zero=F){
  #   Takes a named vector w and a database db. Performs
  #   the WKS test with alternative "two.sided", "less", or "greater". 
  #   The limiting distribution is estimated through nsim Monte-Carlo 
  #   simulations, the number of discretization points being ndiscr. 
  #   If ndiscr is missing, then the number of discretization points
  #   is equal to the size of w.
  #   Calculates the p-values (upper bound of the one-sided confidence 
  #   interval with level 0.95) 
  #   if mask.zero, zero weights are not included in the calculations
  if (mask.zero){
    db <- sapply(db, function(x) x[w[x] != 0])
    w <- w[w!=0]
  }
  
  correl.vector <- sort(w,decreasing=TRUE) # sort the vector in decreasing order 
  acv <- abs(correl.vector)
  lg <- length(correl.vector)
  gene.list <- names(correl.vector)        # all genes
  
  # Precompute G(t): cumulated sum of weights. This stays the same for all gene sets.
  Gt <- cumsum(acv)/sum(acv)
  # Evaluate the WKS statistic
  wks.eval <- unlist(lapply(db,function(x) WKS.EnrichScore(gene.list, x, acv, alternative, Gt)))
  # observed test statistic
  ots <- wks.eval
  # eliminate NA values corresponding to gene sets with an empty intersection with the list
  ots <- na.omit(ots)               
  
  # if not provided ndiscr
  if (is.null(ndiscr)){
    ndiscr <- length(w)
  }
  
  # if not provided incrs
  if (is.null(incrs)){
    sdev <- 1/sqrt(ndiscr)
    incrs <- sapply(1:nsim, function(x) rnorm(n=ndiscr,sd=sdev))
  }
  
  # mb <- microbenchmark(
  
  ld <- {lim.distr(weights=abs(correl.vector),
                   nsim=nsim,alternative, incrs)} # estimation of limiting distribution
  # , times = 50)
  
  pvalsm <- {unlist(lapply(abs(ots),function(x){
    length(which(ld>=x))}))}
  # upper bound one-sided confidence
  pvalsci <- unlist(lapply(pvalsm,function(x) # interval with level 0.95
  {prop.test(x,nsim,alternative="less")$conf.int[2]}))
  
  # return(pvalsci)
  #  now return a list of pvalues and ES
  return(list(pvals=pvalsci, statistic=ots))
}                                       # end function WKS.test


#---------------------------------------------------------------
#                        GSEA test
#---------------------------------------------------------------


GSEA.test <- function(w,db,nsim=1000){
  #   Takes a named vector w and a database db. Performs the GSEA test. 
  #   The number of Monte-Carlo simulations for every gene set 
  #   is nsim. Calculates the p-values (upper bound of the one-sided confidence 
  #   interval with level 0.95), sorts them and returns them as a vector.
  #
  #   Usage: dcol <- readRDS("liver.rds")
  #          C2 <- readRDS("C2.rds")
  #          pvgsea <- GSEA.test(dcol,C2)
  # 
  weights <- sort(w,decreasing=TRUE) # sort weights in decreasing order
  gene.list <- names(weights)        # gene names
  db <- reduce.symbols(gene.list,db) # reduce database to symbols in gene.list
  Ng <- length(db)                   # number of gene sets
  Obs.ES <- vector(length=Ng, mode="numeric")
  lg <- vector(length=Ng, mode="numeric")
  lens <- lapply(db,length)
  for (i in 1:Ng) {
    Obs.ES[i] <- {GSEA.EnrichScore(gene.list, 
                                   db[[i]],weights)}
  }
  phi <- matrix(nrow = Ng, ncol = nsim)
  for (r in 1:nsim) {
    rdb <- random.database(lens,gene.list)# take random gene sets
    for (i in 1:Ng) {
      # calculate test statistic
      phi[i, r] <- {GSEA.EnrichScore(gene.list,
                                     rdb[[i]],weights)}   
    }
  }
  
  p.vals <- matrix(0, nrow = Ng, ncol = 1)
  
  for (i in 1:Ng) {
    ES.value <- Obs.ES[i]
    if (ES.value >= 0) {                    # estimate significance according to the
      # sign of the observed test statistic
      temp <- phi[i,which(phi[i,]>=0)]
      p.vals[i, 1] <- {signif(length(which(temp >= ES.value))/
                                length(temp), digits=5)}
    } else {
      temp <- phi[i,which(phi[i,]<0)]
      p.vals[i, 1] <- {signif(length(which(temp <= ES.value))/
                                length(temp), digits=5)}
    }
  }
  indna <- which(is.na(p.vals))             # replace the na p-values
  p.vals[indna] <- 0                        # with zero
  
  # upper bound of the one-sided confidence
  p.vals <- unlist(lapply(p.vals*nsim,function(x) # interval with level 0.95
  {prop.test(x,nsim,alternative="less")$conf.int[2]}))  
  names(p.vals) <- names(db)
  return(list(pvals=p.vals, statistic=Obs.ES))
}                                         # end function GSEA.test

random.database <- function(lens,gene.list){
  #   Returns random vectors of sizes given in list
  #   lens out of gene.list.
  #
  return(lapply(lens,function(n){sample(gene.list,n)}))
}                                       # end function random.database



reduce.symbols <- function(pv,db){
  #   Reduces database db to symbols present in vector pv.
  #   Returns the reduced database.
  #
  rdb <- {lapply(db,function(v){          # apply to all gene sets
    return(find.matching(v,pv))})}     # reduce gene set to symbols in pv
  l <- lapply(rdb,length)                 # new gene set lengths
  rdb <- rdb[which(l>0)]                  # remove empty gene sets
  return(rdb)
}                                       # end function reduce.symbols

find.matching <- function(v1,v2){
  #    returns the character chains common to
  #    v1 and v2, any two vectors of character chains
  #
  return(v2[which(v2 %in% v1)])     
}                                      # end function find.matching

order.matching <- function(v1,v2){
  #    returns the indices of those entries of v1 found in v2,
  #    v1 and v2 being any two vectors of charater chains
  #
  return(which(v1 %in% v2))
}                                      # end function order.matching

#---------------------------------------------------------------
#             Calculation of the test statistics
#---------------------------------------------------------------

#------------------ auxiliary functions --------------------------

normal.function <- function(weights){
  #   Takes a vector weights which contains the discretized
  #   values of a function g. Approximates the integral and
  #   normalizes it so that it sums up to 1.
  #
  distr1 <- cumsum(weights)               # calculate (approximate) the integral and
  distr <- distr1/distr1[length(weights)] # normalize it so that it sums up to 1
  return(distr)
}                                       # end function normal.function


calc_max_diff <- function(weights,s,alternative){
  #   Takes a sample s and a vector weights.
  #   Calculates and returns the value of the test
  #   statistic used for the test with alternative
  #   hypothesis alternative.
  #
  ss <- sort(s)                           # sorted sample
  lg <- length(s)                         # sample size
  wss <- weights[ss]                      # corresponding weights
  rts <- cumsum(wss)                      # random value of the test statistic
  rtsn <- rts/rts[lg]                     # same normalized
  distr <- normal.function((weights))       # normalized integral
  trval <- distr[ss]                      # expected weighted distribution
  switch(alternative,
         # "two.sided"={diff1 <- rtsn-trval      # difference at the discontinuity points 
         "two.sided"={diff1 <- rtsn-trval      # difference at the discontinuity points 
         if (lg>=2){      
           diff2 <- {   c(trval[1],trval[2:lg]-rtsn[1:(lg-1)])}
           # and at one point before
         } else{
           diff2 <- trval[1]
         }
         # diff <- diff1},
         diff <- (c(diff1,diff2))},
         "greater"={diff <- rtsn-trval},
         "less"={
           if (lg>=2){                   
             diff <- {c(trval[1],trval[2:lg]-rtsn[1:(lg-1)])}
             # at one point before
           } else{
             diff <- trval[1]
           }})
  
  # added by BSIRANOS
  # convert to two-tailed statistic by looking
  # for negative and positive values.
  # Similar to GSEA
  # max.ES <- max(diff,na.rm = T)
  # min.ES <- min(diff, na.rm = T)
  # print(paste('MAX:',max.ES,'MIN:', min.ES))
  # if (max.ES > -min.ES) {
  #   m <- max.ES
  #   
  # } else {
  #   m <- min.ES
  # }
  m <- max(diff)                          # take the maximum
  return(m)
}                                       # end function calc_max_diff


WKS.EnrichScore <- function(gene.list, gene.set, weights,alternative, Gt){
  #   Calculates the value of the WKS test statistic 
  #   when the alternative hypothesis is alternative for 
  #   given numeric data weights, indexed by the
  #   genes in gene.list and a given set of genes gene.set.
  #   Returns the calculated observed test statistic.
  
  # if all weights of the gene set are zero, return 0
  if(all(weights[gene.set]==0)){
    return(0)
  }
  
  cgi <- order.matching(gene.list,gene.set)# common gene indices 
  if (length(cgi)>0){ 
    # rewrite this function to calculate max diff
    # use logic similar to GSEA that I understand better...
    # taken from function below
    
    tag.indicator <- sign(match(gene.list, gene.set, nomatch=0))
    no.tag.indicator <- 1 - tag.indicator 
    N <- length(gene.list)                # length of gene list
    Nh <- length(gene.set)                # gene set length
    Nm <-  N - Nh                         # difference between the two
    correl.vector <- abs(weights)
    sum.correl.tag    <- sum(correl.vector[tag.indicator == 1])
    norm.tag    <- 1.0/sum.correl.tag     # normalization factor
    norm.no.tag <- 1.0/Nm                 # 1/(N-Nh)
    # calculate S(t)
    St <- cumsum(tag.indicator*correl.vector * norm.tag)
    
    # calculate G(t) : cumulative sum of all weights
    # normalized by the sum of all weights
    # Gt <- cumsum(weights)/sum(weights)
    
    wRES <- St-Gt
    
    max.wES <- max(wRES, na.rm = T)
    min.wES <- min(wRES, na.rm = T)
    if (max.wES > - min.wES) {
      m <- signif(max.wES, digits = 5)
    } else {
      m <- signif(min.wES, digits=5)
    }
    
  } else m <- NA
  return(m*sqrt(length(cgi)))             # scale it               
}   

GSEA.EnrichScore <- function(gene.list, gene.set, weights) {  
  #   Calculates the value of the GSEA test statistic for 
  #   given numeric data weights, ranked in decreasing order, indexed by the
  #   genes in gene.list and a given set of genes gene.set.
  #   Returns the calculated observed test statistic.
  #   Part of the functions encoded by the Broad Institute, have been used.
  #   Usage: dcol <- readRDS("kidney.rds")
  #          C2 <- readRDS("C2.rds")
  #          weights <- sort(dcol,decreasing=TRUE)
  #          gene.list <- names(weights)
  #          gene.set <- C2[[16]]
  #          GSEA.EnrichScore(gene.list, gene.set, weights)
  #
  # get signs
  tag.indicator <- sign(match(gene.list, gene.set, nomatch=0))
  no.tag.indicator <- 1 - tag.indicator 
  N <- length(gene.list)                # length of gene list
  Nh <- length(gene.set)                # gene set length
  Nm <-  N - Nh                         # difference between the two
  correl.vector <- abs(weights)
  sum.correl.tag    <- sum(correl.vector[tag.indicator == 1])
  norm.tag    <- 1.0/sum.correl.tag     # normalization factor
  norm.no.tag <- 1.0/Nm                 # 1/(N-Nh)
  # calculation of the test statistic
  RES <- {cumsum(tag.indicator*correl.vector * norm.tag
                 - no.tag.indicator * norm.no.tag)}      
  max.ES <- max(RES)
  min.ES <- min(RES)
  if (max.ES > - min.ES) {
    ES <- signif(max.ES, digits = 5)
  } else {
    ES <- signif(min.ES, digits=5)
  }
  return(ES)    
}                                        # end function GSEA.EnrichScore


#----------------------------------------------------------------
#        Simulation for the limiting stochastic process
#----------------------------------------------------------------

#------------------ auxiliary function --------------------------

rbrmotint <- function(weights,ndiscr, subseq, wsubseq, sdev, lg, incr=NULL){
  #   Takes a vector weights. 
  #   Simulates a trajectory of the stochastic integral
  #   of weights with respect to BM in ndiscr 
  #   discretization points. Returns the result.
  #
  
  # generate if not precomputed. 
  if (is.null(incr)){
    incr <- rnorm(n=ndiscr,sd=sdev)# BM increments
  }
  
  # lg <- length(weights)
  # subseq <- trunc(seq(1,lg,lg/ndiscr))
  # mult <- incr*weights[subseq]            # multiply increments
  
  mult <- incr*wsubseq           # multiply increments
  res <- cumsum(mult)                     # sum of increments
  return(res)
}                                       # end function rbrmotint

#--------------------- main function ----------------------------

lim.distr<-function(weights,nsim,alternative, incrs=NULL){
  #   Takes a vector weights. Simulates nsim trajectories
  #   of the limiting stochastic process (centered BM integral).
  #   The number of discretization points for each is ndiscr.
  #   Returns a vector with the realizations of the limiting 
  #   random variable (maximum of the absolute simulated trajectory
  #   when alternative="two.sided" 
  # remove the option to specify another ndiscr, simplify things
  
  
  lg <- length(weights)
  ndiscr <- lg
  distr1 <- cumsum(weights)               # approximate the integral and
  distr <- distr1/distr1[lg]              # normalize it so that it sums up to 1
  norm_fact <- distr1[lg]/lg             # normalization factor
  
  
  # took this out of rbrmotint because it doesnt change
  sdev <- 1/sqrt(ndiscr)
  
  # okay, pregenerate BM increments. Do only if not provided
  if (is.null(incrs)){
    incrs <- sapply(1:nsim, function(x) rnorm(n=ndiscr,sd=sdev))
    print('ran incrs')
  }
  
  # cary out the other steps from the simulation in vectorized form
  # each column is one simulation of nsamples values
  
  # things done in rbmotint
  mult <- incrs*weights
  res <- apply(mult, 2, cumsum)
  # thing typically done in this funcitons
  res.subtract <- t(matrix(distr, nrow=ncol(res), ncol=nrow(res), byrow = T) * res[ndiscr,])
  resnorm <- res-res.subtract
  # resnorm <- apply(res, 2, function(x) x-distr*x[ndiscr])
  fres <- apply(resnorm, 2, function(x) max(abs(x)))
  
  # fres <- sapply(1:nsim, function(x) {
  #   res <- rbrmotint(weights,ndiscr, subseq, wsubseq, sdev, lg, incrs[x,])       # simulation of the BM integral
  #   # realization of process
  #         #   1093 1093    1    
  #   resnorm <- res-dsubseq*res[ndiscr]
  #   max(abs(resnorm))       
  # })
  
  # for(k in 1:nsim){
  #   res <- rbrmotint(weights,ndiscr, subseq, wsubseq)       # simulation of the BM integral
  #   # realization of process
  #   resnorm <- res-dsubseq*res[ndiscr]
  #     fres[k] <- max(abs(resnorm))         # maximum of the absolute limit
  # }                                       # end for
  return(fres/norm_fact)                  # scale the result
}                                       # end function lim.distr


#----------------------------------------------------------------
#                Plot two vectors of p-values
#----------------------------------------------------------------

paired.pvalues <- function(pv1,pv2,kwd,nid=0,title="paired p-values",
                           xlab="-log10 p-values",ylab="-log10 p-values"){
  #   Takes two vectors of p-values pv1 and pv2.
  #   Plots the two vectors (pv1 on the y-axis, pv2 on the x-axis), 
  #   -log10 transformed, and represents the gene sets 
  #   matching kwd as red triangles. Identifies nid of those.
  #
  #   Usage: paired.pvalues(pvwks,pvgsea,"liver",title="WKS vs. GSEA",
  #            xlab="-log10 p-values GSEA",ylab="-log10 p-values WKS")
  #
  lpv1 <- -log10(pv1)                     # -log10 transformed
  lpv2 <- -log10(pv2[names(pv1)])         # -log10 transformed
  {plot(lpv1,lpv2,                        # all points 
        xlab=xlab,ylab=ylab,
        main=title,cex.axis=cexa,cex.lab=cexl,
        cex.main=cexm,pch=19,cex=cexp,col="blue4")}
  abline(0,1,lty=lty,lwd=lth)             # add bisector
  abline(h=-log10(0.05),lty=lty,lwd=lth)  # horizontal line at pv=0.05
  abline(v=-log10(0.05),lty=lty,lwd=lth)  # vertical line at pv=0.05
  if (!missing(kwd)){
    lkwd <- tolower(kwd)                 # switch to lower case
    nmpv <- tolower(names(pv1))          # switch to lower case
    indt <- which(grepl(lkwd,nmpv))      # get gene sets matching keyword
    lpv1t <- lpv1[indt]                  # their p-values for test 1
    lpv2t <- lpv2[indt]                  # their p-values for test 2
    {points(lpv1t,lpv2t,                 # points with biological info
            pch=17,cex=1.2,col=coltri)}       # as red triangles
    if(nid>0){
      {identify(lpv1t,lpv2t,n=nid,         # identify nid gene sets
                labels=names(lpv1t),col=colid,cex=cext)}}
  }
}                                       # end function paired.pvalues

#----------------------------------------------------------------
#               Plot cumulated proportions of weights
#----------------------------------------------------------------

cumulated.weights <- function(w,db,gene.sets,title="Cumulated weights"){
  #   Takes a named vector w, a database db and a vector of 
  #   gene set names gene.sets.
  #   Plots the realizations of the random quantity used in
  #   WKS test statistic for the given gene sets.
  #   The bisector and the expected theoretical function are
  #   added on the graph.
  #
  #   Usage: dcol <- readRDS("kidney.rds")
  #          C2 <- readRDS("C2.rds"); 
  #          gene.sets <- c("ACEVEDO_LIVER_CANCER_DN","ACEVEDO_LIVER_CANCER_UP")
  #          cumulated.weights(dcol,C2,gene.sets)
  #
  xlab <- "t"
  ylab <- "Sn(t)"
  weights <- abs(sort(w,decreasing=TRUE))        # sort weights in decreasing order
  gene.list <- names(weights)                    # all genes
  lgw <- length(weights)                         # their number
  stpfunc <- function(gene.set){
    ma <- order.matching(gene.list,gene.set)    # indices of common genes
    ma1 <- ma/lgw                               # proportion
    vma <- weights[ma]                          # corresponding weights     
    fvma <- cumsum(vma)                         # cumulated weights
    lg <- length(fvma)
    fvman <- fvma/fvma[lg]                      # normalize
    sfun <- stepfun(ma1,c(0,fvman), f = 0)      # make it a step function
    return(sfun)
  }                                              # end step function
  sfun <- stpfunc(db[[gene.sets[1]]])            # compute first step function
  {plot(sfun,verticals = TRUE,do.points=FALSE,   # plot it
        xlim=c(0,1),ylim=c(0,1),col=colstep,
        xlab=xlab,ylab=ylab,
        main=title,cex.axis=cexa,cex.main=cexm)}
  len <- length(gene.sets)                       # get number of plots
  if (len>1){                                    # if more than one
    {lapply(gene.sets[2:len],                     # iterate
            function(x){lines(stpfunc(db[[x]]),xlim=c(0,1),
                              verticals = TRUE,do.points=FALSE,col=colstep)})}
  }
  abline(0,1,lty=lty,lwd=lth,col=colline)        # add bisector
  distr1 <- cumsum(abs(weights))                 # calculate the integral and
  distr <- distr1/distr1[lgw]                    # normalize
  {lines(seq(1/lgw,1,1/lgw),distr,
         lty=lty,lwd=lth,col=colline)}               # add the expected theoretical 
}                                              # end function cumulated.weights


enrichment.plot <- function(w,db,gene.sets,xlab="genes",ylab="ES"){
  #   Takes a named vector w, a database db and a vector of 
  #   gene set names gene.sets and creates a matrix of 
  #   ceiling(length(gene.sets)/2) x 2 plots.
  #   For each gene set, the difference between the cumulated
  #   weights (S_n) and the primitive function (G) is plotted.
  #   At the bottom of the graph, vertical lines are added, 
  #   indicating the positions of the genes inside the gene set
  #   in the ranked list.
  #
  #   Usage: dcol <- readRDS("kidney.rds")
  #          C2 <- readRDS("C2.rds"); 
  #          gene.sets <- c("ACEVEDO_LIVER_CANCER_DN","ACEVEDO_LIVER_CANCER_UP")
  #          enrichment.plot(dcol,C2,gene.sets)
  #          pvwks <- WKS.test(dcol,C2)
  #          gene.sets <- names(pvwks)[1:5]; enrichment.plot(dcol,C2,gene.sets)
  #          gene.sets <- names(tail(pvwks,5)); enrichment.plot(dcol,C2,gene.sets)
  #
  len <- length(gene.sets)                       # number of gene sets
  weights <- abs(sort(w,decreasing=TRUE))        # sort weights in decreasing order
  gene.list <- names(weights)                    # all genes
  lgw <- length(weights)                         # their number
  distr1 <- cumsum(abs(weights))                 # calculate the integral and
  distr <- distr1/tail(distr1,1)                 # normalize
  plotenrich <- function(gene.set,cexmain){
    name <- gene.set
    gene.set <- db[[gene.set]]
    ma <- order.matching(gene.list,gene.set)    # indices of common genes
    ma1 <- ma/lgw                               # proportion
    vma <- weights[ma]                          # corresponding weights     
    fvma <- cumsum(vma)                         # cumulated weights
    lg <- length(fvma)
    fvman <- fvma/tail(fvma,1)                  # normalize
    trv <- distr[ma]                            # (expected) theoretical values
    X <- rbind(ma1,ma1)
    X <-  as.vector(X)
    X <- c(0,X,1)                               # ordinates
    Y <- rbind(fvman,fvman)
    Y <- as.vector(Y)
    Y <- c(0,0,Y)                               # coordinates for the step function
    C <-  rbind(trv,trv)
    C <- as.vector(C)
    C <- c(0,C,1)
    Y <- Y-C                                    # coordinates for the difference
    scy0 <- min(Y); scy1 <- scy0+(max(Y)-scy0)*sctick
    scyM <- max(Y)
    # plot difference S_n-G
    {plot(X,Y,xlim=c(0,1),ylim=c(2*scy0-scy1,scyM),
          type="l",lty=1,col=colstep,             
          xlab=xlab,ylab=ylab,main=name,
          cex.lab=cexl,cex.main=cexmain,cex.axis=cexa)}
    lines(c(0,1),c(0,0),lty=lty,col=colline)    # add the line y=0
    scy0 <- min(Y); scy1 <- scy0+(max(Y)-scy0)*sctick
    X <- rbind(ma1,ma1)
    Y <- rbind(rep(2*scy0-scy1,lg),rep(scy0,lg))
    matlines(X,Y,lty=rep(1,lg),col=colline)     # add the vertical lines
  } 
  # split graphical window
  if(len>1){
    par(mfrow=c(ceiling(len/2),2))              # more than one gene set
    par(mar = c(4,4,1.5,1.5))                   # margin parameters       
    # apply to all gene sets
    pl <- lapply(gene.sets,function(x){plotenrich(x,cext)})
  }else{                                   # only one gene set
    plotenrich(gene.sets,cexm)
  }                                           # end if                    
}                                              # end function enrichment.plot
