# script to scale the wtcs file by the leading fraction 
library(argparser)
parser <- arg_parser('Scale WTCS by leading fraction. Fist renames WTCS file to something new, 
                     then reads in both files and scales the WTCS scores by the leading fraction. 
                     Scaled file will be outputed to the same name as the original WTCS file, so that
                     PDEX tool can be used on this precomputed query result')
parser <- add_argument(parser, 'wtcs', 'WTCS file')
parser <- add_argument(parser, 'lefu', 'leading fraction file')
parser <- add_argument(parser, '--sqrt', 'scale by the square root of the product', flag=T)


arguments <- parse_args(parser)
# arguments <- list(wtcs='~/cmap_tcga/cmap_queries_lm/scale_LEFU/benchmark_alteration_sets/result_WTCS.CUSTOM.COMBINED_n18x82902.gctx',
#                   lefu='~/cmap_tcga/cmap_queries_lm/scale_LEFU/benchmark_alteration_sets/result_WTCS.CUSTOM.LEFU_n18x82902.gctx')
# check to make sure both exist..
if(!(all(file.exists(arguments$wtcs), file.exists(arguments$lefu)))){
  stop('Improper input files, or do not exist')
}

# read in both
basedir <- dirname(arguments$wtcs)
wtcs <- parse.gctx(arguments$wtcs)
lefu <- parse.gctx(arguments$lefu)

print(paste('renaming WTCS file to ORIGINAL_result_WTCS.CUSTOM.COMBINED.gctx'))
file.rename(arguments$wtcs, path.join(dirname(arguments$wtcs), 'ORIGINAL_result_WTCS.CUSTOM.COMBINED.gctx'))

# scale
wtcs.scaled <- wtcs@mat * lefu@mat
if(arguments$sqrt){
  sign.mat <- sign(wtcs@mat)
  wtcs.scaled <- sqrt(abs(wtcs.scaled))
  wtcs.scaled <- wtcs.scaled * sign.mat
} 
wtcs@mat <- wtcs.scaled

write.gctx(wtcs, path.join(basedir, 'result_WTCS.gctx'))
# okay now we need to rename it again...
written.file <- path.join(basedir, paste('result_WTCS_n', ncol(wtcs@mat), 'x',nrow(wtcs@mat),'.gctx', sep=''))
rename.to <- path.join(basedir, paste('result_WTCS.CUSTOM.COMBINED_n', ncol(wtcs@mat), 'x',nrow(wtcs@mat),'.gctx', sep=''))
file.rename(written.file, rename.to)
  
