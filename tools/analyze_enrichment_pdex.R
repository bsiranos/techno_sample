# tool to analyze the result of by-sample enrichment - TOOL file
options(stringsAsFactors = F)
library(tools, quietly = T)
library(argparser, quietly=T)
# library(Biobase)
msg.trap <- capture.output(suppressMessages(library(roller)))

# source for debugging
source('~/projects/techno_sample/src/analyze_enrichment_pdex.R')

parser <- arg_parser("Script to conduct standard analysis of the by_sample results. SHORT version. Doesn't compute mean gutc score, 
                     signature strenght, correlations or other things that take a long time to do.")
parser <- add_argument(parser, "arf", help="Folder containing the results of enrichment testing of sample sets
                       in CMap query data. Must contain the following files:  
                       query_up.gmt : names of samples in the different sets 
                       query_stats.txt : sats on query sizes 
                         query_result/result_WTCS.CUSTOM.COMBINEDn*x82902.gctx : wtcs scores of samples sets connecting to cmap 
                         -OR-
                         pdex_result/cs.gctx : wtcs scores of samples sets connecting to cmap. This file will take precidence if both are found. 
                       pdex_result/ps.gctx : pdex scores of sample sets to 
                       targes.gmt : annotated targets to match for each sample set", default=NA)
parser <- add_argument(parser, "--out", help="Directory to save results. Default is the arf/analyze_enrichment_results", default=NA)
parser <- add_argument(parser, "--cmap_tcga", help="file containing percentile score connectivities of querying CMap with 
                       signatures from TCGA samples. Samples are in rows, CMap perts are in columns. Default is set.", 
                       # default='~/cmap_tcga/cmap_queries_lm/pert_pcl_all_transpose_n82902x10255.gctx')
                       default='/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/gutc_results/gutc/pert_pcl_all_transpose_n82902x10255.gctx')
parser <- add_argument(parser, "--expression", help="gctx of mRNA gene expression for samples analyzed. log2 transformed. Default is 
                       set for TCGA.", default='/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KAIG_TXONLY_n10255x12276.gctx')
parser <- add_argument(parser, "--cp_targets", help="File containing cp target annotations. Default is set. ",
                       default='/cmap/projects/cmap_tcga/cp_targets.tsv')
parser <- add_argument(parser, "--limit_perts", help="DEPRECATED: Speed up testing. limit analysis to first n perts.", default=NA)
parser <- add_argument(parser, "--do_correlation", help="Do correlation analysis and heatmap generation", flag=T)
parser <- add_argument(parser, "--do_meangutc", help="Do calculation of mean gutc scores", flag=T)
parser <- add_argument(parser, "--do_sigstrength", help="Do signature strength analysis", flag=T)
parser <- add_argument(parser, "--do_overallbest", help="Do overall best connection analysis", flag=T)
parser <- add_argument(parser, "--do_bestpert", help="Do best connections to each perturbagen analysis", flag=T)
parser <- add_argument(parser, "--do_intersection", help="Do intersection of sample set analysis", flag=T)
parser <- add_argument(parser, "--num_cores", help="parallelize median gutc score computation across n cores. If set to
                       zero, will use the number of cores on your system minus one.", default=1)
parser <- add_argument(parser, "--run_tests", help="Specify this flag to run the test casess", flag=T)
parser <- add_argument(parser, "--run_demo", help="Specify this flag to run the demo analysis.", flag=T)

# if run_tests or run_demo specified, add dummy arguments to make the parsers
# work and just do the test/demo
tryCatch(arguments <- parse_args(parser), error=function(e) 
  stop(paste(e,"If trying to run tests or demo, supply dummy arguments for all required.", sep='   ')))

# if directory is a dot, make it the current working directory
if (arguments$arf == "."){
  arguments$arf <- getwd()
}
# if out is not specified, make it relative to the arf. 
if(is.na(arguments$out)){
  arguments$out <- path.join(arguments$arf, 'analyze_enrichment_results')
}

# tests and demo
# run tests if specified, then quit
if (arguments$run_tests) {
  CalculateWKSPvalues@runTests()
  message('testing finished')
  quit(save = 'no',status = 0)
}
# run demo if specified, then quit
if (arguments$run_demo) {
  AnalyzeEnrichment@runDemo()
  message('demo finished')
  quit(save = 'no',status = 0)
}

# register tool
register.tool(arguments, "analyze_enrichment", arguments$out)

# set up call
results <- AnalyzeEnrichment@runAnalysis(arguments)

# save the results
message(paste('Saving results to', arguments$out))
AnalyzeEnrichment@saveResults(results, arguments$out)

message("Done :)")


