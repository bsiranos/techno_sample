:  
help: FALSE 
no_correlation: FALSE 
run_tests: FALSE 
run_demo: FALSE 
opts: NA 
out: NA 
cmap_tcga: /cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/gutc_results/gutc/pert_pcl_all_transpose_n82902x10255.gctx 
expression: /cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ.RSEM_ZSCLUSTER.L1KAIG_TXONLY_n10255x12276.gctx 
cp_targets: /cmap/projects/cmap_tcga/cp_targets.tsv 
limit_perts: NA 
num_cores: 1 
arf: /Users/bsiranos/cmap_tcga/pdex_testing/benchmark_alteration_sets/ 
