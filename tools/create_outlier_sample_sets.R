# create_outlier_sample_sets: TOOL FILE
options(stringsAsFactors = F)
library(tools, quietly = T)
library(argparser, quietly=T)
msg.trap <- capture.output(suppressMessages(library(roller)))
# source for debugging
source('~/projects/techno_sample/src/create_outlier_sample_sets.R')
source('~/projects/techno_sample/shiny_apps/common_functions.R')
#################################################
############## outlier sample sets ##############
#################################################
# need a dataset of robust zscored expression data
# set of genes to pick outliers for, or all (default)
# Z threshold
# minimum sample size (default 5)
# Do you pick min if there aren't enough?
# output file name

parser <- arg_parser("This script will create a gmt file for containing the names of samples with outlier expression in each gene.
                     Default is to use all genes in dataset, but this can be limited with the --genes option")
parser <- add_argument(parser, "ds", help="(robust) Z-scored dataset of expression values in gctx format. Columns correspond to samples, rows correspond to genes.", default=NA)
parser <- add_argument(parser, "out", help="Name of the output gmt file", default=NA)
parser <- add_argument(parser, "--limit_genes", help="A grp file containing the IDs of genes to report sample sets for, instead of using them all", default=NA)
parser <- add_argument(parser, "--limit_cohort", help="For tcga samples, specify a cohort to limit computation to", default=NA)
parser <- add_argument(parser, "--limit_columns", help="A grp file containing the IDs of samples to report sample sets for, instead of using them all", default=NA)
parser <- add_argument(parser, "--threshold", help="Samples with abs(zscored expression) greater than this value will be considered outliers.", default=3)
parser <- add_argument(parser, "--scale_threshold", help="Scale the z-score threshold for each gene by a function related to the number of zero samples for that gene.", flag=T)
parser <- add_argument(parser, "--min_size", help="The minimnum number of outlier samples to report a sample set.", default=5)
parser <- add_argument(parser, "--adaptive_min_size", help="Minimum size must be this fraction of total samples or min_size, whichever is greater. Commmonly chosen to be 0.01", default=NA)
parser <- add_argument(parser, "--include_name", help="Include this text (such as the name of a TCGA cohort) in the gmt head.", default=NA)
parser <- add_argument(parser, "--always_report", help="This mode will always report at least (min_size) samples as an outlier set. \n 
                       The least extreme outlier value of the set will be recorded in the file.", flag = T)
parser <- add_argument(parser, "--numzero_matrix", help="lookup table of number of zero samples per gene in each cohort", default = '/cmap/projects/stacks/STK039_TCGA_RNASEQ/tx_only/TCGA_RNASEQ_NZ_n31x12276.gctx')
parser <- add_argument(parser, "--run_tests", help="Specify this flag to run the test casess", flag=T)
parser <- add_argument(parser, "--run_demo", help="Specify this flag to run the demo analysis. 
                       Results will be saved in a folder titled outler_ss_demo in the current working directory", flag=T)

# numzero matrix
# if run_tests or run_demo specified, add dummy arguments to make the parser
# work and just do the test/demo
tryCatch(arguments <- parse_args(parser), error=function(e) 
  stop(paste(e,"If trying to run tests or demo, supply 2 dummy arguments for ds and out", sep='   ')))

# run tests if specified, then quit
if (arguments$run_tests) {
  CreateOutlierSampleSets@runTests()
  message('testing finished')
  quit(save = 'no',status = 0)
}
# run demo if specified, then quit
if (arguments$run_demo) {
  CreateOutlierSampleSets@runDemo()
  message('demo finished')
  quit(save = 'no',status = 0)
}

# run some checks on inputs
# ds exists
if(!file.exists(arguments$ds)){stop('Input ds does not exist')}
# ds is a gct/gctx
if(!(file_ext(arguments$ds) %in% c('gctx', 'gct'))){stop('Input ds must be a gct/gctx file')}

# check grps if provided
if(!is.na(arguments$limit_genes)){
  if(!file.exists(arguments$limit_genes)){stop('limit_genes does not exist')}
}
if(!is.na(arguments$limit_columns)){
  if(!file.exists(arguments$limit_columns)){stop('limit_columns does not exist')}
}
# numeric args
trash <- check_is_numeric(arguments$threshold, throw_error=T, argname="threshold")
trash <- check_is_numeric(arguments$min_size, throw_error=T, argname="min_size")
# all good, continue on...

# # create output path if it doesnt exist
# if(!file.exists(dirname(arguments$out))){
#   dir.create(dirname(arguments$out), recursive = T)
# }

# register tool
register.tool(arguments, "create_outlier_sample_sets", dirname(arguments$out))

# set up call 
results <- CreateOutlierSampleSets@runAnalysis(arguments)

# save the results
CreateOutlierSampleSets@saveResults(results, arguments$out)

message("Done :)")